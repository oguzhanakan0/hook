from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from blog.models import PostPage
# Get Blog Posts for each Tool
basit_kredi_hesaplama_post = PostPage.objects.get(pk=22)
kredi_pesinat_hesaplama_post = PostPage.objects.get(pk=22)
kredi_erken_kapama_hesaplama_post = PostPage.objects.get(pk=22)
gecikme_faizi_hesaplama_post = PostPage.objects.get(pk=22)
kredi_vergisi_hesaplama_post = PostPage.objects.get(pk=22)
kredi_yapilandirma_hesaplama_post = PostPage.objects.get(pk=22)
kk_nakit_avans_hesaplama_post = PostPage.objects.get(pk=22)
kredi_erken_odeme_cezasi_hesaplama_post = PostPage.objects.get(pk=22)

# Tool Views
def index(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/index.html', {})

def basit_kredi_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/basit_kredi_hesaplama_page.html', {'post':basit_kredi_hesaplama_post})

def kredi_erken_kapama_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/kredi_erken_kapama_hesaplama_page.html', {'post':kredi_erken_kapama_hesaplama_post})

def kredi_pesinat_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/kredi_pesinat_hesaplama_page.html', {'post':kredi_pesinat_hesaplama_post})

def kredi_yapilandirma_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/kredi_yapilandirma_hesaplama_page.html', {'post':kredi_yapilandirma_hesaplama_post})

def kk_nakit_avans_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/kk_nakit_avans_hesaplama_page.html', {'post':kk_nakit_avans_hesaplama_post})

def gecikme_faizi_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/gecikme_faizi_hesaplama_page.html', {'post':gecikme_faizi_hesaplama_post})

def kredi_erken_odeme_cezasi_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/kredi_erken_odeme_cezasi_hesaplama_page.html', {'post':kredi_erken_odeme_cezasi_hesaplama_post})

def kredi_vergisi_hesaplama(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'tools/kredi_vergisi_hesaplama_page.html', {'post':kredi_vergisi_hesaplama_post})
