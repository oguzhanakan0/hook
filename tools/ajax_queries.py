from django.contrib.sessions.models import Session
from django.http import HttpResponse
from userlogs.forms import *
from loans.views import calculate_loan_details, calculate_installment, reorder_loans
from loans.models import LoanType
from scripts.py.payment_plan import payment_plan
from scripts.py.calculators import *
from django.shortcuts import render
import json

loan_types = LoanType.objects.all()

# HELPER FUNCTIONS
def fix_inputs(post, integers=['principal','tenure','paid','delinquent'], floats=['interest']):
	for c in integers:
		try: post[c]=int(post[c].replace('.',''))
		except: pass
	for c in floats:
		try: post[c]=float(post[c].replace(',','.'))/100
		except: pass
	return post

def get_ltv(form):
	try:
		downpayment, max_principal = None, None
		if form.data['loan_type'] == 'MOR':
			downpayment, max_principal = mortgage_ltv(form.data['value'])
		elif form.data['loan_type'] in ('CAR','CA2'):
			downpayment, max_principal = tasit_ltv(form.data['value'])
		return int(downpayment), int(max_principal)
	except:
		return [None]*2

def get_early_closure(form):
	try:
		if form.data['loan_type'] == 'MOR':
			return mortgage_early_closure(form.data['principal'],form.data['tenure'],form.data['interest'],form.data['paid'])
		else:
			return early_closure(form.data['principal'],form.data['tenure'],form.data['interest'],form.data['paid'])
	except Exception as e:
		print('get_early_closure ERROR:')
		print(e)
		return [None]*5

def get_delinquency_interest(form):
	try:
		if form.data['loan_type'] == 'MOR':
			return gecikme_faizi(form.data['principal'],form.data['tenure'],form.data['interest'],form.data['paid'],form.data['delinquent'],bsmv=0, kkdf=0)
		else:
			return gecikme_faizi(form.data['principal'],form.data['tenure'],form.data['interest'],form.data['paid'],form.data['delinquent'],bsmv=0.05, kkdf=0.15)
	except Exception as e:
		print('get_delinquency_interest ERROR:')
		print(e)
		return [None]*3

def add_taxes(loan):
	if loan.loan_type.loan_type=='MOR':
		loan.total_kkdf = 0
		loan.total_bsmv = 0
		return loan
	loan.total_kkdf = loan.payment_plan.KKDF.sum()
	loan.total_bsmv = loan.payment_plan.BSMV.sum()
	return loan

def get_restructure_calculation(loan_type, *args):
	# print('get_restructure_calculation')
	# print(*args)
	if loan_type=='MOR':
		return mortgage_restructure_calculation(*args)
	return restructure_calculation(*args)
# BasitKrediHesaplamaForm
# KrediVergisiHesaplamaForm
# ErkenKapamaAraOdemeHesaplamaForm
# KrediPesinatHesaplamaForm
# KrediYapilandirmaHesaplamaForm
# KKNakitAvansHesaplamaForm
# GecikmeFaiziHesaplamaForm
# ErkenOdemeCezasiHesaplama

# AJAX QUERIES
def ajax_basit_kredi_hesaplama(request):
	print("ajax_basit_kredi_hesaplama runs with this request.POST")
	try:
		request_post = fix_inputs(request.POST.copy())
		request_post['session'] = request.session.session_key
		form = BasitKrediHesaplamaForm(request_post)
		if form.is_valid():
			form.save()
			loan =calculate_loan_details(Loan(interest=form.data['interest'],loan_type=LoanType.objects.get(loan_type=form.data['loan_type'])),\
										      principal=form.data['principal'], tenure=form.data['tenure'])
			loan.payment_plan = payment_plan(amount=loan.principal.principal, maturity=loan.tenure.tenure, interest=loan.interest)
			loan.payment_plan.nrows = loan.payment_plan.shape[0]
			tenure = loan.tenure.tenure

			at_least_one_loan = False
			min_interest = None
			loans = Loan.objects.filter(loan_type=loan.loan_type,tenure__gte=loan.tenure,principal__gte=loan.principal,active=True).order_by('bank','tenure','principal','interest').distinct('bank')
			if len(loans)>0:
				loans = Loan.objects.filter(id__in=loans).order_by('interest')
				min_interest = loans[0].interest
				tenure = loans[0].tenure.tenure
				loans = reorder_loans(loans)
				at_least_one_loan = True
			for l in loans:
				l = calculate_loan_details(l, loan.principal.principal, l.tenure.tenure)
			return render(request, 'tools/basit_kredi_hesaplama/hesaplama_sonucu.html', 
				{'loan':loan, 'loans':loans,
				'at_least_one_loan':at_least_one_loan,
				'principal':loan.principal.principal,
				'tenure':tenure,
				'min_interest':min_interest})
		else:
			print("form is not valid")
			print(form.errors)
	except Exception as e:
		print('error')
		print(str(e))

	return render(request, 'tools/global/calculation_failed_message.html')

def ajax_kredi_erken_kapama_hesaplama(request):
	print("ajax_kredi_erken_kapama_hesaplama runs with this request.POST")
	print(request.POST)
	try:
		request_post = fix_inputs(request.POST.copy())
		request_post['session'] = request.session.session_key
		print(f"fixed inputs: {request_post}")
		form = ErkenKapamaAraOdemeHesaplamaForm(request_post)
		if form.is_valid():
			form.save()
			early_payment_amount, early_payment_fine, total_interest_paid, total_cost, profit = get_early_closure(form)
			if early_payment_amount:
				return render(request, 'tools/kredi_erken_kapama_hesaplama/hesaplama_sonucu.html',
					{'early_payment_amount':early_payment_amount,
					'early_payment_fine':early_payment_fine,
					'total_interest_paid':total_interest_paid,
					'total_cost':total_cost,
					'profit':profit,
					'loan_type':loan_types.get(loan_type=form.data['loan_type'])})
			else:
				return render(request, 'tools/global/calculation_failed_message.html')
		else:
			print(form.errors)

	except Exception as e:
		print('error')
		print(str(e))

	return render(request, 'tools/global/calculation_failed_message.html')

def ajax_kredi_pesinat_hesaplama(request):
	print("ajax_konut_kredisi_pesinat_hesaplama runs with this request.POST")
	print(request.POST)
	request_post = fix_inputs(request.POST.copy(), integers=['value','tenure'])
	request_post['session'] = request.session.session_key
	form = KrediPesinatHesaplamaForm(request_post)
	if form.is_valid():
		form.save()
		downpayment, max_principal = get_ltv(form)
		if type(form.data['tenure'])==int:
			# calc loans
			at_least_one_loan = False
			min_interest = None
			tenure = form.data['tenure']
			loans = Loan.objects.filter(loan_type=form.data['loan_type'],tenure__gte=tenure,\
										principal__gte=max_principal,active=True).order_by('bank','tenure','principal','interest').distinct('bank')
			if len(loans)>0:
				loans = Loan.objects.filter(id__in=loans).order_by('interest')
				min_interest = loans[0].interest
				tenure = loans[0].tenure.tenure
				loans = reorder_loans(loans)
				at_least_one_loan = True
			for l in loans:
				l = calculate_loan_details(l, max_principal, l.tenure.tenure)
			return render(request, 'tools/kredi_pesinat_hesaplama/hesaplama_sonucu.html',
				   {'downpayment':downpayment,'max_principal':max_principal,'loans':loans,
					'at_least_one_loan':at_least_one_loan,'principal':max_principal,
					'tenure':tenure,'min_interest':min_interest})
		return render(request, 'tools/kredi_pesinat_hesaplama/hesaplama_sonucu.html',
					  {'downpayment':downpayment,'max_principal':max_principal})
	else:
		print('error')
		print(form.errors)
		return HttpResponse('errors:'+json.dumps(form.errors))

def ajax_kredi_yapilandirma_hesaplama(request):
	print("ajax_kredi_yapilandirma_hesaplama runs with this request.POST")
	print(request.POST)
	try:
		request_post = fix_inputs(request.POST.copy())
		request_post['session'] = request.session.session_key
		form = KrediYapilandirmaHesaplamaForm(request_post)
		if form.is_valid():
			form.save()
			loan =calculate_loan_details(Loan(interest=form.data['interest'],loan_type=LoanType.objects.get(loan_type=form.data['loan_type'])),\
										      principal=form.data['principal'], tenure=form.data['tenure'])

			# tenure = loan.tenure.tenure
			# print(f"tenure: {tenure}")
			loan.remaining_tenure = form.data['tenure']-form.data['paid']
			kkdf, bsmv = (0.05, 0.15) if form.data['loan_type']!='MOR' else (0,0)
			prev_plan, principal_left = restructure_get_principal_left(loan.principal.principal, loan.tenure.tenure, loan.interest, form.data['paid'], kkdf, bsmv)
			
			# at_least_one_loan = False
			# min_interest = None
			cheapest_loan = Loan.objects.filter(loan_type=loan.loan_type,tenure__gte=loan.remaining_tenure,principal__gte=principal_left,active=True).order_by('tenure','principal','interest').first()
			# if len(loans)>0:
			# 	loans = Loan.objects.filter(id__in=loans).order_by('interest')
			# 	min_interest = loans[0].interest
			# 	tenure = loans[0].tenure.tenure
			# 	loans = reorder_loans(loans)
			# 	at_least_one_loan = True
			# for l in loans:
			# 	l.tenure = Tenure(tenure=loan.remaining_tenure)
			# 	l = calculate_loan_details(l, principal_left, l.tenure.tenure)
			# cheapest_loan = loans[0]
			cheapest_loan.tenure = Tenure(tenure=loan.remaining_tenure)
			cheapest_loan = calculate_loan_details(cheapest_loan, principal_left, cheapest_loan.tenure.tenure)

			principal_left, waiting_payment, early_payment_fine, new_total_payment, profit, new_plan = get_restructure_calculation(\
				loan.loan_type.loan_type,\
				loan.principal.principal,\
				loan.tenure.tenure,\
				loan.interest,\
				form.data['paid'],\
				cheapest_loan.tenure.tenure,\
				cheapest_loan.interest,\
				prev_plan,\
				principal_left)

			loan.principal_left = principal_left
			loan.waiting_payment = waiting_payment
			loan.early_payment_fine = early_payment_fine
			cheapest_loan.new_total_payment = new_total_payment
			cheapest_loan.profit = profit
			# print(principal_left, waiting_payment, early_payment_fine, new_total_payment, profit)
			diff_interest, diff_installment, diff_total_payment = cheapest_loan.interest-loan.interest,\
			cheapest_loan.installment-loan.installment, cheapest_loan.new_total_payment-loan.waiting_payment
			return render(request, 'tools/kredi_yapilandirma_hesaplama/hesaplama_sonucu.html', 
				{'loan':loan, 'cheapest_loan':cheapest_loan,
				# 'loans':loans,
				# 'at_least_one_loan':at_least_one_loan,
				# 'principal':loan.principal.principal,
				# 'tenure':tenure,
				# 'min_interest':min_interest,
				'diff_interest':diff_interest,
				'diff_installment':diff_installment,
				'diff_total_payment':diff_total_payment,
				'should_restructure':diff_total_payment<0})
		else:
			print("form is not valid")
			print(form.errors)
	except Exception as e:
		print('error')
		print(str(e))

	return render(request, 'tools/global/calculation_failed_message.html')

def ajax_kk_nakit_avans_hesaplama(request):
	print("ajax_kk_nakit_avans_hesaplama runs with this request.POST")
	print(request.POST)
	try:
		request_post = fix_inputs(request.POST.copy())
		request_post['session'] = request.session.session_key
		form = KKNakitAvansHesaplamaForm(request_post)
		if form.is_valid():
			form.save()
			loan =calculate_loan_details(Loan(interest=form.data['interest'],loan_type=LoanType.objects.get(loan_type='GPL')),\
										      principal=form.data['principal'], tenure=form.data['tenure'])
			loan.payment_plan = payment_plan(amount=loan.principal.principal, maturity=loan.tenure.tenure, interest=loan.interest)
			loan.payment_plan.nrows = loan.payment_plan.shape[0]
			tenure = loan.tenure.tenure

			at_least_one_loan = False
			min_interest = None
			loans = Loan.objects.filter(loan_type=loan.loan_type,tenure__gte=loan.tenure,principal__gte=loan.principal,active=True).order_by('bank','tenure','principal','interest').distinct('bank')
			if len(loans)>0:
				loans = Loan.objects.filter(id__in=loans).order_by('interest')
				min_interest = loans[0].interest
				tenure = loans[0].tenure.tenure
				loans = reorder_loans(loans)
				at_least_one_loan = True
			for l in loans:
				l = calculate_loan_details(l, loan.principal.principal, l.tenure.tenure)
			return render(request, 'tools/kk_nakit_avans_hesaplama/hesaplama_sonucu.html', 
				{'loan':loan, 'loans':loans,
				'at_least_one_loan':at_least_one_loan,
				'principal':loan.principal.principal,
				'tenure':tenure,
				'min_interest':min_interest})
		else:
			print("form is not valid")
			print(form.errors)
	except Exception as e:
		print('error')
		print(str(e))

	return render(request, 'tools/global/calculation_failed_message.html')

def ajax_gecikme_faizi_hesaplama(request):
	print("ajax_gecikme_faizi_hesaplama runs with this request.POST")
	print(request.POST)
	try:
		request_post = fix_inputs(request.POST.copy())
		request_post['session'] = request.session.session_key
		print(f"fixed inputs: {request_post}")
		form = GecikmeFaiziHesaplamaForm(request_post)
		if form.is_valid():
			form.save()
			del_interest, del_interest_tax, total_del_interest = get_delinquency_interest(form)
			return render(request, 'tools/gecikme_faizi_hesaplama/hesaplama_sonucu.html',
				{'del_interest':del_interest,
				'del_interest_tax':del_interest_tax,
				'total_del_interest':total_del_interest})
		else:
			print(form.errors)

	except Exception as e:
		print('error')
		print(str(e))

	return render(request, 'tools/global/calculation_failed_message.html')

def ajax_kredi_erken_odeme_cezasi_hesaplama(request):
	print("ajax_kredi_erken_odeme_cezasi_hesaplama runs with this request.POST")
	print(request.POST)
	try:
		request_post = fix_inputs(request.POST.copy())
		request_post['session'] = request.session.session_key
		form = ErkenOdemeCezasiHesaplamaForm(request_post)
		if form.is_valid():
			form.save()
			if form.data['loan_type'] in ['GPL','CAR','CA2']:
				return render(request, 'tools/kredi_erken_odeme_cezasi_hesaplama/no_mortgage_message.html',{'loan_type':loan_types.get(loan_type=form.data['loan_type'])})
			principal_left, early_payment_fine, early_payment_amount = mortgage_early_closure_fee(form.data['principal'], form.data['tenure'], form.data['interest'], form.data['paid'])
			return render(request, 'tools/kredi_erken_odeme_cezasi_hesaplama/hesaplama_sonucu.html', 
				{'principal_left':principal_left,
				'early_payment_fine':early_payment_fine,
				'early_payment_amount':early_payment_amount})
		else:
			print("form is not valid")
			print(form.errors)
	except Exception as e:
		print('error')
		print(str(e))

	return render(request, 'tools/global/calculation_failed_message.html')

def ajax_kredi_vergisi_hesaplama(request):
	print("ajax_kredi_vergisi_hesaplama runs with this request.POST")
	print(request.POST)
	try:
		request_post = fix_inputs(request.POST.copy())
		request_post['session'] = request.session.session_key
		form = KrediVergisiHesaplamaForm(request_post)
		if form.is_valid():
			form.save()
			loan =calculate_loan_details(Loan(interest=form.data['interest'],loan_type=LoanType.objects.get(loan_type=form.data['loan_type'])),\
										      principal=form.data['principal'], tenure=form.data['tenure'])
			loan.payment_plan = payment_plan(amount=loan.principal.principal, maturity=loan.tenure.tenure, interest=loan.interest)
			loan = add_taxes(loan)
			loan.payment_plan.nrows = loan.payment_plan.shape[0]
			tenure = loan.tenure.tenure

			at_least_one_loan = False
			min_interest = None
			loans = Loan.objects.filter(loan_type=loan.loan_type,tenure__gte=loan.tenure,principal__gte=loan.principal,active=True).order_by('bank','tenure','principal','interest').distinct('bank')
			if len(loans)>0:
				loans = Loan.objects.filter(id__in=loans).order_by('interest')
				min_interest = loans[0].interest
				tenure = loans[0].tenure.tenure
				loans = reorder_loans(loans)
				at_least_one_loan = True
			for l in loans:
				l = calculate_loan_details(l, loan.principal.principal, l.tenure.tenure)
			return render(request, 'tools/kredi_vergisi_hesaplama/hesaplama_sonucu.html', 
				{'loan':loan, 'loans':loans,
				'at_least_one_loan':at_least_one_loan,
				'principal':loan.principal.principal,
				'tenure':tenure,
				'min_interest':min_interest})
		else:
			print("form is not valid")
			print(form.errors)
	except Exception as e:
		print('error')
		print(str(e))
	return render(request, 'tools/global/calculation_failed_message.html')