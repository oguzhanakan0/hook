from django.urls import path
from . import views
from . import ajax_queries
urlpatterns = [
	# VIEWS
	path('kredi/hesaplama-araclari/', views.index, name='hesaplama_araclari'),
	path('kredi/hesaplama-araclari/basit-kredi-hesaplama/', views.basit_kredi_hesaplama, name='basit_kredi_hesaplama'),
	path('kredi/hesaplama-araclari/kredi-erken-kapama-hesaplama/', views.kredi_erken_kapama_hesaplama, name='kredi_erken_kapama_hesaplama'),
	path('kredi/hesaplama-araclari/kredi-pesinat-hesaplama/', views.kredi_pesinat_hesaplama, name='kredi_pesinat_hesaplama'),
	path('kredi/hesaplama-araclari/kredi-yapilandirma-hesaplama/', views.kredi_yapilandirma_hesaplama, name='kredi_yapilandirma_hesaplama'),
	path('kredi/hesaplama-araclari/kredi-karti-taksitli-nakit-avans-hesaplama/', views.kk_nakit_avans_hesaplama, name='kk_nakit_avans_hesaplama'),
	path('kredi/hesaplama-araclari/gecikme-faizi-hesaplama/', views.gecikme_faizi_hesaplama, name='gecikme_faizi_hesaplama'),
	path('kredi/hesaplama-araclari/kredi-erken-odeme-cezasi-hesaplama/', views.kredi_erken_odeme_cezasi_hesaplama, name='kredi_erken_odeme_cezasi_hesaplama'),
	path('kredi/hesaplama-araclari/kredi-vergisi-hesaplama/', views.kredi_vergisi_hesaplama, name='kredi_vergisi_hesaplama'),
	
	# AJAX QUERIES
	path('ajax/hesaplama-araclari/basit-kredi-hesaplama/', ajax_queries.ajax_basit_kredi_hesaplama, name='ajax_basit_kredi_hesaplama'),
	path('ajax/hesaplama-araclari/kredi-erken-kapama-hesaplama/', ajax_queries.ajax_kredi_erken_kapama_hesaplama, name='ajax_kredi_erken_kapama_hesaplama'),
	path('ajax/hesaplama-araclari/kredi-pesinat-hesaplama/', ajax_queries.ajax_kredi_pesinat_hesaplama, name='ajax_kredi_pesinat_hesaplama'),
	path('ajax/hesaplama-araclari/kredi-yapilandirma-hesaplama/', ajax_queries.ajax_kredi_yapilandirma_hesaplama, name='ajax_kredi_yapilandirma_hesaplama'),
	path('ajax/hesaplama-araclari/kredi-karti-taksitli-nakit-avans-hesaplama/', ajax_queries.ajax_kk_nakit_avans_hesaplama, name='ajax_kk_nakit_avans_hesaplama'),
	path('ajax/hesaplama-araclari/gecikme-faizi-hesaplama/', ajax_queries.ajax_gecikme_faizi_hesaplama, name='ajax_gecikme_faizi_hesaplama'),
	path('ajax/hesaplama-araclari/kredi-erken-odeme-cezasi-hesaplama/', ajax_queries.ajax_kredi_erken_odeme_cezasi_hesaplama, name='ajax_kredi_erken_odeme_cezasi_hesaplama'),
	path('ajax/hesaplama-araclari/kredi-vergisi-hesaplama/', ajax_queries.ajax_kredi_vergisi_hesaplama, name='ajax_kredi_vergisi_hesaplama'),
	
]