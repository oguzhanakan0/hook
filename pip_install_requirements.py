import sys
from pip._internal import main as pip_main

def install(package):
    pip_main(['install', package])

if __name__ == '__main__':
    with open('/hook/original_requirements.txt') as f:
        for line in f:
            install(line)
