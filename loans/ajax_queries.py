from django.contrib.sessions.models import Session
from django.http import HttpResponse
from userlogs.forms import SaveForm
import datetime as dt
import pprint
import json
import time

def save_save(request,action,time):
	try:
		save_dict = dict(zip(('loan','principal','tenure'),request.GET['loan_key'].split('&')))
		save_dict['time'] = time
		save_dict['action'] = action
		save_dict['session'] = request.session.session_key
		SaveForm(save_dict).save()
	except Exception as e:
		# print('save couldnt be saved!!')
		# print(e)
		# print('save_dict was ------------------>')
		# print(save_dict)
		pass

def write_liked_loan_to_session(request):
	"""
		This function writes a loan to a sessions 'favorite_loans' component.
		The request should be a GET request and it should have the following:
			- loan_id FK
			- tenure int
			- principal int
		Principal & tenure should be in the range of loan's principal and tenure bucket.
		In default this should not be a problem as user cannot favorite a loan with
		principal and tenure outside of the loan's range. There is no check for this currently.
	"""
	# print("write_liked_loan_to_session runs");
	# print(request.GET)
	try:
		# s = Session.objects.get(pk=request.session.session_key)
		saved_at = dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		# DEBUG SNIPPET
		try:
			print("==request.GET==")
			pprint.pprint(request.GET)
			print("==request.session==")
			print("session_key:"+request.session.session_key)
			print("expire_date:"+str(s.expire_date))
			print('trying to insert loan to favorites. here is the favorite loans BEFORE insertion:')
			try:
				pprint.pprint(s.get_decoded()['favorite_loans'])
			except:
				print('No favorite loans')
		except:
			pass
		# END DEBUG SNIPPET
		try:
			# print('try: insert favorite loan to session')
			request.session['favorite_loans']
		except:
			# print('except: insert favorite loan to session')
			request.session['favorite_loans'] = {}


		get = request.GET['loan_key']
		loan_key = get[:get.rfind('&')] # interesti loan_key'de istemiyoruz
		interest = get[(get.rfind('&')+1):]

		try:
			del request.session['removed_loans'][loan_key]
		except:
			pass
		
		request.session['favorite_loans'][loan_key] = {
			'favorited_at':saved_at,
			'interest':interest
		}
		save_save(request,action='save',time=saved_at)
		# pprint.pprint(request.session['favorite_loans'])
		return HttpResponse('success')
	except Exception as e:
		print('write_liked_loan_to_session failed')
		print(f"error was: {str(e)}")
		return HttpResponse('fail')

def remove_liked_loan_from_session(request):
	# print("remove_liked_loan_from_session runs");
	# print(request.GET)
	try:
		s = Session.objects.get(pk=request.session.session_key)
		# DEBUG SNIPPET
		try:
			print("==request.GET==")
			pprint.pprint(request.GET)
			print('try: remove favorite loan from session. favorite loans BEFORE deletion:')
			try:
				pprint.pprint(s.get_decoded())
			except:
				print('No favorite loans')
		except:
			pass
		# END DEBUG SNIPPET
		try:
			# print('loan removed from favorites. here is the current favorites:')
			removed_at = dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
			try:
				# print('try: remove loan from removed_loans')
				request.session['removed_loans']
			except:
				# print('except: remove loan from removed_loans')
				request.session['removed_loans'] = {}

			get = request.GET['loan_key']
			if get.count('&')>2: loan_key = get[:get.rfind('&')] 
			# interesti loan_key'de istemiyoruz
			# ustteki if clause loan_key'in su sekilde oldugunu varsayar: loan_id&principal&tenure+&interest
			else: loan_key=get
			# interest = get[(get.rfind('&')+1):]
			print(f'tring to remove: {loan_key}')

			# loan_key = request.GET['loan_key']
			request.session['removed_loans'][loan_key] = request.session['favorite_loans'][loan_key]
			request.session['removed_loans'][loan_key]['removed_at'] = removed_at

			del request.session['favorite_loans'][loan_key]
			save_save(request,action='unsave',time=removed_at)
		except:
			# print('except: loan not found in favorite loans LOOK UP! current favorites:')
			# pprint.pprint(s.get_decoded()['favorite_loans'])
			pass
		return HttpResponse('success')
	except:
		# print('remove_liked_loan_from_session failed')
		return HttpResponse('fail')

def get_liked_loans_from_session(request, http_raw = 'http', get='favorite_loans'):
	# print("get_liked_loans_from_session")
	try:
		s = Session.objects.get(pk=request.session.session_key)
		if http_raw=='http':
			return HttpResponse(json.dumps(s.get_decoded()[get]))
		elif http_raw=='raw':
			return s.get_decoded()[get]
	except:
		if http_raw=='http':
			return HttpResponse(json.dumps({}))
		elif http_raw=='raw':
			return {};
