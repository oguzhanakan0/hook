from django.db import models
from django.contrib import auth
from django.db.models import signals
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.contrib.postgres.fields.array import ArrayField
from wagtailmd.utils import MarkdownField
import uuid
import pandas as pd
"""
 ██╗      ██████╗  ██████╗ ██╗  ██╗██╗   ██╗██████╗     ████████╗ █████╗ ██████╗ ██╗     ███████╗███████╗
 ██║     ██╔═══██╗██╔═══██╗██║ ██╔╝██║   ██║██╔══██╗    ╚══██╔══╝██╔══██╗██╔══██╗██║     ██╔════╝██╔════╝
 ██║     ██║   ██║██║   ██║█████╔╝ ██║   ██║██████╔╝       ██║   ███████║██████╔╝██║     █████╗  ███████╗
 ██║     ██║   ██║██║   ██║██╔═██╗ ██║   ██║██╔═══╝        ██║   ██╔══██║██╔══██╗██║     ██╔══╝  ╚════██║
 ███████╗╚██████╔╝╚██████╔╝██║  ██╗╚██████╔╝██║            ██║   ██║  ██║██████╔╝███████╗███████╗███████║
 ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝            ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚══════╝╚══════╝╚══════╝
"""

class LoanType(models.Model):
	id                 = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	loan_type          = models.CharField(max_length = 3, unique = True)
	loan_type_long     = models.CharField(max_length = 100, unique = True)
	description        = models.TextField()
	slug               = models.SlugField()
	active             = models.BooleanField(default=True)
	default_principal  = models.PositiveIntegerField()
	default_tenure     = models.PositiveIntegerField()
	fa_icon	           = models.CharField(max_length=200)
	bg_image		   = models.ForeignKey('wagtailimages.Image',null=True,on_delete=models.SET_NULL,related_name='+')
	participation_name = models.CharField(max_length = 100)

	def __str__(self):
		return self.loan_type_long
		
class Principal(models.Model):
	id                   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	principal            = models.PositiveIntegerField(unique=True)
	active               = models.BooleanField(default=True)
	loan_types           = models.ManyToManyField(LoanType, related_name="principals", )

	def __str__(self):
	 return str(self.principal)

class InstallmentType(models.Model):
	id               = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	installment_type = models.CharField(max_length=40,unique=True)
	active           = models.BooleanField(default=True)

	def __str__(self):
		return str(self.installment_type)

class Program(models.Model):
	id               = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	program          = models.CharField(max_length=40,unique=True, default = "not_specified")
	active           = models.BooleanField(default=True)

	def __str__(self):
	 return str(self.program)

class Action(models.Model):
	id               = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	action           = models.CharField(max_length=40,unique=True, default = "INSERT")
	active           = models.BooleanField(default=True)

	def __str__(self):
	 return str(self.action)

from wagtail.images.models import Image

class Bank(models.Model):
	id                 = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	name               = models.CharField(max_length = 40, unique = True)
	description        = MarkdownField(blank=True)
	logo               = models.ForeignKey('wagtailimages.Image',null=True,on_delete=models.SET_NULL,related_name='+')
	logo_360_270       = models.ForeignKey('wagtailimages.Image',null=True,on_delete=models.SET_NULL,related_name='+')
	icon               = models.ForeignKey('wagtailimages.Image',null=True,on_delete=models.SET_NULL,related_name='+')
	slug               = models.SlugField(unique=True)
	active             = models.BooleanField(default=True)
	sponsored          = models.BooleanField(default=False)
	scrape_id		   = models.PositiveIntegerField(default=0)
	partnered          = models.BooleanField(default=False)
	participation	   = models.BooleanField(default=False)

	def __str__(self):
	 return self.name

class Insurance(models.Model):
	id                = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	insurance_type    = models.CharField(max_length = 40)
	description       = models.TextField()
	active            = models.BooleanField(default=True)

	def __str__(self):
	 return self.insurance_type

class Tenure(models.Model):
	id     = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	tenure = models.PositiveIntegerField(unique = True)
	active = models.BooleanField(default=True)
	loan_types = models.ManyToManyField(LoanType, related_name="tenures", )

	def convert_str(self):
		if (self.tenure >= 12 and self.tenure%12==0):
			return str(self.tenure) + " ay (" +str(int(self.tenure/12))+" yıl)"
		else:
			return str(self.tenure) + " ay"
	readable_tenure = property(convert_str)

	def __str__(self):
		return str(self.tenure)

class Critical(models.Model):
	loan_type          = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	tenure             = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'tenure', to_field = 'tenure')
	principal	       = models.ForeignKey(Principal, on_delete=models.PROTECT, db_column = 'principal', to_field= 'principal')
	critical_mobile    = models.BooleanField(default=False)

	def __str__(self):
		return str(self.loan_type)+'-'+str(self.principal)+'-'+str(self.tenure)

class LoanHeader(models.Model):
	header_type        = models.CharField(unique=True ,max_length=40)
	text               = models.CharField(max_length=40)
	fa_icon	           = models.CharField(max_length=200)
	color			   = models.CharField(max_length=40)
	priority           = models.PositiveIntegerField(unique=True)
	
	def __str__(self):
		return str(self.header_type)

"""
 
 ██████╗ ███████╗ ██████╗ ██╗   ██╗██╗      █████╗ ██████╗     ████████╗ █████╗ ██████╗ ██╗     ███████╗███████╗
 ██╔══██╗██╔════╝██╔════╝ ██║   ██║██║     ██╔══██╗██╔══██╗    ╚══██╔══╝██╔══██╗██╔══██╗██║     ██╔════╝██╔════╝
 ██████╔╝█████╗  ██║  ███╗██║   ██║██║     ███████║██████╔╝       ██║   ███████║██████╔╝██║     █████╗  ███████╗
 ██╔══██╗██╔══╝  ██║   ██║██║   ██║██║     ██╔══██║██╔══██╗       ██║   ██╔══██║██╔══██╗██║     ██╔══╝  ╚════██║
 ██║  ██║███████╗╚██████╔╝╚██████╔╝███████╗██║  ██║██║  ██║       ██║   ██║  ██║██████╔╝███████╗███████╗███████║
 ╚═╝  ╚═╝╚══════╝ ╚═════╝  ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝       ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚══════╝╚══════╝╚══════╝
                                                                                                                
 
"""

class LoanAbs(models.Model):
	# Foreign Keys
	loan_type          = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	bank               = models.ForeignKey(Bank, on_delete=models.PROTECT, db_column = 'bank_name', to_field= 'name')
	tenure             = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'tenure', to_field = 'tenure')
	principal	       = models.ForeignKey(Principal, on_delete=models.PROTECT, db_column = 'principal', to_field= 'principal')
	# Other Fields
	name               = models.CharField(max_length = 100)
	slug               = models.SlugField()
	description        = models.TextField()
	interest           = models.FloatField()
	is_base 		   = models.BooleanField(default=False)
	is_featured        = models.BooleanField(default=False)
	feature_order      = models.PositiveIntegerField(null=True,blank=True)
	header_type        = models.ForeignKey(LoanHeader, on_delete=models.PROTECT, db_column = 'header_type', to_field= 'header_type',null=True,blank=True)
	last_interest_change = models.DateTimeField(null=True, blank=True, db_column = 'last_interest_change')
	last_interest        = models.FloatField(null=True, blank=True, db_column = 'last_interest')
	# Log Fields
	upd_user           = models.ForeignKey(auth.get_user_model(), on_delete=models.PROTECT, db_column = '_user', to_field= 'username')
	upd_program        = models.ForeignKey(Program, on_delete=models.PROTECT, db_column = '_program', to_field= 'program')
	active             = models.BooleanField(default=True)

	class Meta:
		abstract = True

class Loan(LoanAbs):
	id            	     = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	upd_time             = models.DateTimeField(auto_now=True, db_column = '_upd_time')

	def set_bsmv_kkdf(self):
		self.bsmv, self.kkdf = (0, 0) if self.loan_type.loan_type=='MOR' else (0.05, 0.15)

	def set_installment(self):
		# print(f'self.principal.principal: {self.principal.principal}, self.tenure.tenure:{self.tenure.tenure}, self.bsmv:{self.bsmv}, self.kkdf:{self.kkdf}')
		total_interest = self.interest * (1+self.bsmv+self.kkdf)
		self.installment = round((total_interest * self.principal.principal) / (1 - (1 + total_interest) ** (-1 * self.tenure.tenure)),2)
	
	def set_loan_details(self, principal=None, tenure=None):
		if principal: self.principal = Principal(principal=principal)
		if tenure: self.tenure = Tenure(tenure=tenure)
		self.set_bsmv_kkdf()
		# calculate installment
		self.set_installment()
		self.allocation_fee = round(self.principal.principal*0.005,2)
		self.total_payment = round(self.installment * self.tenure.tenure +  self.allocation_fee,2)
		self.total_interest = round(self.total_payment - self.principal.principal - self.allocation_fee,2)

	def set_payment_plan(self):
		# total_interest = interest + interest * kkdf + interest * bsmv
		# monthly_payment = (total_interest * amount) / (1 - (1 + total_interest) ** (-1 * maturity))
		payment_plan = pd.DataFrame(index=range(self.tenure.tenure), columns=['month','monthly_pay','principal_before','principal_paid','interest_paid','kkdf','bsmv','principal_left'])

		for i in range(self.tenure.tenure):
			if i == 0:
				principal_before = self.principal.principal
			else:
				principal_before = payment_plan.loc[i-1, 'principal_left']

			interest_paid = principal_before * self.interest
			kkdf_paid = interest_paid * self.kkdf
			bsmv_paid = interest_paid * self.bsmv
			principal_paid = self.installment - interest_paid - kkdf_paid - bsmv_paid
			principal_left = principal_before - principal_paid

			payment_plan.loc[i] = [i+1,self.installment, principal_before, principal_paid, interest_paid, kkdf_paid, bsmv_paid, principal_left]
			payment_plan = payment_plan.astype('float')

		payment_plan.drop('principal_before', axis=1, inplace=True)
		payment_plan = payment_plan.round(2)
		payment_plan.iloc[-1,-1]=0
		payment_plan.columns=['Ay','Taksit','Anapara','Faiz','KKDF','BSMV','Kalan Ödeme']
		self.payment_plan=payment_plan
		self.payment_plan.nrows=payment_plan.shape[0]

	def save(self, *args, **kwargs):
		if self.loan_type not in self.tenure.loan_types.all():
			raise ValueError('Invalid Loan Type - Tenure combination.')
		else:
			super(Loan, self).save(*args, **kwargs)

	# def __str__(self):
	# 	return self.bank.name + "-"+str(self.loan_type)+"-"+str(self.principal) +"-"+str(self.tenure)

	class Meta:
		constraints = [
        	models.UniqueConstraint(fields=('slug', 'tenure','principal'), name="cons1"),
        	# models.UniqueConstraint(fields=('bank', 'loan_type','tenure','principal'), name="cons2"),
        ]
		# unique_together = ('slug', 'tenure','principal')

	@property
	def get_link(self):
		return Link.objects.get(bank=self.bank,loan_type=self.loan_type)
	

class LinkAbs(models.Model):
	bank               = models.ForeignKey(Bank, on_delete=models.PROTECT, db_column = 'bank_name', to_field= 'name')
	url                = models.TextField()
	loan_type          = models.ForeignKey(LoanType, on_delete=models.CASCADE, db_column = 'loan_type', to_field = 'loan_type')
	upd_user           = models.ForeignKey(auth.get_user_model(), on_delete=models.PROTECT, db_column = '_user', to_field= 'username')
	upd_program        = models.ForeignKey(Program, on_delete=models.PROTECT, db_column = '_program', to_field= 'program')
	active             = models.BooleanField(default=True)

	class Meta:
		abstract = True

class Link(LinkAbs):
	id                 = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	upd_time           = models.DateTimeField(auto_now=True, db_column = '_upd_time')

	class Meta:
		unique_together = ['loan_type', 'bank']

	def __str__(self):
		return self.url

class LoanDetailAbs(models.Model):
	# Foreign Keys
	insurance	       = models.ForeignKey(Insurance, on_delete=models.PROTECT)
	installment_type   = models.ForeignKey(InstallmentType, on_delete=models.PROTECT)
	# Log Fields
	upd_user           = models.ForeignKey(auth.get_user_model(), on_delete=models.PROTECT, db_column = '_user', to_field= 'username')
	upd_program        = models.ForeignKey(Program, on_delete=models.PROTECT, db_column = '_program', to_field= 'program')
	active             = models.BooleanField(default=True)

	class Meta:
		abstract = True

class LoanDetail(LoanDetailAbs):
	id                 = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	upd_time           = models.DateTimeField(auto_now=True, db_column = '_upd_time')
	loan               = models.OneToOneField(Loan, on_delete=models.PROTECT)

	def __str__(self):
		return str(self.loan.bank)+"-"+str(self.loan.loan_type)+"-"+self.loan.name

"""
 
 ██╗      ██████╗  ██████╗     ████████╗ █████╗ ██████╗ ██╗     ███████╗███████╗
 ██║     ██╔═══██╗██╔════╝     ╚══██╔══╝██╔══██╗██╔══██╗██║     ██╔════╝██╔════╝
 ██║     ██║   ██║██║  ███╗       ██║   ███████║██████╔╝██║     █████╗  ███████╗
 ██║     ██║   ██║██║   ██║       ██║   ██╔══██║██╔══██╗██║     ██╔══╝  ╚════██║
 ███████╗╚██████╔╝╚██████╔╝       ██║   ██║  ██║██████╔╝███████╗███████╗███████║
 ╚══════╝ ╚═════╝  ╚═════╝        ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚══════╝╚══════╝╚══════╝
                                                                                
 
"""
# class LoanRequest(models.Model):
# 	request_id   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
# 	loan_type    = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
# 	tenure       = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'tenure', to_field = 'tenure')
# 	principal    = models.PositiveIntegerField()
# 	request_time = models.DateTimeField(auto_now_add=True)

# 	@property
# 	def available_tenures(self):
# 		return self.loan_type.tenures.all()

class LoanLog(LoanAbs):
	# Log Fields
	id                 = models.UUIDField(primary_key=True,default=uuid.uuid1, editable=False)
	loan_id            = models.CharField(max_length=36)
	upd_time           = models.DateTimeField(db_column = '_upd_time')
	upd_action		   = models.ForeignKey(Action, on_delete=models.PROTECT, db_column = '_action', to_field= 'action')

	def __init__(self, loan=None, action=None):
		super(LoanLog, self).__init__()
		self.loan_id        = str(loan.pk)
		self.loan_type      = loan.loan_type
		self.bank           = loan.bank
		self.tenure         = loan.tenure
		self.principal      = loan.principal
		self.name           = loan.name
		self.slug           = loan.slug
		self.description    = loan.description
		self.interest       = loan.interest
		self.active         = loan.active

		self.upd_user       = loan.upd_user
		self.upd_program    = loan.upd_program
		self.upd_time       = loan.upd_time
		self.upd_action     = action

	def __str__(self):
		return "log-"+self.bank.name + "-" +self.name

class LoanDetailLog(LoanDetailAbs):
	# Log Fields
	id                 = models.UUIDField(primary_key=True,default=uuid.uuid1, editable=False)
	upd_action		   = models.ForeignKey(Action, on_delete=models.PROTECT, db_column = '_action', to_field= 'action')
	loan_detail_id     = models.CharField(max_length=36)
	loan_id            = models.CharField(max_length=36)
	upd_time           = models.DateTimeField(db_column = '_upd_time')

	def __init__(self, loan_detail=None, action=None):
		super(LoanDetailLog, self).__init__()
		self.loan_detail_id   = str(loan_detail.pk)
		self.loan_id          = str(loan_detail.loan.id)
		self.insurance        = loan_detail.insurance
		self.installment_type = loan_detail.installment_type
		self.upd_program      = loan_detail.upd_program
		self.upd_user         = loan_detail.upd_user
		self.upd_time         = loan_detail.upd_time
		self.active           = loan_detail.active

		self.upd_action       = action

	def __str__(self):
		return "log-"+str(self.loan_detail_id)

class LinkLog(LinkAbs):
	# Log Fields
	id                 = models.UUIDField(primary_key=True,default=uuid.uuid1, editable=False)
	link_id            = models.CharField(max_length=36)
	upd_time           = models.DateTimeField(db_column = '_upd_time')
	upd_action		   = models.ForeignKey(Action, on_delete=models.PROTECT, db_column = '_action', to_field= 'action')

	def __init__(self, link=None, action=None):
		super(LinkLog, self).__init__()
		self.link_id          = str(link.pk)
		
		self.bank             = link.bank
		self.loan_type        = link.loan_type
		self.url              = link.url
		self.active           = link.active

		self.upd_program      = link.upd_program
		self.upd_user         = link.upd_user
		self.upd_time         = link.upd_time
		self.upd_action       = action

	def __str__(self):
		return "log-"+self.bank.name+"-"+str(self.loan_type)


class LoanSummary(models.Model):
	"""
	View table that is produced everyday and can be used directly in frontend.
	'bank','loan_name','min_principal','max_principal','min_tenure','max_tenure','interest','url'
	"""
	bank           = models.ForeignKey(Bank, on_delete=models.PROTECT)
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field= 'loan_type')
	min_principal  = models.ForeignKey(Principal, on_delete=models.PROTECT, db_column = 'min_principal', to_field= 'principal',related_name='+')
	max_principal  = models.ForeignKey(Principal, on_delete=models.PROTECT, db_column = 'max_principal', to_field= 'principal',related_name='+')
	min_tenure     = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'min_tenure', to_field= 'tenure',related_name='+')
	max_tenure     = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'max_tenure', to_field= 'tenure',related_name='+')
	min_interest   = models.FloatField()
	max_interest   = models.FloatField()
	tenures        = models.ManyToManyField(Tenure, related_name="loan_summaries", )
	date           = models.DateField(auto_now=True)

class LoanMatrix(models.Model):
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field= 'loan_type')
	tenure         = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'min_tenure', to_field= 'tenure',related_name='+')
	principal      = models.ForeignKey(Principal, on_delete=models.PROTECT, db_column = 'min_principal', to_field= 'principal',related_name='+')
	min_interest   = models.FloatField(null=True, blank=True)
	loan_1         = models.ForeignKey(Loan, on_delete=models.CASCADE,related_name='+', blank=True, null=True)
	loan_2         = models.ForeignKey(Loan, on_delete=models.CASCADE,related_name='+', blank=True, null=True)
	loan_3         = models.ForeignKey(Loan, on_delete=models.CASCADE,related_name='+', blank=True, null=True)
	date           = models.DateField(auto_now=True)

class LoanMatrixTable(models.Model):
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field= 'loan_type')
	table_html     = models.TextField()
	date           = models.DateField(auto_now=True)

class InterestBreakTable(models.Model):
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field= 'loan_type')
	bank           = models.ForeignKey(Bank, on_delete=models.PROTECT)
	table_html     = models.TextField()
	date           = models.DateField(auto_now=True)
	

"""
 
 ███████╗██╗ ██████╗ ███╗   ██╗ █████╗ ██╗     ███████╗
 ██╔════╝██║██╔════╝ ████╗  ██║██╔══██╗██║     ██╔════╝
 ███████╗██║██║  ███╗██╔██╗ ██║███████║██║     ███████╗
 ╚════██║██║██║   ██║██║╚██╗██║██╔══██║██║     ╚════██║
 ███████║██║╚██████╔╝██║ ╚████║██║  ██║███████╗███████║
 ╚══════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═╝╚══════╝╚══════╝
                                                       
 
"""

# Loanlog
@receiver(signals.post_save, sender=Loan)
def log_loan(sender, instance, created, **kwargs):
	action_str = "INSERT" if created else "UPDATE"
	upd_action = get_object_or_404(Action, action=action_str)
	LoanLog(loan=instance,action=upd_action).save()
	print('loan change logged')

@receiver(signals.pre_delete, sender=Loan)
def log_loan_delete(sender, instance, **kwargs):
	action_str = "DELETE"
	upd_action = get_object_or_404(Action, action=action_str)
	instance.upd_time = timezone.now()
	LoanLog(loan=instance,action=upd_action).save()
	print('loan deletion logged')

# Loandetaillog
@receiver(signals.post_save, sender=LoanDetail)
def log_loan_detail(sender, instance, created, **kwargs):
	action_str = "INSERT" if created else "UPDATE"
	upd_action = get_object_or_404(Action, action=action_str)
	LoanDetailLog(loan_detail=instance,action=upd_action).save()
	print('loan detail change logged')

@receiver(signals.pre_delete, sender=LoanDetail)
def log_loan_detail_delete(sender, instance, **kwargs):
	action_str = "DELETE"
	upd_action = get_object_or_404(Action, action=action_str)
	instance.upd_time = timezone.now()
	LoanDetailLog(loan_detail=instance,action=upd_action).save()
	print('loan detail deletion logged')

# Linklog
@receiver(signals.post_save, sender=Link)
def log_link(sender, instance, created, **kwargs):
	print('updating')
	action_str = "INSERT" if created else "UPDATE"
	upd_action = get_object_or_404(Action, action=action_str)
	LinkLog(link=instance,action=upd_action).save()
	print('link change logged')

@receiver(signals.pre_delete, sender=Link)
def log_link_delete(sender, instance, **kwargs):
	action_str = "DELETE"
	upd_action = get_object_or_404(Action, action=action_str)
	instance.upd_time = timezone.now()
	LinkLog(link=instance,action=upd_action).save()
	print('loan detail deletion logged')
