# Generated by Django 3.1.2 on 2020-11-06 15:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0112_loantype_bg_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='loantype',
            name='slug',
            field=models.SlugField(default='kredi'),
            preserve_default=False,
        ),
    ]
