# Generated by Django 3.1.2 on 2020-10-25 13:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0101_auto_20201025_1559'),
    ]

    operations = [
        migrations.RenameField(
            model_name='loan',
            old_name='loan_header',
            new_name='header_type',
        ),
        migrations.RenameField(
            model_name='loanlog',
            old_name='loan_header',
            new_name='header_type',
        ),
    ]
