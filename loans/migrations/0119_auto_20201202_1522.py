# Generated by Django 3.1.2 on 2020-12-02 12:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0118_bank_partnered'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loanmatrix',
            name='loan_1',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='loans.loan'),
        ),
        migrations.AlterField(
            model_name='loanmatrix',
            name='loan_2',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='loans.loan'),
        ),
        migrations.AlterField(
            model_name='loanmatrix',
            name='loan_3',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='loans.loan'),
        ),
    ]
