from django.urls import path
from django.urls import include, re_path
# from .views import requery_loan_detail

from . import views
from . import ajax_queries

urlpatterns = [
    # path('', views.loan_list, name='loan_list'),
    path('sorgu/kredi-tipi=<loan_type>&tutar=<principal>&vade=<tenure>', views.loan_query, name='loan_list'),
    # path('loan_detail', views.loan_detail, name='loan_detail'),
    path('detay/kredi-adi=<slug>&tutar=<principal>&vade=<tenure>', views.loan_detail, name='loan_detail'), # loan detail page with slug name 
    path('detay/kredi-tipi=<loan_type>&banka-adi=<bank_slug>&tutar=<principal>&vade=<tenure>', views.loan_detail_b, name='loan_detail_b'), # loan detail page with bank name
    path('yonlendir/kredi-id-numaralari=<loan_keys>&basvuru-numaralari=<application_ids>', views.redirect_to_bank, name='redirect_to_bank'), # loan detail page with bank name
    # AJAX QUERIES
    path('ajax/write-liked-loan-to-session/', ajax_queries.write_liked_loan_to_session, name='ajax_write_liked_loan_to_session'),  # <-- this one here
    path('ajax/get-liked-loans-from-session/', ajax_queries.get_liked_loans_from_session, name='ajax_get_liked_loans_from_session'),  # <-- this one here
    path('ajax/remove-liked-loan-from-session/', ajax_queries.remove_liked_loan_from_session, name='ajax_remove_liked_loan_from_session'),  # <-- this one here
    path('show-detail', views.show_detail, name='show_detail'),
    path('apply', views.apply, name='apply'),
    path('set-success', views.set_success, name='set_success'),
]