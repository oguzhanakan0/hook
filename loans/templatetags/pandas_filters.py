from django import template
from babel.numbers import format_currency
import locale
import datetime
from django.utils import timezone
today = datetime.date.today()
# locale.setlocale(locale.LC_ALL, 'tr_TR')
today = today.strftime("%d %B %Y")
register = template.Library()

@register.filter
def get(row, key):
	return row[key]

@register.filter
def as_currency_plain(value):
	# print('trying to format '+str(value)+' of type '+str(type(value)))
	try:
		return format_currency(value, 'TRY', u'#,##0.00 ¤', locale='tr_TR')
	except:
		return "-"

@register.filter
def as_currency_plain_short(value):
	# print('trying to format '+str(value)+' of type '+str(type(value)))
	try:
		return format_currency(value, 'TRY', u'#,##0', locale='tr_TR').split(',')[0]
	except:
		return "-"

@register.simple_tag
def as_currency(value, left_right = None):
	_res = format_currency(value, 'TRY', u'#,##0.00 ¤', locale='tr_TR')
	if left_right == 'left':
		return _res.split(',')[0]
	elif left_right == 'right':
		return ','+_res.split(',')[1]
	else:
		return _res

@register.filter
def as_percentage(value):
	try:
		return "%{:.2f}".format(value*100).replace('.',',')
	except:
		return "-"
		# print("cant convert "+str(value)+" of value "+str(type(value)))

@register.filter
def as_int(value):
	try:
		return int(value)
	except:
		return 0

@register.filter
def as_float(value):
	try:
		return float(value)
	except:
		return 0

@register.filter
def leng(value):
	try:
		return len(value)
	except:
		return 0

@register.filter
def add_iyelik(value):
	try:
		if   value[-1] in ("e","i"): return value+"'nin"
		elif value[-1] in ("a","ı"): return value+"'nın"
		elif value[-1] in ("o","u"): return value+"'nun"
		elif value[-1] in ("ö","ü"): return value+"'nün"
		else: return value
	except:
		return value

@register.filter
def add_tenure_suffix(value):
	try:
		if int(value) % 12 == 0:
			return str(int(value)) + " ay (" + str(int(int(value)/12)) +" yıl)"
		else:
			return str(int(value)) + " ay"
	except:
		return '-'

@register.filter
def add_tenure_suffix_plain(value):
	try:
		return str(int(value)) + " ay"
	except:
		return '-'

@register.filter
def replace_blanks(value):
	return value.replace(' ','@')

@register.filter(name='dict_key')
def dict_key(d, k):
    '''Returns the given key from a dictionary.'''
    return d[k]

@register.simple_tag
def weight(value, nrows):
	return str(int(round(1-(value/nrows/3),2)*100))+"%"

@register.simple_tag
def bugun(format="DEFAULT"):
	return today

@register.filter
def time_till_today(dat):
	try:
		delta = timezone.now() - dat
		if delta.days==0:
			return 'bugün'
		else:
			if delta.days<7:
				return str(delta.days)+' gün önce'
			else:
				return str(int(delta.days/7))+' hafta önce'
	except:
		return '-'


@register.simple_tag
def concat(c1, c2, c3=None):
	if c3==None:
		return str(c1)+str(c2)
	else:
		return str(c1)+str(c2)+str(c3)

@register.filter
def add(c1,c2):
	return str(c1)+str(c2)

@register.filter
def minus(c1,c2):
	return c1-c2

@register.filter
def absolute(c):
	return abs(c)

@register.simple_tag
def concat_interests(i1,i2):
	if i1==i2:
		return "%{:.2f}".format(i1*100).replace('.',',')
	else:
		return "%{:.2f}".format(i1*100).replace('.',',') + " - " + "%{:.2f}".format(i2*100).replace('.',',')

@register.simple_tag
def subtract(v1,v2,number_format='default'):
	if(number_format=='currency'):
		return as_currency_plain(v1-v2)
	elif(number_format=='tenure'):
		return add_tenure_suffix_plain(v1-v2)
	elif(number_format=='percentage'):
		return as_percentage(v1-v2)
	else:
		return int(v1)-int(v2)

@register.filter
def convert_loan_type(value):
	return {
	'GPL':'İhtiyaç Kredisi',
	'CAR':'Taşıt Kredisi 0 Km',
	'CA2': 'Taşıt Kredisi 2. El',
	'MOR': 'Konut Kredisi'}[value]

@register.filter
def get_slug_loan_type(value):
	return {
	'GPL':'ihtiyac-kredisi',
	'CAR':'tasit-kredisi-0km',
	'CA2': 'tasit-kredisi-2el',
	'MOR': 'konut-kredisi'}[value]

@register.filter(name='zip')
def zip_lists(a, b):
  return zip(a.split('_'), b.split('_'))

@register.filter(name='zip2')
def zip_lists2(a, b):
  return zip(a,b)