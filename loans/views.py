from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Max
from .models import Loan, Principal, Bank, LoanType, Tenure, LoanHeader, InterestBreakTable
from userlogs.forms import LoanRequestForm, LoanDetailRequestForm, ApplicationForm
from userlogs.models import LoanRequest, SubscriptionType, Application
from .ajax_queries import get_liked_loans_from_session
from home.cart_views import transfer_liked_loans_to_list
# from loantype_pages.views import handle_loan_request_form
from django.urls import reverse
from scripts.py.payment_plan import payment_plan
from datetime import date as dt
import pandas as pd
import pprint
import json

interest_break_tables_max_date = InterestBreakTable.objects.aggregate(Max('date'))['date__max']
interest_break_tables = InterestBreakTable.objects.filter(date=interest_break_tables_max_date)
cheapest_header = LoanHeader.objects.get(header_type='cheapest')

def handle_loan_request_form(request):
    request_post = request.POST.dict()
    request_post['session'] = request.session.session_key
    request_post['principal'] = request_post['principal'].replace('.','')
    form = LoanRequestForm(request_post)
    if (form.is_valid()):
        form.save()
        redirect_to = reverse('loan_list',kwargs={'principal':request_post['principal'].replace('.',''),'loan_type':request_post['loan_type'],'tenure':request_post['tenure'],})
        return HttpResponseRedirect(redirect_to)
    else:
        print("======= FORM IS NOT VALID =========")
        print(request_post)
        print(form.errors)
        return HttpResponse('form is not valid: errors: <br>'+json.dumps(request_post)+'<br>'+json.dumps(form.errors))

def show_subscription_notification(request, subscription_type):
	try:
		if request.session['no-notification']: return False
		else: return True
	except:
		return True

def calculate_installment(principal, tenure, interest, bsmv=0.05, kkdf=0.15):
	total_interest = interest + interest * kkdf + interest * bsmv
	installment = (total_interest * principal) / (1 - (1 + total_interest) ** (-1 * tenure))
	return round(installment,2)

def calculate_loan_details(loan, principal, tenure):
	loan.principal = Principal(principal=principal)
	loan.tenure = Tenure(tenure=tenure)
	bsmv, kkdf = (0, 0) if loan.loan_type.loan_type=='MOR' else (0.05, 0.15)
	loan.installment = calculate_installment(principal, tenure, loan.interest, bsmv, kkdf)
	loan.allocation_fee = round(principal*0.005,2)
	loan.total_payment = round(loan.installment * tenure +  loan.allocation_fee,2)
	loan.total_interest = round(loan.total_payment - principal - loan.allocation_fee,2)
	# loan.interest_str = "{:.2%}".format(loan.interest)
	return loan

def reorder_loans(loans):
	sponsored = LoanHeader.objects.get(header_type='sponsored')
	most_viewed = LoanHeader.objects.get(header_type='most_viewed')
	cheapest = LoanHeader.objects.get(header_type='cheapest')
	ordered_loans = [e for e in loans if e.bank.partnered]+[e for e in loans if e.bank.partnered==False]
	ordered_loans = [e for e in ordered_loans if e.header_type==sponsored]+\
					[e for e in ordered_loans if e.header_type==cheapest]+\
					[e for e in ordered_loans if e.header_type==most_viewed]+\
					[e for e in ordered_loans if e.header_type==None]
	ordered_loans[0].is_first = True
	return ordered_loans

def cheaper_gpl_exists(loan_type,tenure,principal,min_interest):
	try:
		if loan_type in ("CAR","CA2"):
			loans = Loan.objects.filter(loan_type="GPL",tenure__gte=tenure,principal__gte=principal,active=True).order_by('bank','tenure','principal').distinct('bank')
			loans = Loan.objects.filter(id__in=loans).order_by('interest')
			if loans[0].interest < min_interest:
				return True
	except:
		pass
	return False

def get_superior_loan_of(loan):
	superior_loan = None
	better_loans = Loan.objects.filter(tenure__gte=loan.tenure.tenure,principal__gte=loan.principal.principal,loan_type=loan.loan_type,active=True)\
				   .order_by('bank','tenure','principal','interest').distinct('bank')
	better_loans = Loan.objects.filter(id__in=better_loans, interest__lt=loan.interest).order_by('interest')

	if len(better_loans)>0:
		for better_loan in better_loans:
			print(f"better loan: {better_loan.bank.name}, {better_loan.principal.principal}, {better_loan.interest}")
		superior_loan = better_loans.first()
		superior_loan.set_loan_details(loan.principal.principal, loan.tenure.tenure)
	return superior_loan

# Create your views here.
def loan_query(request, loan_type, principal, tenure):

	if ((request.method == "GET") | ((request.method=="POST") & ("requery-button" in request.POST))):
		if ("requery-button" in request.POST):
			return handle_loan_request_form(request)
		else:
			pprint.pprint(request.GET)
		# print("#"*10+" 4 "+"#"*10)
		loans = Loan.objects.filter(loan_type=loan_type,tenure__gte=tenure,principal__gte=principal,active=True).order_by('bank','tenure','principal','interest').distinct('bank')

		_cheaper_gpl_exists = False
		if len(loans)>0:
			loans = Loan.objects.filter(id__in=loans).order_by('interest')

			# Start: En Ucuz Faiz oranini ilistir
			for loan in loans:
				if loan.interest==loans[0].interest: loan.header_type=cheapest_header;
				else: break
			# End: En Ucuz Faiz oranini ilistir
			loans = reorder_loans(loans)
			_cheaper_gpl_exists = cheaper_gpl_exists(loan_type,tenure,principal,loans[0].interest)
		
		available_tenures = LoanType.objects.get(loan_type=loan_type).tenures.all().exclude(tenure=0).order_by('tenure')

		try:
			session_email = request.session['email']
		except:
			session_email = None
		# Alttaki iki satirin ne ise yaradigini cozemedim. Hata alinmaya baslarsa bakilabilir. Simdilik dursun, bir dahaki gorusunde silersin
		# if loan_type in ("CAR","CA2"):
		# 	Loan.objects.filter(loan_type="GPL",tenure__gte=tenure,principal__gte=principal).order_by('bank','tenure','principal').distinct('bank')
		for loan in loans:
			loan.set_loan_details(int(principal), int(tenure))
			# loan = calculate_loan_details(loan, int(principal), int(tenure))
		return render(request, 'loans/loan_query_page.html', {
			'loans':loans,
			'principal':principal,
			'tenure':tenure,
			'available_tenures':available_tenures,
			'loan_type':loan_type,
			'at_least_one_loan':len(loans)>0, 
			'cheaper_gpl_exists': _cheaper_gpl_exists,
			'subscription_type':loan_type,
			'show_subscription_notification':show_subscription_notification(request,subscription_type=loan_type),
			'session_email':session_email,
			'form':LoanRequestForm(initial={'principal': principal,'tenure':tenure})})


def loan_detail(request, slug, principal, tenure):
	if (request.method == "GET"):
		loan = Loan.objects.filter(tenure__gte=tenure,principal__gte=principal,slug=slug,active=True).order_by('tenure','principal').first()
		loan.set_loan_details(int(principal), int(tenure))
		loan.set_payment_plan()
		superior_loan = get_superior_loan_of(loan)

		try:interest_break_table = interest_break_tables.get(loan_type=loan.loan_type,bank=loan.bank).table_html
		except: interest_break_table=None

		print('INTEREST BREAK TABLE')
		print(interest_break_tables_max_date)
		print(interest_break_table)

		try:
			session_email = request.session['email']
		except:
			session_email = None

		return render(request, 'loans/loan_detail_page.html', {
			'loan': loan,
			'principal':principal,
			'tenure':tenure,
			'superior_loan':superior_loan,
			'cheaper_gpl_exists': cheaper_gpl_exists(loan.loan_type.loan_type,tenure,principal,loan.interest),
			# 'form':LoanRequestForm(initial={'principal': principal,'tenure':tenure}),
			'interest_break_table':interest_break_table,
			'show_subscription_notification':show_subscription_notification(request,subscription_type=loan.loan_type.loan_type),
			'session_email':session_email,
			})

def loan_detail_b(request, loan_type, bank_slug, principal, tenure):
	if (request.method == "GET"):
		loan = Loan.objects.filter(tenure__gte=tenure,principal__gte=principal,bank=Bank.objects.get(slug=bank_slug),loan_type=loan_type,active=True).order_by('tenure','principal','interest').first()
		loan.set_loan_details(int(principal), int(tenure))
		loan.set_payment_plan()
		superior_loan = get_superior_loan_of(loan)
		
		try:
			interest_break_table = interest_break_tables.get(loan_type=loan.loan_type,bank=loan.bank).table_html
		except:
			interest_break_table=None

		try:
			session_email = request.session['email']
		except:
			session_email = None

		return render(request, 'loans/loan_detail_page.html', {
			'loan': loan,
			'principal':principal,
			'tenure':tenure,
			'superior_loan':superior_loan,
			'cheaper_gpl_exists': cheaper_gpl_exists(loan.loan_type.loan_type,tenure,principal,loan.interest),
			# 'form':LoanRequestForm(initial={'principal': principal,'tenure':tenure}),
			'interest_break_table':interest_break_table,
			'show_subscription_notification':show_subscription_notification(request,subscription_type=loan_type),
			'session_email':session_email,
			})


def redirect_to_loan_detail_page(request_post):
	request_post['principal'] = request_post['principal'].replace('.','')
	redirect_to = reverse('loan_detail',
				kwargs={
					'slug':request_post.get('slug'),
					'principal':request_post.get('principal'),
					'tenure':request_post.get('tenure'),
				})
	return HttpResponseRedirect(redirect_to)


def redirect_to_bank(request, loan_keys, application_ids):
	if (request.method == "GET"):
		# print("========== loan_keys =========")
		# print(loan_keys)
		loan_keys = loan_keys.split('+')
		loans = {}
		for loan_key in loan_keys:
			loan_props = loan_key.split('&')
			loans[loan_props[0]]={'principal':int(loan_props[1]),'tenure':int(loan_props[2])}
		
		loans_lst = []
		for loan_id in loans.keys():
			loan = Loan.objects.get(id=loan_id)
			if loan.bank.partnered:
				loan = calculate_loan_details(loan, principal=loans[loan_id]['principal'], tenure=loans[loan_id]['tenure'])
				loan.apply_link = loan.get_link
				loans_lst.append(loan)
		
		# print("========== len(loans_lst) =========")
		# print(str(len(loans_lst)))

		return render(request, 'loans/loan_apply_page.html', {'loans':loans_lst,'multiple_applications':1 if len(loans_lst)>1 else 0,'application_ids':application_ids.split('+')})
	else:
		return render(request, 'loans/invalid_loan.html', {})

def save_application(request, loan_key, session, request_source=None):
	try:
		request_post = dict(zip(('loan','principal','tenure'),loan_key.split('&')))
		request_post['request_source'] = request.POST['request_source'] if not request_source else request_source
		request_post['session'] = session.session_key
		form = ApplicationForm(request_post)
		form.save()
		return form.instance
	except Exception as e:
		"========UYYY======"
		print(e)
		print('request_post:')
		print(request_post)
		return None

def apply(request):
	loan_keys = ''
	application_ids = ''
	print('==========APPLY REQUEST RECEIVED==========')
	pprint.pprint(request.POST)
	if 'apply-button' in request.POST:
		print("1-single apply button")
		request_post = request.POST.dict()
		application = save_application(request, request.POST['loan-key'], request.session)
		loan_keys = request.POST["loan-key"]
		application_ids = str(application.id)
	else:
		if ('apply-all') in request.POST:
			request_post  =request.session['favorite_loans'].copy()
		elif ('apply-selected') in request.POST:
			request_post = request.POST.copy()
		# print("multi-keys------------------------------>")
		request_source = request.POST['request_source']
		request_source = 'saved-loans-page-multiapply' if request_source!='side-cart-multiapply' else request_source
		for key in request_post:
			print(key)
			try:
				if len(key.split('&'))==3:
					application = save_application(request, key, request.session, request_source=request_source)
					loan_keys+=key+"+"
					application_ids+=str(application.id)+"+"
			except:
				pass
		loan_keys = loan_keys[:loan_keys.rfind('+')]
		application_ids = application_ids[:application_ids.rfind('+')]
	# print(loan_keys)
	if len(loan_keys) > 0:
		return HttpResponseRedirect(reverse('redirect_to_bank',kwargs={
			"loan_keys":loan_keys,
			"application_ids":application_ids
		}))
	else:
		return HttpResponse('<script>alert("Hiçbir krediyi seçmediniz.");window.location="/sepet";</script>')

def set_success(request):
	# return HttpResponse("successful")
	# print("========ACTUAL APPLICATION REQUEST======")
	# print(request.POST)
	application = Application.objects.get(pk=request.POST['application_id'])
	application.success = True
	application.save()
	return render(request, 'loans/widgets/redirect_success.html')

def show_detail(request):
	# try:
		request_post = request.POST.dict()
		request_post['session'] = request.session.session_key
		request_post['principal'] = request_post['principal'].replace('.','')
		if not request.POST.get('bank'):
			redirect_to = reverse('loan_detail',kwargs={'principal':request_post['principal'],'slug':request_post['slug'],'tenure':request_post['tenure']})
		else:
			redirect_to = reverse('loan_detail_b',kwargs={'loan_type':request_post['loan_type'],'bank_slug':request_post['bank'],'tenure':request_post['tenure'],'principal':request_post['principal']})
		form = LoanDetailRequestForm(request_post)
		if (form.is_valid()):
			form.save()
		else:
			print("======= FORM IS NOT VALID =========")
			print(form.errors)
			print(f"principal: {form.data['principal']}")
			return HttpResponse('form is not valid: errors: '+json.dumps(form.errors))
		return HttpResponseRedirect(redirect_to)
	# except:
	# 	# print('save couldnt be saved!!')
	# 	return HttpResponse('fail')
