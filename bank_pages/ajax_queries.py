from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from scripts.py.payment_plan import payment_plan
from loans.models import Loan, Bank
import pandas as pd
import pprint
from loans.ajax_queries import get_liked_loans_from_session
from home.cart_views import transfer_liked_loans_to_list
from home.views import calculate_loan_details
import json

def bank_specific_loan_refresh(request):
    print("="*10+" bank_specific_loan_refresh "+"="*10)
    pprint.pprint(request.GET)
    try:
        tenure = request.GET['tenure']
        principal = request.GET['principal'].replace('.','')
        bank = Bank.objects.get(slug=request.GET['bank'])
        loan_type = request.GET['loan_type']
        loan = Loan.objects.filter(tenure__gte=tenure,principal__gte=principal,bank=bank,loan_type=loan_type,active=True).order_by('tenure','principal').first()
        print(loan)
        return render(request, 'bank_pages/local/header_loan_request_details.html', {
            'loan':calculate_loan_details(loan,principal=int(principal),tenure=int(tenure))})
    except:
        return render(request,'bank_pages/local/header_loan_request_details_error.html', {})