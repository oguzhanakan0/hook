from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.db.models import Min, Max
from scripts.py.payment_plan import payment_plan
from loans.models import Bank, LoanSummary
from loans.views import show_subscription_notification
from seo.models import FAQItem
from blog.models import PostPage
import pandas as pd
import pprint
from datetime import date as dt
from datetime import datetime as dtm
from datetime import timedelta
from userlogs.models import SubscriptionType

loan_summary_max_date = LoanSummary.objects.aggregate(Max('date'))['date__max']
loan_summary = LoanSummary.objects.filter(date=loan_summary_max_date)

def bank_page(request, bank_name):
	if request.method == "GET":
		bank = Bank.objects.get(slug=bank_name)
		# today = dt.today() # if dtm.now().hour>5 else dt.today()+timedelta(days=-1)
		# loan_summary_list = LoanSummary.objects.filter(date=today,bank=bank)
		loan_summary_list = loan_summary.filter(bank=bank)
		print("===== LOAN_SUMMARY_LIST ====")
		print(loan_summary_list)
		faqs  = FAQItem.objects.filter(relevant_to=bank_name)
		blogs = PostPage.objects.filter(seo_title__contains=bank_name).order_by('-date')[:4]
		for blog in blogs:
			blog.title = blog.title if len(blog.title)<=50 else blog.title[:50]+" ..."
			blog.body = blog.body[:100]+" ..."

		banks = Bank.objects.filter(active=1).order_by('-sponsored')
		return render(request, 'bank_pages/bank_page.html', {
				'bank':bank,
				'loan_summary_list':loan_summary_list,
				'faqs':faqs,
				'blogs':blogs,
				'banks':banks,
				'show_subscription_notification':show_subscription_notification(request,bank.slug),
				'subscription_type':bank.slug
			})
	# elif request.method == "POST":
	# 	if ("apply-button" in request.POST):
	# 		return handle_apply_request(request.POST)

	print('invalid request:')
	pprint.pprint(request.POST)
	return HttpResponse('invalid request')