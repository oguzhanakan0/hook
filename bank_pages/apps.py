from django.apps import AppConfig


class BankPagesConfig(AppConfig):
    name = 'bank_pages'
