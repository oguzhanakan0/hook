from django.urls import path
from django.urls import include, re_path
from . import views
from . import ajax_queries

urlpatterns = [
    path('banka/<bank_name>/', views.bank_page, name='bank_page'),
    path('ajax/bank-specific-loan-refresh/', ajax_queries.bank_specific_loan_refresh, name='ajax_bank_specific_loan_refresh'),  # <-- this one here
]