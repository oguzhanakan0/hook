function setPrincipalLimitsToFeaturedLoans() {
    
    $("div[id^='featured_loan_id_form_']").each(function() {
        var principal = $(this).find("div[id^='featured_loan_id_principal_']");
        var tenure = $(this).find("div[id^='featured_loan_id_tenure_']");
        var details = $("ul[refers-to='"+$(this).attr("id")+"']")
        $.when(
                runAjaxQuery('/ajax/get-principal-limits/',
                    {'slug': principal.attr("slug"), "for_featured_loan":1, 'initial_principal': principal.attr("initial")},
                    principal),
                runAjaxQuery('/ajax/get-tenure-limits/',
                    {'slug': tenure.attr("slug"), "for_featured_loan":1, 'initial_tenure': tenure.attr("initial")},
                    tenure)
            ).then(function () {
                principal.change(function(){
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery('/ajax/featured-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
                        details);
                });
                tenure.change(function(){
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery('/ajax/featured-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
                        details);
                });
            }
        );
    });
}


function runAjaxQuery(url_,data_,return_to_) {
    // $(return_to_).addClass('loader');
    return $.ajax({
        url: url_,
        data: data_,
        success: function (data) {
            console.log('writing data to: '+$(return_to_).attr('id'));
            $(return_to_).html(data);
            // $(return_to_).removeClass('loader');
        }
    });
}


$(document).ready(function() {
    console.log("document ready!");
    // Get tenures
    runAjaxQuery('/ajax/get-tenure-limits/',{'loan_type': $("input[id='id_loan_type']").attr('value'),'class':'display-7'},"div[id='main-request-tenure']");
    // Get principal limits
    runAjaxQuery('/ajax/get-principal-limits/',{'loan_type': $("input[id='id_loan_type']").attr('value'),'class':'display-7'},"div[id='main-request-principal']");
    setPrincipalLimitsToFeaturedLoans();
});
