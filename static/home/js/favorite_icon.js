var $loading = $("#ajax-loader");
$(document)
    .ajaxStart(function () {
        $loading.show();
    })
    .ajaxStop(function () {
        $loading.hide();
        $("#lblCartCount").html(Math.max(0,$('ul[class="cd-cart-items"] > li').length-1));


    // new Intl.NumberFormat('tr-TR', { }).format(523523646);
    // NUMBER SEPARATOR 
	// $("input[name='principal']").on('keyup', function(){
	//     var n = Intl.NumberFormat('tr-TR', { }).format(parseInt($(this).val().replace(/\D/g,''),10));
	//     n = isNaN(n) ? $(this).val('') : $(this).val(n);;
	// });

	// $("input[name='principal']").focusout(function() {
	// 	var n = parseInt($(this).val().replace(/\D/g,''),10);
	//     n = (n > $(this).data('max')) ? $(this).data('max') : (n < $(this).data('min')) ? $(this).data('min') : n;
	//     $(this).val(Intl.NumberFormat('tr-TR', { }).format(n));
	// });
});


console.log('fav buttons js running');

// THIS FUNCTIONS NEED TO BE RETRIEVED FROM A SEPARATE FILE!!
function runAjaxQuery(url_,data_,return_to_) {
    $.ajax({
        url: url_,
        data: data_,
        success: function (data) {
            console.log('query success');
            $(return_to_).html(data);
        }
    });
}

function runAjaxQueryWithUrlAndDataOnly(url_,data_) {
    return $.ajax({
        url: url_,
        data: data_,
        success: function (data) {
			console.log('ajax query response:');
            console.log(data);
            // $(return_to_).html(data);
        }
    });
}

// END OF THIS FUNCTIONS NEED TO BE RETRIEVED FROM A SEPARATE FILE!!

function getLikedLoans(url_) {
    $.ajax({
        url: url_,
        success: function (data) {
            if(data!="no-liked-loans"){
				var liked_loans = JSON.parse(data);
				var liked_loan_keys = Object.keys(liked_loans)
				// console.log(liked_loans);
				// BELOW CODE COLORS UP THE LIKE BUTTONS
				$("i[class^='like-icon']").each(function() {
					if(!(liked_loan_keys.includes($(this).attr('loan-key')))){
						console.log("removing attributes of: "+$(this).attr('loan-key'));
						$(this).html('<span class="display-8"> Kaydet</span>');
						$(this).removeClass( "like-icon-press", 1000 );
						$(this).removeClass( "liked-text", 1000 );
						$(this).removeClass( "liked-text-press", 1000 );
						$(this).attr("is-liked",0);
					} else {
						console.log("adding attributes to: "+$(this).attr('loan-key'));
						$(this).html('<span class="display-8"> Kayıtlı</span>');
						$(this).addClass( "like-icon-press", 1000 );
						$(this).addClass( "liked-text", 1000 );
						$(this).addClass( "liked-text-press", 1000 );
						$(this).attr("is-liked",1);
					}
				});
				// BELOW CODE FILLS THE CART
				runAjaxQuery('/ajax/render-cart/',{'favorite_loans':data},"div[id='cd-cart']");
				console.log("cart rendered successfully");

				// console.log("WINDOW.LOCATION.PATHNAME:");
				// console.log(window.location.pathname);
				runAjaxQuery('/sepet/ajax/sepet-refresh/',{
					'favorite_loans':data,
					'url':window.location.pathname},
					"div[id='cart-page-content']"
				);
    		}
        }
    });
}

$(document).ready(function() {
    // console.log("document ready!")
    // $("i[class='like-icon']").each(function() {
    // 	$(this).html('<span class="display-8"> Favorilere ekle</span>');
    // });
    getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
    refreshCartButtons();

	// $('form').on('submit', function(e){
	//        e.preventDefault();
	//        var newval = $("input[name='principal']").val().split('.').join("");
	// 	$("input[name='principal']").val(newval);
	// 	this.submit();
	//    });

});


$(function() {

	$.fn.extend({
	    toggleHtml: function(a, b){
	        return this.html(this.html() == b ? a : b);
	    }
	});

	$("i[class='like-icon']").click(function() {
		console.log('clicked on like button with loan key: '+$(this).attr('loan-key'));
		$(this).toggleClass( "like-icon-press", 1000 );
		$(this).toggleClass( "liked-text", 1000 );
		$(this).toggleClass( "liked-text-press", 1000 );
		// $(this).toggleHtml('', '<span class="display-8"> eklendi</span>');
		$(this).attr("is-liked",$(this).attr("is-liked")==1?0:1);
		console.log("is-liked?");console.log($(this).attr('is-liked')==1);
		if($(this).attr('is-liked')==1){
			$.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/write-liked-loan-to-session/',
				{
					'loan_key':$(this).attr('loan-key'),
				})).then(function () {
			    getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
			});
		} else {
			$.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/remove-liked-loan-from-session/',
				{
					'loan_key':$(this).attr('loan-key')
				})).then(function () {
			    getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
			});
		}
	});
});

function refreshCartButtons() {
	console.log('refreshing cart buttons');
    if($("div[class^='checkbox']").length<2){
    	$("button[name*='compare-']").prop('disabled', true);
    	// $("button[name*='compare-']").removeClass('bg-secondary');
    } else{
    	$("button[name*='compare-']").prop('disabled', false);
   		//	 if(!$("button[name*='compare-']").hasClass("bg-secondary")){
		//    $("button[name*='compare-']").addClass("bg-secondary");
		// }
    }
}


// REMOVE LIKED LOAN USING X BUTTON ON SIDE CART AND IN THE ACTUAL CART
$(document).on('click', 'a[class^="loan-remove-property"]', function(){
    console.log('clicked on remove button with loan key: '+$(this).attr('loan-key'));
	$.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/remove-liked-loan-from-session/',
		{
			'loan_key':$(this).attr('loan-key')
		})).then(function () {
	    getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
	    refreshCartButtons();
	});
});
//////////////
$(document).on('click', 'i[class^="re-like-icon"]',function() {
	console.log('clicked on like button with loan key: '+$(this).attr('loan-key'));
	$(this).toggleClass( "like-icon-press", 1000 );
	$(this).toggleClass( "liked-text", 1000 );
	$(this).toggleClass( "liked-text-press", 1000 );
	// $(this).toggleHtml('', '<span class="display-8"> eklendi</span>');
	$.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/write-liked-loan-to-session/',
		{
			'loan_key':$(this).attr('loan-key'),
		})).then(function () {
	    getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
	    refreshCartButtons();
	});
});

$('.field__input').on('input', function() {
  var $field = $(this).closest('.field');
  if (this.value) {
    $field.addClass('field--not-empty');
  } else {
    $field.removeClass('field--not-empty');
  }
});