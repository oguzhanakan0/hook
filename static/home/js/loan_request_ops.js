// INITIAL CONFIGURATION


function setPrincipalLimitsToFeaturedLoans() {
    
    $("div[id^='featured_loan_id_form_']").each(function() {
        var principal = $(this).find("div[id^='featured_loan_id_principal_']");
        var tenure = $(this).find("div[id^='featured_loan_id_tenure_']");
        var details = $("ul[refers-to='"+$(this).attr("id")+"']")
        $.when(
                runAjaxQuery('/ajax/get-principal-limits/',
                    {'slug': principal.attr("slug"), "for_featured_loan":1, 'initial_principal': principal.attr("initial")},
                    principal),
                runAjaxQuery('/ajax/get-tenure-limits/',
                    {'slug': tenure.attr("slug"), "for_featured_loan":1, 'initial_tenure': tenure.attr("initial")},
                    tenure)
            ).then(function () {
                principal.change(function(){
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery('/ajax/featured-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
                        details);
                });
                tenure.change(function(){
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery('/ajax/featured-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
                        details);
                });
            }
        );
    });
}

function refreshMinMaxInterest(){
    runAjaxQuery(
        '/ajax/render-min-max-interest/',
        {
            'loan_type': $("input[id='id_loan_type']").attr('value'),
            'principal':parseInt($("div[id='main-request-principal']").find(':input').val().replaceAll('.','')),
            'tenure':parseInt($("div[id='main-request-tenure']").find('select').val())
        },
        "div[id='min-max-interest-section']");
}

function setLoanType(loan_type){
    $("input[id='id_loan_type']").attr('value',loan_type);
    var tenure = "div[id='main-request-tenure']";
    var principal = "div[id='main-request-principal']";
    // Get tenures
    $.when(
        runAjaxQuery('/ajax/get-tenure-limits/',{'loan_type': loan_type,'class':'display-7 form-text-color'},tenure),
        runAjaxQuery('/ajax/get-principal-limits/',{'loan_type': loan_type,'class':'display-7 form-text-color'},principal)
        ).then(function () {refreshMinMaxInterest()});
}

function runAjaxQuery(url_,data_,return_to_,type='GET') {
    // $(return_to_).addClass('loader');
    return $.ajax({
        type : type,
        url: url_,
        data: data_,
        success: function (data) {
            console.log('writing data to: '+$(return_to_).attr('id'));
            $(return_to_).html(data);
            // $(return_to_).removeClass('loader');
        }
    });
}

$(document).ready(function() {
    console.log("document ready!");
    setLoanType('GPL');
    $("div[id='main-request-tenure']").change(function(){refreshMinMaxInterest()});
    $("div[id='main-request-principal']").change(function(){refreshMinMaxInterest()});
    $("li[class='nav-item']").click(function () {setLoanType($(this).attr('loan-type'));});
    $("input[id='0km']").click(function () {setLoanType('CAR');});
    $("input[id='2el']").click(function () {setLoanType('CA2');});
    $("a[id='expand-loan-matrix-section']").one("click", function(){runAjaxQuery('/ajax/render-loanmatrix-tables/',{},"div[id='loanmatrix-tables-section']");});
    $("a[id='expand-loan-summary-section']").one("click", function(){runAjaxQuery('/ajax/render-loansummary/',{},"div[id='loansummary-section']");});
    $("a[id='expand-featured-loans-section']").one("click", function(){
        $.when(
            runAjaxQuery('/ajax/render-featured-loans/',{},"div[id='featured-loans-section']")
        ).then(function () {
            setPrincipalLimitsToFeaturedLoans();
        });
    });
    $("a[id='expand-bank-list-section']").one("click", function(){runAjaxQuery('/ajax/render-banks/',{},"div[id='banks-section']");});
    // $("a[id='expand-faq-section").one("click", function(){runAjaxQuery('/ajax/render-faq/',{},"div[id='faq-section']");});
    // $("a[id='expand-blog-section").one("click", function(){runAjaxQuery('/ajax/render-blog/',{},"div[id='blog-section']");});

    $("a[id='expand-faq-section']").one("click", function(){$("#toggle1-z").show();});
    $("a[id='expand-blog-section']").one("click", function(){$("#features18-4").show();});


    // var waypoint = new Waypoint({
    //     element: document.getElementById("loanmatrix-tables-section"),
    //     handler: function(direction) {
    //         console.log('Loamatrix tables are getting ready.');
    //         runAjaxQuery('/ajax/render-loanmatrix-tables/',{},"div[id='loanmatrix-tables-section']");
    //         this.destroy();
    //     },
    //     offset: 1000
    // });

    // var waypoint = new Waypoint({
    //     element: document.getElementById("featured-loans-section"),
    //     handler: function(direction) {
    //         console.log('Featured loans section are getting ready.');
    //         $.when(
    //             runAjaxQuery('/ajax/render-featured-loans/',{},"div[id='featured-loans-section']")
    //         ).then(function () {
    //             setPrincipalLimitsToFeaturedLoans();
    //         });
    //         this.destroy();
    //     },
    //     offset: 400
    // });

    // var waypoint = new Waypoint({
    //     element: document.getElementById("loansummary-section"),
    //     handler: function(direction) {
    //         console.log('Loansummary section are getting ready.');
    //         runAjaxQuery('/ajax/render-loansummary/',{},"div[id='loansummary-section']");
    //         this.destroy();
    //     },
    //     offset: 400
    // });

    // var waypoint = new Waypoint({
    //     element: document.getElementById("banks-section"),
    //     handler: function(direction) {
    //         console.log('Banks section are getting ready.');
    //         runAjaxQuery('/ajax/render-banks/',{},"div[id='banks-section']");
    //         this.destroy();
    //     },
    //     offset: 400
    // });

    // var waypoint = new Waypoint({
    //     element: document.getElementById("blog-section"),
    //     handler: function(direction) {
    //         console.log('Blog section are getting ready.');
    //         runAjaxQuery('/ajax/render-blog/',{},"div[id='blog-section']");
    //         this.destroy();
    //     },
    //     offset: 400
    // });

    // var waypoint = new Waypoint({
    //     element: document.getElementById("faq-section"),
    //     handler: function(direction) {
    //         console.log('FAQ section are getting ready.');
    //         runAjaxQuery('/ajax/render-faq/',{},"div[id='faq-section']");
    //         this.destroy();
    //     },
    //     offset: 400
    // });

});
