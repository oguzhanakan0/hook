var $loading = $(".loader-div");
$(document)
    .ajaxStart(function () {
        $loading.show();
        $(document.body).css('overflow','hidden');
    })
    .ajaxStop(function () {
        $loading.hide();
        $(document.body).css('overflow','visible');
});
