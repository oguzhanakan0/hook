// INITIAL CONFIGURATION

// function setPrincipalLimitsToFeaturedLoans() {
    
//     $("form[id^='loan-request-form-']").each(function() {
//         var principal = $(this).find("div[id^='principal-']");
//         var tenure = $(this).find("div[id^='tenure-']");
//         var details = $("ul[refers-to='"+$(this).attr("id")+"']")
//         $.when(
//                 runAjaxQuery('/ajax/get-principal-limits/',
//                     {'slug': principal.attr("slug"), "for_featured_loan":1, 'initial_principal': principal.attr("initial")},
//                     principal),
//                 runAjaxQuery('/ajax/get-tenure-limits/',
//                     {'slug': tenure.attr("slug"), "for_featured_loan":1, 'initial_tenure': tenure.attr("initial")},
//                     tenure)
//             ).then(function () {
//                 principal.change(function(){
//                     $(this).css("background-color", "#D6D6FF");
//                     runAjaxQuery('/ajax/featured-loan-refresh/',
//                         {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
//                         details);
//                 });
//                 tenure.change(function(){
//                     $(this).css("background-color", "#D6D6FF");
//                     runAjaxQuery('/ajax/featured-loan-refresh/',
//                         {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
//                         details);
//                 });
//             }
//         );
//     });
// }

function setLimitsToLoanRequests() {
    
    $("div[id^='loan-request-form-']").each(function() {
        var principal = $(this).find("div[id^='principal-']");
        var tenure = $(this).find("div[id^='tenure-']");
        var bank = $(this).attr('bank');
        var loan_type = $(this).attr('loan_type');
        var details = $("ul[refers-to='"+$(this).attr("id")+"']")
        $.when(
                runAjaxQuery('/ajax/get-principal-limits/',
                    {'initial_principal': principal.attr("initial"),
                     'loan_type': loan_type,
                     'bank': bank
                    },
                    principal),
                runAjaxQuery('/ajax/get-tenure-limits/',
                    {'initial_tenure': tenure.attr("initial"),
                     'loan_type': loan_type,
                     'bank': bank
                    },
                    tenure)
            ).then(function () {
                runAjaxQuery(
                        '/ajax/bank-specific-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "loan_type":loan_type, "bank":bank},
                        details);
                principal.on('focusout', '.form-field', function (e) {
                    fixMinMaxLimits($(this));
                    console.log('running');
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery(
                        '/ajax/bank-specific-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "loan_type":loan_type, "bank":bank},
                        details);
                });
                tenure.change(function(){
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery(
                        '/ajax/bank-specific-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "loan_type":loan_type, "bank":bank},
                        details);
                });
            }
        );
    });
}


function runAjaxQuery(url_,data_,return_to_) {
    // $(return_to_).addClass('loader');
    return $.ajax({
        url: url_,
        data: data_,
        success: function (data) {
            console.log('writing data to: '+$(return_to_).attr('id'));
            // if ($(return_to_).attr('id')==undefined){
            //     console.log('UNDEFINED FOR:\nurl:'+url_+'\ndata:'+data);
            // }
            $(return_to_).html(data);
            // $(return_to_).removeClass('loader');
        }
    });
}


$(document).ready(function() {
    console.log("document ready!");
    setLimitsToLoanRequests();
    console.log('limits are set!')
});