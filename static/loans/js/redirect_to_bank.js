// setTimeout(function(){ 
// 	setSuccess();
// 	window.location.href = "http://www.google.com"; 
// }, 3000);

// function setSuccess() {
//     $.ajax({
//         url: url_,
//         data: data_,
//         success: function (data) {
//         	console.log('### this application success set to true.')
//             // $(return_to_).html(data);
//         }
//     });
// }

$(document).ready(function() {
    $("form[action='/set-success'").each(function () {
    	var $this = $(this);
    	var $parent = $this.parent();
    	$this.submit(function (e) {
	        e.preventDefault(); // avoid to execute the actual submit of the form.
	        var form = $this;
	        console.log("ACTUAL REDIRECTION HAPPENS WITH THE FOLLOWING FORM:")
	        console.log(form.find("input[name='loan_id']")[0])
	        console.log(form.find("input[name='application_id']")[0])
	        $.ajax({
		        type : "POST",
		        url: "/set-success",
		        data: form.serialize(),
		        success: function (data) {
		            $parent.html(data);
					window.open(form.find("input[name='apply_link']")[0].value);
		        }
		    });
	    });
    });
});


// $.when(
//     runAjaxQuery('/ajax/render-featured-loans/',{},"div[id='featured-loans-section']")
// ).then(function () {
//     setPrincipalLimitsToFeaturedLoans();
// });