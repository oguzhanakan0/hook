"""
WSGI config for hook project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

# import subprocess
# subprocess.run('conda activate digi', shell=True)
# activate_this = 'C:/Users/myuser/Envs/my_application/Scripts/activate_this.py'
# execfile(activate_this, dict(__file__=activate_this))
# exec(open(activate_this).read(),dict(__file__=activate_this))

# import subprocess

# subprocess.run('C:\Anaconda3\condabin\conda.bat activate digi && python C:\hook\hook\sss.py')

import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('C:/Anaconda3/envs/digi/lib/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('C:/hook')
sys.path.append('C:/hook/hook')

os.environ['DJANGO_SETTINGS_MODULE'] = 'hook.settings'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hook.settings")

from django.core.wsgi import get_wsgi_application
print('yazdir biseyler')
application = get_wsgi_application()

