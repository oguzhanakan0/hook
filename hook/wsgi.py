#!/usr/bin/env python
# coding: utf-8

"""
WSGI config for hook project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

# import subprocess
# subprocess.run('conda activate digi', shell=True)
# activate_this = 'C:/Users/myuser/Envs/my_application/Scripts/activate_this.py'
# execfile(activate_this, dict(__file__=activate_this))
# exec(open(activate_this).read(),dict(__file__=activate_this))

# import subprocess

# subprocess.run('C:\Anaconda3\condabin\conda.bat activate digi && python C:\hook\hook\sss.py')

import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/root/anaconda3/envs/digi/lib/python3.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/hook')
sys.path.append('/hook/hook')

os.environ['DJANGO_SETTINGS_MODULE'] = 'hook.settings'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hook.settings")

try:
	from django.core.wsgi import get_wsgi_application
except Exception as e:
	print("There was a critical error")
	print(sys.executable)
	print("Installed packages:")
	print(help("modules"))
	print(os.system('echo $CONDA_DEFAULT_ENV'))
	print("Error: ")
	try: print(e)
	except: print(e.message)
	finally: print("OLMUYOR")
# print('yazdir biseyler')
application = get_wsgi_application()

