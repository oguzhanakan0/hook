import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('C:/Anaconda3/envs/digi/lib/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('C:/hook')
sys.path.append('C:/hook/hook')

os.environ['DJANGO_SETTINGS_MODULE'] = 'hook.settings'
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hook.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
