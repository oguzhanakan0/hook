### 22Jun Update:
- Loans app created. Find details in Trello --> Backend Tasks --> Create Models in Django
- loan_list and loan_detail views created.
- App redirects from loan_list to loan_detail with required arguments.
- Next on deck: implement dropdown on forms, create activity log and blog apps.

### 23Jun Update:
- Blog app created. Find details in Trello --> Backend Tasks --> Create Models in Django
- blog_index_page and blog_page views created.
- Wagtail CMS integrated with the project. Go to localhost:8000/cms
- Wagtail is used for easy adding & editing content (blog posts in our case.)
- Next on deck: implement dropdown on forms, create activity log.

### 24Jun Update:
- New models created. Find details in Trello --> Backend Tasks --> Create Models in Django
- Next on deck: create log tables, implement dropdown on forms, create activity log.

### 25Jun Update:
- Working on logging logic.. will explain tomorrow.

### 26Jun Update:
- DB for loans has been finalized. Some rusts to be fixed on demand.
- Logging logic:
    - tables loans_loan, loans_loandetail and loans_link are logged. just append "log" at the end for each table.
    - there are three actions possible for logging: INSERT, UPDATE, and DELETE.
    - Log related fields are the following:
        - _user    : username of the person who made the change & insertion & deletion.
        - _program : from which source has the change made (admin panel / script etc.)
        - _action  : INSERT, UPDATE, or DELETE
        - _upd_time: timestamp
    - Log fields except _action are present in original tables as well.


### 29Jun Update:
- Form related front-end learnings. Nothing visible yet but there are some progress.

### 30Jun Update:
- Homepage Front-end LoanRequestForm constructed. CSS & styling edits.

### 1Jul Update:
- Homepage Front-end LoanRequestForm edited so that there is only 1 form now - eliminated duplicate div ids etc. CSS & styling edits.
- Blog section integrated with templates on Homepage.
- Next on deck: Build loan_list template

### 2Jul Update:
- Scraping db is created according to @Enes' plan.
- LoanSummary model has been created. This table will directly be used in production and refreshed everytime Loan table changes (though, this process is not ready yet).
- Next on deck: Build loan_list template

### 6Jul Update:
- Working on HTML inheritance between pages. Here is how it is used:
    - home/base.html: is the base page for every page of ours.
    - home/index.html: extends base.html and fills with blocks. Some blocks may be global and some may be local, e.g. menu, javascripts, footer are global and loan request section is local.
    - every other app must extend home/base.html and include global blocks. local blocks are supposed to be created in related app folders.

### 7July Update:
- Loan query result page front-end is being shaped. 1-2 days to go.

### 8July Update:
- Loan query result page front-end is being shaped. 1-2 days to go.

### 9July Update:
- Loan query result page front-end is done (add-ons tbd).
- Loan detail page front-end is under development. 2-3 days to go.

### 9July Update:
- Loan detail page front-end is under development. 2-3 days to go.

### 12July Update:
- Loan detail page front-end is under development. 1 day to go.

### 13July Update:
- Loan detail page front-end is under development.
- Loan Request panel on loan detail page is working inefficiently. The url needs to be changed to slug instead of loan id
- Tooltips to info icons added on loan list page.

### 14July Update:
- The url on detail page changed to slug instead of loan id
- Working on the min-max principal and tenure settings

### 14July "GECE MESAİSİ" Update:
- Limit setting on the principal and tenure is done!

### 15July Update:
- show/hide button on payment_plan is working properly. it will show first 10 rows initially.
- loan summary dataframe is inserted to loans_loansummary table. this will be a cronjob in production to run everyday.
- next item: display loan summary in homepage


### 22July Update:
- SEO module created with FAQItem model.
- FAQ html template inserted in homepage.
- next item: display loan summary in homepage

### 11August Update:
- Batch commit after some holiday.
- Loan Summary Table finished with query section. Loan summary table is populated everyday in loans_loansummary table.
- Loan Matrix Table finished (href is still todo):
    - loans_loanmatrix
    - loans_loanmatrixtable
    - These two tables are populated using loans_loan table everyday. The reason why we have 2 tables in place is that creating matrix dynamically would be very costly in the server side, and it would require js work in the client side. Therefore, I decided to write the table's HTML form directly in the daily job. So, we have an organized table called loans_loanmatrix and a HTML table version of it in loans_loanmatrixtable.
- Featured loans are created. Two columns added to loans_loan table; 1- is_featured (bool) and 2- feature_order (integer). This widget still has some work to do like setting up the Ajax query path and rendering the widget using the url.

ALL THE SCRIPTS THAT I SAY "RUNS EVERYDAY" IS PLACED IN SCRIPTS/JUPYTER PATH RIGHT NOW. THEY WILL BE TRANSFERRED TO SCRIPTS/PY FOLDER IN PRODUCTION.


### 21September:
- Finally we get to start back.
- Requery button in loan list page is functionalized. Now you can change loan amount etc and search again in the same page.
- I am working on the tree bar on top.

### 29September:
- Favorite logic has been implemented. Now whenever the user clicks on favorite button, a record will be inserted to his session_key located in django_session table. We need to archive this as well, but not for now.
- Refreshing favorite loans has been implemented. The favorites are cookie-backed so the user will keep his favorite loans when he switch pages.
- Cart has been created. Favorite loans go to cart and they kept there.
- Loan comparison logic is created. Now we can compare two loans only, but we will implement N-comparison as well.
- If the user compares two loans, two-loan comparison logic will work. Otherwise the N-loan comparison will work.

### 1October:
- Favorite loan logic implemented sitewise. Now all loan elements can be functionally liked/unliked.
- Cart page has been created. Now the cart has all the liked loans and a history of unliked loans.
- Loan detail redirection from loan matrix table has been implemented.
- Header CSS has changed, now the header has 50px height.

### 4October:
- There has been so many things regarding functionality of the app.
- Instant refresh on featured loan items has been implemented. Fucking featured loans.
- All buttons & forms in all pages are functional right now - except for the cart page.
- todo: loan-specific and bank-specific pages.

### 5October:
- Çok işim varrr update yazamıcam ama şu an her şey fonksiyonel (cart page dahil). apply buttonu için redirect url işini halletmek lazım bir tek.
- başkaa dünden kalan todolar duruyor o sayfalara çok üşeniyorum bakalım napcaz.

### 9October:
- Known issues: 
> get-principal-limits runs for featured loans as well even though we change the main request's loan type. we need to separate the ajax query to only refresh the main request's limits. Same issue may go with get-tenure-limits as well.
> we need to toggle back the fav icons after we delete fav loans in the side cart menu. right now they stay colored.

### 19October:
- Known issues: 
> get-principal-limits runs for featured loans as well even though we change the main request's loan type. we need to separate the ajax query to only refresh the main request's limits. Same issue may go with get-tenure-limits as well.

### 20October:
- Aggregator Scraping process has been transferred. Some bugs due to this have been fixed.
- Known issues: 
> get-principal-limits runs for featured loans as well even though we change the main request's loan type. we need to separate the ajax query to only refresh the main request's limits. Same issue may go with get-tenure-limits as well.

### 20October:
- Bank specific pages are created.
- Known issues: 
> get-principal-limits runs for featured loans as well even though we change the main request's loan type. we need to separate the ajax query to only refresh the main request's limits. Same issue may go with get-tenure-limits as well.

### 25October:
- New models in loans:
    * Critical: indicates critical buckets for each loan_type. so it's unique with (loan_type, tenure, principal)
    * LoanHeader: this has been implemented to add headers to loan boxes. if a loan is sponsored (or any other custom feature), the loan will have a header according to it.
    * Loan.loan_header: ForeignKey to LoanHeader has been added.
- Main loan request's principal and tenure widgets has been converted to the new versions.
- Min-max indications are removed from principal and tenure inputs. Now they have an initial value. We must specify different initials for loan_type's.
- Min-max indications are transferred to labels.
- NEW! blog page is COMPLETELY changed:
    * blog page was formerly a plain page with plain text. Now it has the following features:
        1. pagination
        2. search via tags and/or content and/or category
        3. markdown support
        4. comment feature if we enable disqus (but it may be hard to control over)
        5. categorization

### 26October:
- New fields in bank:
    * sponsored: we want to indicate whether a bank is sponsored and order accordingly.
    * logo_360_270: for optimization purposes, we will use 128x96px images across the site, except for the bank-specific pages. in that case, we will use 360x270px images.
- Bank specific pages have been created. It looks ok for now, we may improve later.

### 27October:
- Tooltips have changed. Now we show details of the loans.

### 29October:
- Breadcrumbs are added. They are implemented on the loan_query_page and loan_detail_page. We need to implement in the following as well:
- bank_pages, sepet, loantype_pages

### 29October:
- Unutmadan, Cumhuriyet Bayramımız kutlu olsun!!

### 30October:
- loantype_pages'i yaptik. cok az kaldi.
- loading spinner implemented for all pages.
- go to top button is added.
- breadcrumbs applied to all pages.

### 7November:
- loantype_pages finished.
- min-max interest rates are showing now on main loan request section and detail page.
- other small improvements

### 7November:
- gathering up the CSS and JS files.

### 11November:
- CSS file is cleaner now.
- Performance improvements, namely, waypoints in featured loans and one-time execution of home components.
- Side cart UI is better now.

### 15November:
- UI improvements

### 25November:
- Backend work almost finished. Too exhausted to write details. fuqk..

### 26November:
- Backend work finished.
- We keep all user-posted data in the following tables:
* userlogs_application
* userlogs_loanrequest
* userlogs_loandetailrequest
* userlogs_subscription
* userlogs_save
- Redirection to bank pages are finished. todo: fill the url's in loans_link table.

### 27November:
- Loan comparison pages updated. Now they look better.
- I guess it's all done. Let's deploy it.

### 27November:
- Top menu improvements.