from django.urls import path
from django.urls import include, re_path
from . import views

urlpatterns = [
    path('kredi/ihtiyac-kredisi/', views.ihtiyac_kredisi, name='ihtiyac_kredisi'),
    path('kredi/tasit-kredisi-0km/', views.tasit_kredisi_0km, name='tasit_kredisi_0km'),
    path('kredi/tasit-kredisi-2el/', views.tasit_kredisi_2el, name='tasit_kredisi_2el'),
    path('kredi/konut-kredisi/', views.konut_kredisi, name='konut_kredisi')
]