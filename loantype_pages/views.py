from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.db.models import Max
from scripts.py.payment_plan import payment_plan
from loans.models import Loan, Tenure, LoanType, LoanSummary, LoanMatrixTable, Bank
from loans.views import calculate_loan_details, calculate_installment, show_subscription_notification, handle_loan_request_form
from userlogs.models import LoanRequest, SubscriptionType
from seo.models import FAQItem
from userlogs.forms import LoanRequestForm
from blog.models import PostPage
from datetime import date as dt
from datetime import datetime as dtm
from datetime import timedelta
import pandas as pd
import pprint

today = LoanSummary.objects.aggregate(Max('date'))['date__max'] # dt.today() # if dtm.now().hour>5 else dt.today()+timedelta(days=-1)
loan_matrix_tables = LoanMatrixTable.objects.filter(date=today)
loan_summaries = LoanSummary.objects.filter(date=today).order_by('min_interest')
banks = Bank.objects.filter(active=1).order_by('-sponsored','name')

def handle_get_request(request, loan_type):
    if not request.session.session_key: request.session.save()
    
    try: loan_matrix_table = loan_matrix_tables.get(loan_type=loan_type)
    except: loan_matrix_table = None
    try:
        loan_summary = loan_summaries.filter(loan_type=loan_type)
        loan_summary_tenures = dict(zip([e.id for e in loan_summary],[list(e.tenures.all().order_by('tenure')) for e in loan_summary]))
    except: loan_summary = None
    
    featured_loans = [calculate_loan_details(e, int(e.principal.principal), int(e.tenure.tenure)) for e in Loan.objects.filter(is_featured=True,loan_type=loan_type,active=True).order_by('feature_order')]
    
    print("loan-summary: ****************")
    print(loan_summary)
    return render(request, 'ihtiyac_kredisi_page.html', {
    	'loan_matrix_table':loan_matrix_table,
    	'loan_type':loan_type,
    	'loan_summary':loan_summary,
    	'loan_summary_tenures':loan_summary_tenures,
        'featured_loans':featured_loans,
        'banks':banks,
        'show_subscription_notification':show_subscription_notification(request,subscription_type=loan_type.loan_type),
        'subscription_type':loan_type.loan_type
    	})

def handle_loan_request_form_from_loan_summary_table(request_post,loan_type):
    request_post['principal'] = request_post['principal'].replace(',','')
    print("=====REQUEST====")
    print(request_post)
    # TODO: NEW FORM CREATE & SAVE METHOD IS MISSING
    redirect_to = reverse('loan_detail_b',
        kwargs={
            'loan_type':request_post['loan_type'],
            'bank_slug':request_post['bank_slug'],
            'principal':request_post['principal'],
            'loan_type':loan_type,
            'tenure':request_post['tenure'],}
    )
    print("redirecting to: "+redirect_to)
    return HttpResponseRedirect(redirect_to)

def handle_loantype_page_request(request, loan_type):
    if (request.method=='POST'):
        request_post = request.POST.copy()
        if ("main-request" in request.POST):
            return handle_loan_request_form(request)
        elif ("request-from-loan-summary-table" in request.POST):
            return handle_loan_request_form_from_loan_summary_table(request_post,loan_type)
        # elif ("apply-button" in request.POST):
        #     return handle_apply_request(request.POST)
    loan_type = LoanType.objects.get(loan_type=loan_type)
    return handle_get_request(request, loan_type=loan_type)

def ihtiyac_kredisi(request):
    return handle_loantype_page_request(request, loan_type='GPL')

def tasit_kredisi_0km(request):
    return handle_loantype_page_request(request, loan_type='CAR')
    
def tasit_kredisi_2el(request):
    return handle_loantype_page_request(request, loan_type='CA2')

def konut_kredisi(request):
    return handle_loantype_page_request(request, loan_type='MOR')
