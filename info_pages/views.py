from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from blog.models import PostPage

neden_digikredi_post = PostPage.objects.get(pk=22)
gizlilik_ve_cerez_politikasi_post = PostPage.objects.get(pk=22)
kisisel_verilerin_korunmasi_post = PostPage.objects.get(pk=22)
acik_riza_metni_post = PostPage.objects.get(pk=22)
kullanim_kosullari_post = PostPage.objects.get(pk=22)


def neden_digikredi(request):
	return render(request, 'info_pages/info_page.html', {'post':neden_digikredi_post})

def gizlilik_ve_cerez_politikasi(request):
	return render(request, 'info_pages/info_page.html', {'post':gizlilik_ve_cerez_politikasi_post})

def kisisel_verilerin_korunmasi(request):
	return render(request, 'info_pages/info_page.html', {'post':kisisel_verilerin_korunmasi_post})

def acik_riza_metni(request):
	return render(request, 'info_pages/info_page.html', {'post':acik_riza_metni_post})

def kullanim_kosullari(request):
	return render(request, 'info_pages/info_page.html', {'post':kullanim_kosullari_post})

def iletisim_formu(request):
	return render(request, 'info_pages/info_page.html', {})