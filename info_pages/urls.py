from django.urls import path
from django.urls import include, re_path
from . import views

urlpatterns = [
    path('neden-digikredi/', views.neden_digikredi, name='neden_digikredi'),
    path('gizlilik-ve-cerez-politikasi/', views.gizlilik_ve_cerez_politikasi, name='gizlilik_ve_cerez_politikasi'),
	path('kisisel-verilerin-korunmasi/', views.kisisel_verilerin_korunmasi, name='kisisel_verilerin_korunmasi'),
	path('acik-riza-metni/', views.acik_riza_metni, name='acik_riza_metni'),
	path('iletisim-formu/', views.iletisim_formu, name='iletisim_formu'),
	path('kullanim-kosullari/', views.kullanim_kosullari, name='kullanim_kosullari')
]