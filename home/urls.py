from django.urls import path

from . import views
from . import cart_views
from . import ajax_queries

urlpatterns = [
	path('', views.index, name='index'),
	path('kredi/', views.kredi, name='kredi'),
	path('no-notification', views.no_notification, name='no_notification'),
	path('subscribe', views.subscribe, name='subscribe'),
	path('subscribe-to-alerts', views.subscribe_to_alerts, name='subscribe_to_alerts'),
	path('search-across-all-banks', views.search_across_all_banks, name='search_across_all_banks'),
	path('banka', ajax_queries.banka_listesi, name='banka_listesi'),
	path('ajax/load-tenures/', views.load_tenures, name='ajax_load_tenures'),
	path('ajax/get-principal-limits/', views.get_principal_limits, name='ajax_get_principal_limits'),
	path('ajax/get-tenure-limits/', views.get_tenure_limits, name='ajax_get_tenure_limits'),
	path('ajax/render-cart/', cart_views.render_cart, name='ajax_render_cart'),
	path('ajax/featured-loan-refresh/', ajax_queries.featured_loan_refresh, name='ajax_featured_loan_refresh'),
	path('ajax/render-min-max-interest/', ajax_queries.render_min_max_interest, name='ajax_render_min_max_interest'),
	path('ajax/render-loanmatrix-tables/', ajax_queries.render_loanmatrix_tables, name='ajax_render_loanmatrix_tables'),
	path('ajax/render-loansummary/', ajax_queries.render_loansummary, name='ajax_render_loansummary'),
	path('ajax/render-banks/', ajax_queries.render_banks, name='ajax_render_banks'),
	path('ajax/render-featured-loans/', ajax_queries.render_featured_loans, name='ajax_render_featured_loans'),
	# path('ajax/render-blog/', ajax_queries.render_blog, name='ajax_render_blog'),
	# path('ajax/render-faq/', ajax_queries.render_faq, name='ajax_render_faq'),
]