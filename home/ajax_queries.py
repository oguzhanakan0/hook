from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.db.models import Max
from scripts.py.payment_plan import payment_plan
from loans.models import Loan
import pandas as pd
import pprint
import json
from loans.ajax_queries import get_liked_loans_from_session
from home.cart_views import transfer_liked_loans_to_list
from loans.views import calculate_loan_details
from datetime import date as dt
from datetime import datetime as dtm
from datetime import timedelta
from loans.models import Loan, Tenure, LoanType, LoanSummary, LoanMatrixTable, Bank
from blog.models import PostPage
from seo.models import FAQItem

today = dt.today() # if dtm.now().hour>5 else dt.today()+timedelta(days=-1)
loan_summary_max_date = LoanSummary.objects.aggregate(Max('date'))['date__max']
loan_matrix_max_date = LoanMatrixTable.objects.aggregate(Max('date'))['date__max']

print(f"System date: {today.strftime('%Y-%m-%d')}")
loan_matrix_tables = LoanMatrixTable.objects.filter(date=loan_matrix_max_date)
loan_summary = LoanSummary.objects.filter(date=loan_summary_max_date).order_by('min_interest')
loan_summary_tenures = dict(zip([e.id for e in loan_summary],[list(e.tenures.all().order_by('tenure')) for e in loan_summary]))
banks = Bank.objects.filter(active=1).order_by('-sponsored','name')
# featured_loans = [calculate_loan_details(e, int(e.principal.principal), int(e.tenure.tenure)) for e in Loan.objects.filter(is_featured=True,loan_type='GPL').order_by('feature_order')[:4]]
# blogs = PostPage.objects.all().order_by('-date')[:4]
# faqs  = FAQItem.objects.filter(relevant_to='homepage')
# for blog in blogs:
#     blog.title = blog.title if len(blog.title)<=50 else blog.title[:50]+" ..."
#     blog.body = blog.body[:100]+" ..."

def banka_listesi(request):
    return render(request,'home/global/bank_list_page.html', {'banks':banks})

def featured_loan_refresh(request):
    print("="*10+" featured_loan_refresh "+"="*10)
    pprint.pprint(request.GET)
    try:
        tenure = request.GET['tenure']
        principal = request.GET['principal']
        slug = request.GET['slug']
        slug = slug.split('_')[-1]
        loan = Loan.objects.filter(tenure__gte=tenure,principal__gte=principal,slug=slug).order_by('tenure','principal').first()
        return render(request, 'home/local/featured_loan_item_details.html', {
            'loan':calculate_loan_details(loan,principal=int(principal),tenure=int(tenure)),
            'request_source':'home-featured-loans'})
    except:
        return render(request,'home/local/featured_loan_item_details_error.html', {})

def render_min_max_interest(request):
    print("="*10+" render_min_max_interest "+"="*10)
    pprint.pprint(request.GET)
    try:
        tenure = request.GET['tenure']
        superior_loan = Loan.objects.filter(tenure__gte=request.GET['tenure'],principal__gte=request.GET['principal'],loan_type=request.GET['loan_type'],active=True).order_by('tenure','principal','interest').first()
        inferior_loan = Loan.objects.filter(tenure__gte=request.GET['tenure'],principal__gte=request.GET['principal'],loan_type=request.GET['loan_type'],active=True).order_by('tenure','principal','-interest').first()
        return render(request, 'home/local/min_max_interest_section.html', {
                'superior_loan':calculate_loan_details(superior_loan,tenure=int(request.GET['tenure']),principal=int(request.GET['principal'])),
                'inferior_loan':calculate_loan_details(inferior_loan,tenure=int(request.GET['tenure']),principal=int(request.GET['principal'])),
        })
    except:
        return HttpResponse('')

def render_loanmatrix_tables(request):
    print("="*10+" render_loanmatrix_tables "+"="*10)
    try:
        return render(
            request,
            'home/local/loan_matrix_section.html',
            {'loan_matrix_tables':loan_matrix_tables}
        )
    except:
        return HttpResponse('')

def render_featured_loans(request):
    print("="*10+" render_featured_loans "+"="*10)
    try:
        return render(
            request,
            'home/local/featured_loans_section.html',
            {'featured_loans':featured_loans,
             'request_source':'home-featured-loans'}
        )
    except:
        return HttpResponse('')


def render_loansummary(request):
    print("="*10+" render_loansummary "+"="*10)
    try:
        return render(
            request,
            'home/local/loan_summary_section.html',
            {'loan_summary':loan_summary,'loan_summary_tenures':loan_summary_tenures}
        )
    except:
        return HttpResponse('')

def render_banks(request):
    print("="*10+" render_banks "+"="*10)
    try:
        return render(
            request,
            'home/global/bank_list_section.html',
            {'banks':banks,
            "title":'Kredi Çekebileceğiniz',
            "subtitle":'bankalar',
            "description":''
            }
        )
    except:
        return HttpResponse('')

# def render_blog(request):
#     print("="*10+" render_blog "+"="*10)
#     try:
#         return render(
#             request,
#             'home/local/blog_section.html',
#             {
#                 "blogs":blogs,
#                 "title":"DigiKredi'den",
#                 "subtitle":'Haberler',
#                 "description":''
#             }
#         )
#     except:
#         return HttpResponse('')

# def render_faq(request):
#     print("="*10+" render_faq "+"="*10)
#     try:
#         return render(
#             request,
#             'home/local/faq_section.html',
#             {
#                 "faqs":faqs,
#                 "title":'Krediler Hakkında',
#                 "subtitle":'Merak Edilenler',
#                 "description":''
#             }
#         )
#     except:
#         return HttpResponse('')
