from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User, Group
from django.contrib.sessions.models import Session
from rest_framework import viewsets
from home.serializers import UserSerializer, GroupSerializer
from loans.models import Loan, Tenure, LoanType, LoanSummary, LoanMatrixTable, Bank
from userlogs.models import LoanRequest, SubscriptionType
from seo.models import FAQItem
from userlogs.forms import LoanRequestForm, SubscriptionForm
from blog.models import PostPage
from django.urls import reverse
from django.db.models import Min, Max
from datetime import date as dt
from datetime import datetime as dtm
from datetime import timedelta
from loans.views import calculate_loan_details, calculate_installment, show_subscription_notification, handle_loan_request_form
from userlogs.forms import AlertSubscriptionForm
from tools.ajax_queries import fix_inputs
from scripts.py.loan_comparison import *
import pprint
import json


blogs = PostPage.objects.all().order_by('-date')[:4]
for blog in blogs:
    blog.title = blog.title if len(blog.title)<=50 else blog.title[:50]+" ..."
    blog.body = blog.body[:100]+" ..."
faqs  = FAQItem.objects.filter(relevant_to='homepage')

index_blog_post = PostPage.objects.get(pk=22)


# Create your views here.
def index(request):
    if not request.session.session_key: request.session.save()
    return render(request, 'index.html',{'post':index_blog_post})

def kredi(request):
    """
    Homepage renderer function. Redirection depends on request type, otherwise will render default index page.
    << TODO: This function needs reorganization. >>
    # currently request.method == 'POST' is being redirected only to loan query page. We will change the condition to request.POST['request_type'] == xyz.
    """
    # print("======= REQUEST.METHOD =========")
    # print(request.method)
    # print("======= REQUEST.POST - MAIN =========")
    # print(
    #     'request_post:'+'\n'+\
    #     json.dumps(request.POST.dict())
    # )

    if not request.session.session_key:
        request.session.save()
    
    if (request.method=='POST'):
        request_post = request.POST.dict()
        request_post['session'] = request.session.session_key
        # print("======REQUEST.SESSION=======")
        # print(request.session)
        # print(request.session.session_key)
        # request_post['session'] = 
        if ("main-request" in request.POST):
            # print("=====REQUEST BEFORE ====")
            # print(request_post)
            request_post['principal'] = request_post['principal'].replace('.','')
            # print("=====REQUEST AFTER====")
            # print(request_post)
            # print("=====REQUEST.POST====")
            # print(request_post)
            # request_post['request_source'] = 'home-main'
            form = LoanRequestForm(request_post)
            if (form.is_valid()):
                print("======= FORM IS VALID =========")
                print(
                    'errors:'+'\n'+\
                    json.dumps(form.errors)+'\n'+\
                    'request_post:'+'\n'+\
                    json.dumps(request_post)
                )
                form.save()
                redirect_to = reverse('loan_list',
                    kwargs={
                        'principal':request_post['principal'],
                        'loan_type':request_post['loan_type'],
                        'tenure':request_post['tenure'],})
                # print("redirecting to: "+redirect_to)
                return HttpResponseRedirect(redirect_to)
            else:
                print("======= FORM IS NOT VALID =========")
                print(form.errors)
                return HttpResponse('form is not valid:'+'\n'+\
                    'errors:'+'\n'+\
                    json.dumps(form.errors)+'\n'+\
                    'request_post:'+'\n'+\
                    json.dumps(request_post)
                )
        # elif ("request-from-loan-summary-table" in request.POST):
        #     request_post['principal'] = request_post['principal'].replace(',','')
        #     # print("=====REQUEST====")
        #     # print(request_post)
        #     # TODO: NEW FORM CREATE & SAVE METHOD IS MISSING
        #     redirect_to = reverse('loan_detail_b',
        #         kwargs={
        #             'loan_type':request_post['loan_type'],
        #             'bank_slug':request_post['bank_slug'],
        #             'principal':request_post['principal'],
        #             'loan_type':request_post['loan_type'],
        #             'tenure':request_post['tenure'],}
        #     )
        #     print("redirecting to: "+redirect_to)
        #     return HttpResponseRedirect(redirect_to)
        # elif ("apply-button" in request.POST):
        #     # print("="*10+" Clicked on Apply button "+"="*10)
        #     # pprint.pprint(request.POST)
        #     return handle_apply_request(request.POST)
        # elif 'apply-all' in request.POST:
        #     print("============== apply-all =============")
        #     request_post = request.session['favorite_loans'].copy()
        #     request_post['apply-all']=1
        #     try:
        #         print("============== apply-all =============")
        #         return handle_apply_request(request_post)
        #     except:
        #         print("============== apply-all =============")
        #         return HttpResponse('an error occured. you probably dont have any liked loans?')
    else:
        # Homepage is rendered with the code below

        # print("="*15,"SESSION","="*15)
        # pprint.pprint(Session.objects.get(pk=request.session.session_key).get_decoded())
        
        # today = dt.today() if dtm.now().hour>5 else dt.today()+timedelta(days=-1)
        # loan_summary = LoanSummary.objects.filter(date=today).order_by('min_interest')
        # loan_summary_tenures = dict(zip([e.id for e in loan_summary],[list(e.tenures.all().order_by('tenure')) for e in loan_summary]))
        return render(request, 'kredi_page.html', {
            # 'loan_summary':loan_summary,'loan_summary_tenures':loan_summary_tenures
                'show_subscription_notification':show_subscription_notification(request,subscription_type='general'),
                'subscription_type':'general',
                'faqs':faqs,
                'blogs':blogs
            })

def load_tenures(request):
    """
    Filters dropdown options depending on *request*. Tenure options may be filtered according to either "slug" of the loan or "loan_type"
    << TODO: This function needs reorganization. >>
    """
    if request.GET.get('slug')==None:
        loan_type = request.GET.get('loan_type')
        print('load_tenures running with loan_type = '+ str(loan_type))
        tenures = LoanType.objects.get(loan_type=loan_type).tenures.all().exclude(tenure=0).order_by('tenure')
    else:
        tenure_ids = Loan.objects.filter(slug=request.GET.get('slug')).values_list('tenure', flat=True)
        print("tenure_ids")
        print(tenure_ids)
        tenures = Tenure.objects.filter(tenure__in=set(tenure_ids))
    limits = tenures.aggregate(Min('tenure'),Max('tenure'))
    return render(request, 'hr/tenure_dropdown_list_options.html', {'tenures': tenures, 'min': limits['tenure__min'], 'max':limits['tenure__max'], 'initial_tenure':request.GET.get('initial_tenure')})



def get_tenure_limits(request):
    """
    Filters dropdown options depending on *request*. Tenure options may be filtered according to either "slug" of the loan or "loan_type"
    << TODO: This function needs reorganization. >>
    """
    print('>>>>>> get_tenure_limits: request.GET:')
    print(request.GET)
    if request.GET.get('slug')!=None:
        tenure_ids = Loan.objects.filter(slug=request.GET.get('slug'),active=True).values_list('tenure', flat=True)
        tenures = Tenure.objects.filter(tenure__in=set(tenure_ids)).exclude(tenure=0).order_by('tenure')
    else:
        if request.GET.get('bank')!=None:
            bank = Bank.objects.get(slug=request.GET.get('bank'))
            loan_type = LoanType.objects.get(loan_type=request.GET.get('loan_type'))
            tenure_ids = Loan.objects.filter(bank=bank,loan_type=loan_type,active=True).values_list('tenure', flat=True)
            tenures = Tenure.objects.filter(tenure__in=set(tenure_ids)).exclude(tenure=0).order_by('tenure')
        else:
            tenures = LoanType.objects.get(loan_type=request.GET.get('loan_type'),active=True).tenures.all().exclude(tenure=0).order_by('tenure')
    try: c = request.GET['class']
    except: c = None
    limits = tenures.aggregate(Min('tenure'),Max('tenure'))
    initial_tenure = request.GET.get('initial_tenure') if request.GET.get('initial_tenure')!=None else limits['tenure__min'] if request.GET.get('for_featured_loan') else LoanType.objects.get(loan_type=request.GET.get('loan_type')).default_tenure if request.GET.get('loan_type')!=None else 12
    dct = {
        'tenures': tenures,
        'min': limits['tenure__min'],
        'max':limits['tenure__max'],
        'initial_tenure':initial_tenure,
        'featured_loan':0 if not request.GET.get('for_featured_loan') else 1,
        'label_class':c
    }
    print(">>>>>> TENURE LIMITS RETURNING:")
    pprint.pprint(dct)
    return render(request, 'loans/widgets/tenure_input_widget.html', dct)

def get_principal_limits(request):
    print('get_principal_limits running')
    pprint.pprint(request.GET)
    # request_get = fix_inputs(request.GET.copy(),integers=['principal'])
    if request.GET.get('slug')!=None:
        principals = Loan.objects.filter(slug=request.GET.get('slug'),active=True).exclude(principal=0).aggregate(Min('principal'),Max('principal'))
    else:
        if request.GET.get('bank')!=None:
            bank = Bank.objects.get(slug=request.GET.get('bank'))
            loan_type = LoanType.objects.get(loan_type=request.GET.get('loan_type'))
            principals = Loan.objects.filter(bank=bank,loan_type=loan_type,active=True).exclude(principal=0).aggregate(Min('principal'),Max('principal'))
        else:
            principals = Loan.objects.filter(loan_type=request.GET.get('loan_type'),active=True).exclude(principal=0).aggregate(Min('principal'),Max('principal'))
    try: c = request.GET['class']
    except: c = None
    initial_principal = int(str(request.GET.get('initial_principal')).replace('.','')) if request.GET.get('initial_principal')!=None else principals['principal__max'] if request.GET.get('for_featured_loan') else LoanType.objects.get(loan_type=request.GET.get('loan_type')).default_principal if request.GET.get('loan_type')!=None else 10000
    dct ={
            'min':str(principals['principal__min']),
            'max': str(principals['principal__max']),
            'initial_principal':str(initial_principal),
            'featured_loan':0 if not request.GET.get('for_featured_loan') else 1,
            'label_class':c
          }
    print(">>>>> PRINCIPAL LIMITS RETURNING:")
    pprint.pprint(dct)
    return render(request, 'loans/widgets/principal_input_widget.html', dct)

def search_across_all_banks(request):
    return handle_loan_request_form(request)
    # return HttpResponse('something')

def subscribe(request):
    request_post = request.POST.dict()
    request_post['session'] = request.session.session_key
    # print("=====SUBSCRIPTION REQUEST======")
    # print(request_post)
    form = SubscriptionForm(request_post)
    form.save()
    # try: request.session['subscription-list']
    # except: request.session['subscription-list'] = []
    # request.session['subscription-list'].append(form['subscription_type'].value())
    request.session['no-notification'] = True
    request.session['email'] = request_post['email']
    return render(request, 'hr/subscription_success.html', {})

def no_notification(request):
    request.session['no-notification'] = True
    return render(request, 'hr/subscription_noshow.html', {})
    # return HttpResponse('success!')

def subscribe_to_alerts(request):
    print("=====ALERT SUBSCRIPTION REQUEST======")
    print(request.POST)

    request_post = request.POST.dict()
    request.session['no-notification'] = True
    request.session['email'] = request_post['email']
    request_post['session'] = request.session.session_key
    request_post = fix_inputs(request_post, integers=['principal','tenure'], floats=[])
    
    print(request_post)
    form = AlertSubscriptionForm(request_post)
    try:
        form.save()
    except Exception as e:
        print("FORM CANT BE SAVED:")
        print(e)
        print(form.errors)
        return HttpResponse(form.errors)
    # try: request.session['subscription-list']
    # except: request.session['subscription-list'] = []
    # request.session['subscription-list'].append(form['subscription_type'].value())
    return render(request, 'hr/subscription_success_with_close_btn.html', {})