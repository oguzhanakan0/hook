// LOANS/JS/REQUERY_LOAN_DETAIL.JS
$(document).ready(function() {
    // console.log("document ready!");
    if (window.location.pathname.includes('detay')) {
      var loc = window.location.pathname;
      loc = loc.substr(loc.lastIndexOf('/')+1)
      loc = loc.split("&").map(v => v.split("=")).reduce( (pre, [key, value]) => ({ ...pre, [key]: value }), {} );
      
      // var initial_tenure = document.getElementById("initial_tenure_id").getAttribute("value");
      // var initial_principal = document.getElementById("initial_principal_id").getAttribute("value");
      console.log('getting principal limits with loc:');
      console.log(loc);
      runAjaxQuery('/ajax/get-principal-limits/',
        {
          'bank': loc['banka-adi'], 
          'loan_type': loc['kredi-tipi'],
          'initial_principal': loc['tutar'],
        },
        "#principal-input-div");
      // Get tenures
      runAjaxQuery('/ajax/get-tenure-limits/',
        {
          'bank': loc['banka-adi'],
          'loan_type': loc['kredi-tipi'],
          'initial_tenure': loc['vade']
        },
        "#tenure-input-div");
    }
    // console.log(slug)
});
// END LOANS/JS/REQUERY_LOAN_DETAIL.JS

// HOME/JS/CART_MAIN.JS
console.log("cart_main.js working");
jQuery(document).ready(function($){
	//if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
	var $L = 1200,
		$menu_navigation = $('#main-nav'),
		$cart_trigger = $('#cd-cart-trigger'),
		$hamburger_icon = $('#cd-hamburger-menu'),
		$lateral_cart = $('#cd-cart'),
		$shadow_layer = $('#cd-shadow-layer');

	//open lateral menu on mobile
	$hamburger_icon.on('click', function(event){
		event.preventDefault();
		//close cart panel (if it's open)
		$lateral_cart.removeClass('speed-in');
		toggle_panel_visibility($menu_navigation, $shadow_layer, $('body'));
	});

	//open cart
	$cart_trigger.on('click', function(event){
		event.preventDefault();
		//close lateral menu (if it's open)
		$menu_navigation.removeClass('speed-in');
		toggle_panel_visibility($lateral_cart, $shadow_layer, $('body'));
	});

	//close lateral cart or lateral menu
	$shadow_layer.on('click', function(){
		$shadow_layer.removeClass('is-visible');
		// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
		if( $lateral_cart.hasClass('speed-in') ) {
			$lateral_cart.removeClass('speed-in').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').removeClass('overflow-hidden');
			});
			$menu_navigation.removeClass('speed-in');
		} else {
			$menu_navigation.removeClass('speed-in').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').removeClass('overflow-hidden');
			});
			$lateral_cart.removeClass('speed-in');
		}
	});

	//move #main-navigation inside header on laptop
	//insert #main-navigation after header on mobile
	move_navigation( $menu_navigation, $L);
	$(window).on('resize', function(){
		move_navigation( $menu_navigation, $L);
		
		if( $(window).width() >= $L && $menu_navigation.hasClass('speed-in')) {
			$menu_navigation.removeClass('speed-in');
			$shadow_layer.removeClass('is-visible');
			$('body').removeClass('overflow-hidden');
		}

	});
});

function toggle_panel_visibility ($lateral_panel, $background_layer, $body) {
	if( $lateral_panel.hasClass('speed-in') ) {
		// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
		$lateral_panel.removeClass('speed-in').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$body.removeClass('overflow-hidden');
		});
		$background_layer.removeClass('is-visible');

	} else {
		$lateral_panel.addClass('speed-in').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$body.addClass('overflow-hidden');
		});
		$background_layer.addClass('is-visible');
	}
}

function move_navigation( $navigation, $MQ) {
	if ( $(window).width() >= $MQ ) {
		$navigation.detach();
		$navigation.appendTo('header');
	} else {
		$navigation.detach();
		$navigation.insertAfter('header');
	}
}

// END HOME/JS/CART_MAIN.JS

// HOME/JS/FAVORITE_ICON.JS
var $loading = $("#ajax-loader");
$(document)
    .ajaxStart(function () {
        $loading.show();
    })
    .ajaxStop(function () {
        $loading.hide();
        $("#lblCartCount").html(Math.max(0,$('ul[class="cd-cart-items"] > li').length-1));
});

// THIS FUNCTIONS NEED TO BE RETRIEVED FROM A SEPARATE FILE!!
function runAjaxQuery(url_,data_,return_to_) {
    $.ajax({
        url: url_,
        data: data_,
        success: function (data) {
            console.log('query success');
            // $(return_to_).addClass('fade-in');
            $(return_to_).html(data);
        }
    });
}

function runAjaxQueryWithUrlAndDataOnly(url_,data_) {
    return $.ajax({
        url: url_,
        data: data_,
        success: function (data) {
      console.log('ajax query response:');
            console.log(data);
            // $(return_to_).html(data);
        }
    });
}

// END OF THIS FUNCTIONS NEED TO BE RETRIEVED FROM A SEPARATE FILE!!

function getLikedLoans(url_) {
    $.ajax({
        url: url_,
        success: function (data) {
            if(data!="no-liked-loans"){
        var liked_loans = JSON.parse(data);
        var liked_loan_keys = Object.keys(liked_loans)
        console.log(liked_loan_keys);
        // BELOW CODE COLORS UP THE LIKE BUTTONS
        $("i[class^='like-icon']").each(function() {
          lk = $(this).attr('loan-key');
          lk = lk.substring(0,lk.lastIndexOf('&'));
          console.log('searching: '+lk);
          if(!(liked_loan_keys.includes(lk))){
            console.log("removing attributes of: "+$(this).attr('loan-key'));
            $(this).html('<span class="display-8"> Kaydet</span>');
            $(this).removeClass( "like-icon-press", 1000 );
            $(this).removeClass( "liked-text", 1000 );
            $(this).removeClass( "liked-text-press", 1000 );
            $(this).attr("is-liked",0);
          } else {
            console.log("adding attributes to: "+$(this).attr('loan-key'));
            $(this).html('<span class="display-8"> Kayıtlı</span>');
            $(this).addClass( "like-icon-press", 1000 );
            $(this).addClass( "liked-text", 1000 );
            $(this).addClass( "liked-text-press", 1000 );
            $(this).attr("is-liked",1);
          }
        });
        // BELOW CODE FILLS THE CART
        $.when(runAjaxQuery('/ajax/render-cart/',{'favorite_loans':data},"div[id='cd-cart']")).then(function () {
          refreshCartButtons();
        })
        console.log("cart rendered successfully");

        // console.log("WINDOW.LOCATION.PATHNAME:");
        if(window.location.pathname=='/sepet/'){
          $.when(runAjaxQuery('/sepet/ajax/sepet-refresh/',{
              'favorite_loans':data,
              'url':window.location.pathname},
              "div[id='cart-page-content']"
              )).then(function () {
                refreshCartButtons();
            });
          }
        }
      }
    });
}

$(document).ready(function() {
    // console.log("document ready!")
    // $("i[class='like-icon']").each(function() {
    //  $(this).html('<span class="display-8"> Favorilere ekle</span>');
    // });
    getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
    enablePaymentPlanButton();

    // setTimeout(function(){ showNotification("subscription-notification-fullpage"); }, 100);

    // DISABLE ENTER FROM SUBMITTING FORMS
    $(window).keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });
    

  // $('form').on('submit', function(e){
  //        e.preventDefault();
  //        var newval = $("input[name='principal']").val().split('.').join("");
  //  $("input[name='principal']").val(newval);
  //  this.submit();
  //    });

});

function showNotification(id){
  try {
    document.getElementById(id).style.width = "100%";
  } catch(err) {}
}

function hideNotification(id){
  try {
    document.getElementById(id).style.width = "0%";
  } catch(err) {}
}

// SUBSCRIPTION FORM
$("#home-subscription-form").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var url = form.attr('action');
    console.log("=== form ===");
    console.log(form.serialize());
    runAjaxQuery(url,form.serialize(),"#home-subscription-form",'POST')
});

function toggleLikeIconClasses(icon) {
  icon.toggleClass( "like-icon-press", 1000 );
  icon.toggleClass( "liked-text", 1000 );
  icon.toggleClass( "liked-text-press", 1000 );
}

$(function() {

  $("i[class='like-icon']").click(function() {

    if($("#session-email").val()=="None"){
      console.log('here1');
      $("#show-subscription-notification").val("False");
      editSubscriptionOverlayContent();
      openSubscriptionOverlay($(this));
    } else {
      $(this).attr("is-liked",$(this).attr("is-liked")==1?0:1);
      if($(this).attr('is-liked')==1){
          console.log('here2');
          $.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/write-liked-loan-to-session/',
            {
              'loan_key':$(this).attr('loan-key'),
            })).then(function () {
              toggleLikeIconClasses($(this)),
              getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
          });
        
      } else {
        $.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/remove-liked-loan-from-session/',
          {
            'loan_key':$(this).attr('loan-key')
          })).then(function () {
            toggleLikeIconClasses($(this)),
            getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
        });
      }
    }
  });
});

function refreshCartButtons() {
    console.log('refreshing cart buttons');
    console.log('n liked loans: ');
    console.log($("div[class^='checkbox']").length);

    // BELOW IS FOR DISABLING/ENABLING COMPARE BUTTONS
    if($("div[class^='checkbox']").length<2){
      $("button[name*='compare-']").prop('disabled', true);
      $("button[name*='compare-']").removeClass('bg-secondary');
    } else{
      $("button[name*='compare-']").prop('disabled', false);
      if(!$("button[name*='compare-']").hasClass("bg-secondary")){
         $("button[name*='compare-']").addClass("bg-secondary");
      }
    }

    // BELOW IS FOR DISABLING/ENABLING APPLY BUTTONS
    var total_apply_buttons = 0;
    $('ul[class="cd-cart-items"] > li').each(function() {
      total_apply_buttons += $(this).find("button[name='apply-button']").length;
    });
    console.log('total_apply_buttons:');
    console.log(total_apply_buttons);
    if(total_apply_buttons==0){
      console.log('disabling apply buttons');
      $("button[name='apply-all'], button[name='apply-selected']").prop('disabled', true);
      // $("button[name='apply-all']","button[name='apply-selected']").removeClass('general-button');
    } else {
      $("button[name='apply-all'], button[name='apply-selected']").prop('disabled', false);
      // if(!$("button[name*='apply-']").hasClass("apply-button")){
      //    $("button[name*='apply-']").addClass("apply-button");
      // }
    }
}


// REMOVE LIKED LOAN USING X BUTTON ON SIDE CART AND IN THE ACTUAL CART
$(document).on('click', 'a[class^="loan-remove-property"]', function(){
    console.log('clicked on remove button with loan key: '+$(this).attr('loan-key'));
  $.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/remove-liked-loan-from-session/',
    {
      'loan_key':$(this).attr('loan-key')
    })).then(function () {
      getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
  });
});
//////////////
$(document).on('click', 'i[class^="re-like-icon"]',function() {
  console.log('clicked on like button with loan key: '+$(this).attr('loan-key'));
  $(this).toggleClass( "like-icon-press", 1000 );
  $(this).toggleClass( "liked-text", 1000 );
  $(this).toggleClass( "liked-text-press", 1000 );
  // $(this).toggleHtml('', '<span class="display-8"> eklendi</span>');
  $.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/write-liked-loan-to-session/',
    {
      'loan_key':$(this).attr('loan-key'),
    })).then(function () {
      getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
  });
});

$('.field__input').on('input', function() {
  var $field = $(this).closest('.field');
  if (this.value) {
    $field.addClass('field--not-empty');
  } else {
    $field.removeClass('field--not-empty');
  }
});



$(document).on('input', '.principal-input', function (e) {
  var v = $(this).val().replaceAll('.','');
  if (/^\d+$/.test(v)) {
    var start = e.target.selectionStart, end = e.target.selectionEnd;
    var v = parseInt(v);
    v = v ? Intl.NumberFormat('tr-TR').format(v) : '' ;
    diff = $(this).val().length - v.length;
    $(this).val(v);
    e.target.setSelectionRange(start-diff, end-diff);
  } else {
    $(this).val(
      $(this).val().substring(0, $(this).val().length - 1)
    );
  }
})


$(document).on('input', '.interest-input', function (e) {
  var v = $(this).val().replaceAll(',','');
  if (/^[0-9,]+$/.test(v)) {
    // var start = e.target.selectionStart, end = e.target.selectionEnd;
    v = parseInt(v);
    v = v ? (v<10 ? $(this).val(): 
              (v<100 ? Intl.NumberFormat('tr-TR').format(v/10) : 
                Intl.NumberFormat('tr-TR').format(v/100))) : 
                  '' ;
    // diff = $(this).val().length - v.length;
    $(this).val(v);
    // e.target.setSelectionRange(start-diff, end-diff);
  } else {
    $(this).val(
      $(this).val().substring(0, $(this).val().length - 1)
    );
  }
})

function fixMinMaxLimits(a){
  var v = parseInt(a.val().replaceAll('.',''));
  v = v ? v : 0;
  var tmp = v;
  v = Math.min(Math.max(v,a.data('min')),a.data('max'));
  a.val(Intl.NumberFormat('tr-TR').format(v));
  if(tmp!=v) {
    var warning = a.parents().eq(1).find('span[name="principal-validator"]'); // 2 ustteki parenti bulup validatoru getir
    var buyuk_kucuk = tmp>v ? 'büyük' : tmp<v ? 'küçük': ''; // girilen deger minden mi kucuk, max'tan mi buyuk diye kontrol et
    warning.html('Girdiğiniz değer '+a.val()+" TL'den "+buyuk_kucuk+' olduğu için '+a.val()+" TL'ye eşitledik."); // validator'e uyari goster
    setTimeout(function(){warning.html('');}, 5000); // validator'den uyariyi sil.
    if(window.location.pathname=='/kredi/'){
      refreshMinMaxInterest();
    }
  }
}

$(document).on('focusout', '.principal-input', function (e) {
  console.log('fixing');
  if($(this).data('min') | $(this).data('max')) {
    fixMinMaxLimits($(this));
  }
})

// END HOME/JS/FAVORITE_ICON.JS

// HOME/JS/GO_TO_TOP.JS
var btn = document.getElementById("top-btn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    btn.style.display = "block";
  } else {
    btn.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
// END HOME/JS/GO_TO_TOP.JS


// SCRIPTS TO RUN IN /KREDI PAGE

// HOME/JS/LOAN_REQUEST_OPS.JS

// INITIAL CONFIGURATION


function setPrincipalLimitsToFeaturedLoans() {
    
    $("div[id^='featured_loan_id_form_']").each(function() {
        var principal = $(this).find("div[id^='featured_loan_id_principal_']");
        var tenure = $(this).find("div[id^='featured_loan_id_tenure_']");
        var details = $("ul[refers-to='"+$(this).attr("id")+"']")
        $.when(
                runAjaxQuery('/ajax/get-principal-limits/',
                    {'slug': principal.attr("slug"), "for_featured_loan":1, 'initial_principal': principal.attr("initial")},
                    principal),
                runAjaxQuery('/ajax/get-tenure-limits/',
                    {'slug': tenure.attr("slug"), "for_featured_loan":1, 'initial_tenure': tenure.attr("initial")},
                    tenure)
            ).then(function () {
                principal.change(function(){
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery('/ajax/featured-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
                        details);
                });
                tenure.change(function(){
                    // $(this).css("background-color", "#D6D6FF");
                    runAjaxQuery('/ajax/featured-loan-refresh/',
                        {'tenure': tenure.find(':input').val(), "principal":principal.find(':input').val(), "slug":principal.attr('slug')},
                        details);
                });
            }
        );
    });
}

function refreshMinMaxInterest(){
    runAjaxQuery(
        '/ajax/render-min-max-interest/',
        {
            'loan_type': $("input[id='id_loan_type']").attr('value'),
            'principal':parseInt($("div[id='main-request-principal']").find(':input').val().replaceAll('.','')),
            'tenure':parseInt($("div[id='main-request-tenure']").find('select').val())
        },
        "div[id='min-max-interest-section']");
}

function setLoanType(loan_type){
    console.log('setting loan_type to:');
    console.log(loan_type);
    $("input[id='id_loan_type']").attr('value',loan_type);
    var tenure = "div[id='main-request-tenure']";
    var principal = "div[id='main-request-principal']";
    // Get tenures
    $.when(
        runAjaxQuery('/ajax/get-tenure-limits/',{'loan_type': loan_type,'class':'display-7 form-text-color'},tenure),
        runAjaxQuery('/ajax/get-principal-limits/',{'loan_type': loan_type,'class':'display-7 form-text-color'},principal)
        ).then(function () {refreshMinMaxInterest()});
}

function runAjaxQuery(url_,data_,return_to_,type='GET') {
    // $(return_to_).addClass('loader');
    return $.ajax({
        type : type,
        url: url_,
        data: data_,
        success: function (data) {
            console.log('writing data to: '+$(return_to_).attr('id'));
            // $(return_to_).addClass('fade-in');
            $(return_to_).html(data);
            // $(return_to_).removeClass('loader');
        }
    });
}

$(document).ready(function() {
    if (window.location.pathname=='/kredi/') {
      setLoanType('GPL');
      $("div[id='main-request-tenure']").change(function(){refreshMinMaxInterest()});
      $("div[id='main-request-principal']").change(function(){refreshMinMaxInterest()});
      $("li[class='nav-item']").click(function () {setLoanType($(this).attr('loan-type'));});
      $("input[id='0km']").click(function () {setLoanType('CAR');});
      $("input[id='2el']").click(function () {setLoanType('CA2');});
      $("a[id='expand-loan-matrix-section']").one("click", function(){runAjaxQuery('/ajax/render-loanmatrix-tables/',{},"div[id='loanmatrix-tables-section']");});
      
      $("a[id='expand-loan-summary-section']").one("click", function(){
        $.when(
            runAjaxQuery('/ajax/render-loansummary/',{},"div[id='loansummary-section']")
          ).then(function () {
            refreshPrincipalInputs();
          });
      });
      
      $("a[id='expand-featured-loans-section']").one("click", function(){
          $.when(
              runAjaxQuery('/ajax/render-featured-loans/',{},"div[id='featured-loans-section']")
          ).then(function () {
              setPrincipalLimitsToFeaturedLoans();
          });
      });
      // $("a[id='expand-bank-list-section']").one("click", function(){runAjaxQuery('/ajax/render-banks/',{},"div[id='banks-section']");});
      $("a[id='expand-faq-section']").one("click", function(){$("#toggle1-z").show();});
      $("a[id='expand-blog-section']").one("click", function(){$("#features18-4").show();});

      // LOANS/JS/SUBSCRIBE.JS
      try {
          document.getElementById("toast-container").style.display = 'none';
          setTimeout(function(){ document.getElementById("toast-container").style.display = 'block'; }, 1000);
      } catch(err) {}

      $("#subscription-notification").submit(function(e) {
          e.preventDefault(); // avoid to execute the actual submit of the form.
          var form = $(this);
          var url = form.attr('action');
          // console.log("=== form ===");
          // console.log(form.serialize());
          runAjaxQuery(url,form.serialize(),"#subscription-notification",'POST');
          setTimeout(function(){ document.getElementById("toast-container").style.display = 'none'; }, 2000);
      });

      $("#no-notification").submit(function(e) {
          e.preventDefault(); // avoid to execute the actual submit of the form.
          var form = $(this);
          var url = form.attr('action');
          // console.log("=== form ===");
          // console.log(form.serialize());
          runAjaxQuery(url,form.serialize(),"#subscription-notification",'POST');
          setTimeout(function(){ document.getElementById("toast-container").style.display = 'none'; }, 2000);
      });
    }

    if (window.location.pathname.includes('/kredi/')) {
      console.log('expanding banks');
      $("a[id='expand-loan-matrix-section']").one("click", function(){runAjaxQuery('/ajax/render-loanmatrix-tables/',{},"div[id='loanmatrix-tables-section']");});
      
      $("a[id='expand-loan-summary-section']").one("click", function(){
        $.when(
            runAjaxQuery('/ajax/render-loansummary/',{},"div[id='loansummary-section']")
          ).then(function () {
            refreshPrincipalInputs();
          });
      });
      
      $("a[id='expand-bank-list-section']").one("click", function(){runAjaxQuery('/ajax/render-banks/',{},"div[id='banks-section']");});
    }

});

function setLoanTypeLoanQueryPage(loan_type, initial_tenure, initial_principal){
    var tenure = "div[id='main-request-tenure']";
    var principal = "div[id='main-request-principal']";
    // Get tenures & principals
    runAjaxQuery('/ajax/get-tenure-limits/',{'loan_type': loan_type,'class':'display-8', 'initial_tenure': initial_tenure},tenure);
    runAjaxQuery('/ajax/get-principal-limits/',{'loan_type': loan_type,'class':'display-8', 'initial_principal': initial_principal},principal);
}

// HESAPLAMA ARACLARI SAYFASI ICIN

$(document).ready(function() {
    if (window.location.pathname.includes('/kredi/hesaplama-araclari/')) {
      $("#side-div a").each( function() {
          if(window.location.pathname==$(this).attr('href')){
            console.log('working');
            $(this).find('div').css('background-color','var(--c7)');
          }
      });
    } else if (window.location.pathname.includes('sorgu')) {
      var loan_type = $("input[id='loan_type_id']").attr('value');
      var initial_principal = $("input[id='initial_principal_id']").attr('value');
      var initial_tenure = $("input[id='initial_tenure_id']").attr('value');
      setLoanTypeLoanQueryPage(loan_type, initial_tenure, initial_principal);
    }
});

function submitToolsForm(form_id,form_div_id,return_to_="#hesaplama-sonucu"){
  var form = $(form_id);
  $(form_id+' button').attr("disabled", true);
  $(form_id+' button').html('Hesaplanıyor <span class="lds-hourglass-button"></span>');
  if(form[0].checkValidity()) {
      console.log('valid form');
      // DELETE SETTIMEOUT LATER
      $.when(
        $(form_div_id).css('transform','translateX(0%)'),
        // $(return_to_).addClass('fade-in'),
        runAjaxQuery(form.attr('action'),form.serialize(),return_to_,type='POST'),
      ).then(function () {
        getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
        enablePaymentPlanButton();
        $(form_id+' button').attr("disabled", false);
        $(form_id+' button').html("Tekrar Hesapla");
      }); 
  } else {
    console.log('invalid form');
    $(form_id+' button').attr("disabled", false);
    $(form_id+' button').html("Hesapla");
  }
}

function enablePaymentPlanButton() {
  $("a[id='show-hide-payment-plan-btn']").click(function () {
    var table = document.getElementById("id_payment_plan");
    var show = table.getAttribute('show');
    if (show=='summary'){
      for (var i = 0, row; row = table.rows[i]; i++) {
         //iterate through rows
         //rows would be accessed using the "row" variable assigned in the for loop
         row.removeAttribute('hidden', '');
         $(this).html('Özet Göster &#8593;');
      }
      table.setAttribute('show','all');
    } else {
      for (var i = 11, row; row = table.rows[i]; i++) {
         //iterate through rows
         //rows would be accessed using the "row" variable assigned in the for loop
         
         row.setAttribute('hidden', '');
         $(this).html('Tamamını Göster &#8595;');
      }
      table.setAttribute('show','summary');
    }
  });
}

// FULL SCREEN OVERLAY

$(document).ready(function() {
  setAlertSubscriptionFormProps();
  if($("#show-subscription-notification").val()!="False"){
    setTimeout(function(){ 
      if($("#show-subscription-notification").val()!="False"){ // double checking is deliberate!
        openSubscriptionOverlay()
    } }, 10000);
  }
})

function editSubscriptionOverlayContent() {
  $("#subscription-overlay-title strong").html('E-posta adresiniz gerekiyor');
  $("#subscription-overlay-description").html("DigiKredi tamamen ücretsizdir, e-posta adresinizi belirterek kredi kaydetme özelliğini kullanmaya başlaayın.");
  document.getElementById("no-notification").style.display = "none" ;
}

function setAlertSubscriptionFormProps() {
  // THE GOAL IS TO CUSTOMIZE ALERT SUBSCRIPTION FORM BASED ON CURRENT URL.\
  // IF THERE ARE PARAMETERS LIKE LOAN_TYPE, PRINCIPAL AND TENURE IN THE URL,
  // WE SET ALERT FORM TO "CUSTOM" AND FILL THE FORM BEFOREHAND
  // OTHERWISE WE SET ALERT FORM TO "ALL-MAIL" AND DO NOTHING.
  // USER CAN STILL CHANGE THE DEFAULTS AND SUBMIT HIS EMAIL ADDRESS.
  $("#all-mail").on('click', function(event){
    document.getElementById("custom-options-pane").style.display = "none" ;
    document.getElementById("show-options-btn").style.display = "none" ;
    $("#show-options-btn").html('Düzenle&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>');
  });
  $("#custom-mail").on('click', function(event){
    // document.getElementById("custom-options-pane").style.display = "-webkit-flex" ;
    document.getElementById("show-options-btn").style.display = "inline-block" ;
  });
  var path = window.location.pathname;
  path = path.substring(path.lastIndexOf('/')).substring(1);
  if (path==""){
    // SUBSCRIBE TO ALL
    $("#all-mail").prop('checked', true);
    $("#custom-mail").on('click', function(event){
      document.getElementById("custom-options-pane").style.display = "-webkit-flex" ;
      $("#show-options-btn").html('Düzenle&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>');
    });
    // document.getElementById("custom-options-pane").style.display = "none" ;
  } else {
    // SUBSCRIBE CUSTOM
    $("#custom-mail").prop('checked', true);
    document.getElementById("show-options-btn").style.display = "inline-block" ;
    // document.getElementById("custom-options-pane").style.display = "-webkit-flex" ;
    params = JSON.parse('{"' + decodeURI(path).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
    for (param in params) {
      // console.log(param);
      switch(param) {
        case "kredi-tipi":
          $("#subscription-notification input[name='"+params[param]+"_check']").prop('checked', true);
          break;
        case "tutar":
          $("#subscription-notification input[name='principal']").val(Intl.NumberFormat('tr-TR').format(params[param]));
          break;
        case "vade":
          $("#subscription-notification input[name='tenure']").val(params[param]);
          break;
      }
    }
  }
}

function setAlertSubscriptionFormSubmit(like) {
  $("#subscription-notification").submit(function(e) {
      e.preventDefault(); // avoid to execute the actual submit of the form.
      var form = $(this);
      var url = form.attr('action');
      var serialized = form.serialize();
      var email=$("#subscription-notification input[name='email']").val();
      console.log(serialized);
      $.when(
          document.getElementById("no-notification").style.display = "none",
          $(this).html('<div class="row justify-content-center"><div class="lds-hourglass-ajax"></div></div>')
        ).then(function () {
          $.when(
            runAjaxQuery(url,serialized,"#subscription-notification",'POST')
          ).then(function () {
            console.log('here:');
            $("#session-email").val(email);
            if(like!=undefined){
              $.when(runAjaxQueryWithUrlAndDataOnly('/kredi/ajax/write-liked-loan-to-session/',
                {
                  'loan_key':like.attr('loan-key'),
                }
              )).then(function () {
                  getLikedLoans('/kredi/ajax/get-liked-loans-from-session/');
              });
            }
          });
      });
      
      // ENABLE BELOW LATER
      setTimeout(function(){ document.getElementById("subscription-overlay").style.display = 'none'; }, 2000);
  });

  $("#no-notification").submit(function(e) {
      e.preventDefault(); // avoid to execute the actual submit of the form.
      var form = $(this);
      var url = form.attr('action');
      var serialized = form.serialize();
      $.when(
          document.getElementById("subscription-notification").style.display = "none",
          $(this).html('<div class="row justify-content-center"><div class="lds-hourglass-ajax"></div></div>')
        ).then(function () {
          runAjaxQuery(url,serialized,"#no-notification",'POST');
      });

      // console.log("=== form ===");
      // console.log(form.serialize());
      // ENABLE BELOW LATER
      setTimeout(function(){ document.getElementById("subscription-overlay").style.display = 'none'; }, 2000);
  });
}

function openSubscriptionOverlay(like) {
  try {
    setAlertSubscriptionFormSubmit(like);
    document.getElementById("subscription-overlay").style.width = "100%";
  }
  catch(err) {}
}
function closeSubscriptionOverlay() {
  document.getElementById("subscription-overlay").style.width = "0%";
}

function toggleCustomOptions() {
  var cur = document.getElementById("custom-options-pane").style.display;
  if(cur=='-webkit-flex'){
    $("#show-options-btn").html('Düzenle&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>');
    document.getElementById("custom-options-pane").style.display = 'none'  
  } else {
    $("#show-options-btn").html('Düzenle&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>');
    document.getElementById("custom-options-pane").style.display = '-webkit-flex';
  }
}

// HAMBURGER MENU TOGGLE
function openHamburger() {
  document.getElementById("hamburger-menu-content").style.width = "100%";
}

function closeHamburger() {
  document.getElementById("hamburger-menu-content").style.width = "0%";
}