function runAjaxQuery(url_,data_,return_to_,type='GET') {
    // $(return_to_).addClass('loader');
    return $.ajax({
        type : type,
        url: url_,
        data: data_,
        success: function (data) {
            console.log('writing data to: '+$(return_to_).attr('id'));
            $(return_to_).html(data);
            // $(return_to_).removeClass('loader');
        }
    });
}

$(document).ready(function() {
	$("#home-subscription-form").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var url = form.attr('action');
        console.log("=== form ===");
        console.log(form.serialize());
        runAjaxQuery(url,form.serialize(),"#home-subscription-form",'POST')
    });
})