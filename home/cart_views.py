from django.shortcuts import render
import json
from loans.models import Loan, Principal, Tenure
import pprint
import datetime as dt

def transfer_liked_loans_to_list(liked_loans, sort=False):
    liked_loans_list = []
    for loan_key in liked_loans:
        try:
            loan_id, selected_principal, selected_tenure = loan_key.split('&')
            l = Loan.objects.get(pk=loan_id,active=True)
            if l:
                l.set_loan_details(int(selected_principal), int(selected_tenure))
                try:
                    l.favorited_at = dt.datetime.strptime(liked_loans[loan_key]['favorited_at'], '%Y-%m-%d %H:%M:%S')
                    l.interest_before = round(float(liked_loans[loan_key]['interest']),4)
                except: pass
                liked_loans_list += [l]
        except Exception as e:
            print('error while translating liked loans: either loan does not exist or: '+str(e))

    if sort:
        liked_loans_list.sort(key=lambda x: (x.loan_type.loan_type, x.principal.principal, x.tenure.tenure))

    return liked_loans_list

def render_cart(request):
    print("len(liked_loans)):")
    liked_loans = transfer_liked_loans_to_list(json.loads(request.GET['favorite_loans']))
    print("len(liked_loans)):")
    print(len(liked_loans))
    return render(
        request,
        'hr/cart_single_element.html', 
        {
            'favorite_loans':liked_loans,
            'no_favorite_loans':True if len(liked_loans)==0 else False
        }
    )