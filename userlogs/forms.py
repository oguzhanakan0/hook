from django import forms
from loans.models import Tenure, LoanType
from .models import *
from django.contrib.sessions.models import Session

# class CurrencyInputWidget(forms.widgets.TextInput):
#     template_name = 'loans/widgets/principal_input_widget.html'
#     class Media:
#         js = (
#             'https://code.jquery.com/jquery-3.4.1.slim.min.js',
#             'loans/js/number-seperator.js',
#         )

class LoanRequestForm(forms.ModelForm):
    class Meta:
        model = LoanRequest
        fields = ('loan_type','principal','tenure','request_source','session')
    
    def __init__(self, *args, **kwargs):
        super(LoanRequestForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(LoanRequestForm, self).save()


class SaveForm(forms.ModelForm):
    class Meta:
        model = Save
        fields = ('loan','principal','tenure','time','action','session')
    
    def __init__(self, *args, **kwargs):
        super(SaveForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(SaveForm, self).save()

class SubscriptionForm(forms.ModelForm):
    class Meta:
        model = Subscription
        fields = ('subscription_type','email','permission_check','marketing_check','session')
    
    def __init__(self, *args, **kwargs):
        super(SubscriptionForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(SubscriptionForm, self).save()

class AlertSubscriptionForm(forms.ModelForm):
    class Meta:
        model = AlertSubscription
        fields = ('subscription_type','email','permission_check','marketing_check','session',
            'GPL_check','CAR_check','CA2_check','MOR_check',
            'principal', 'tenure')
    
    def __init__(self, *args, **kwargs):
        super(AlertSubscriptionForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(AlertSubscriptionForm, self).save()

class LoanDetailRequestForm(forms.ModelForm):
    class Meta:
        model = LoanDetailRequest
        fields = ('tenure','principal','slug','session','request_source','bank','loan_type')
    
    def __init__(self, *args, **kwargs):
        super(LoanDetailRequestForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(LoanDetailRequestForm, self).save()

class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ('loan','principal','tenure','request_source','session')
    
    def __init__(self, *args, **kwargs):
        super(ApplicationForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(ApplicationForm, self).save()

# HESAPLAMA ARACLARI FORMLAR

class BasitKrediHesaplamaForm(forms.ModelForm):
    class Meta:
        model = BasitKrediHesaplama
        fields = ('loan_type','principal','tenure','interest','session')
    
    def __init__(self, *args, **kwargs):
        super(BasitKrediHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(BasitKrediHesaplamaForm, self).save()

class KrediVergisiHesaplamaForm(forms.ModelForm):
    class Meta:
        model = KrediVergisiHesaplama
        fields = ('loan_type','principal','tenure','interest','session')
    
    def __init__(self, *args, **kwargs):
        super(KrediVergisiHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(KrediVergisiHesaplamaForm, self).save()

class ErkenKapamaAraOdemeHesaplamaForm(forms.ModelForm):
    class Meta:
        model = ErkenKapamaAraOdemeHesaplama
        fields = ('loan_type','principal','tenure','interest','paid','session')
    
    def __init__(self, *args, **kwargs):
        super(ErkenKapamaAraOdemeHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(ErkenKapamaAraOdemeHesaplamaForm, self).save()

class KrediPesinatHesaplamaForm(forms.ModelForm):
    class Meta:
        model = KrediPesinatHesaplama
        fields = ('loan_type','value','tenure','session')
    
    def __init__(self, *args, **kwargs):
        super(KrediPesinatHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(KrediPesinatHesaplamaForm, self).save()

class KrediYapilandirmaHesaplamaForm(forms.ModelForm):
    class Meta:
        model = KrediYapilandirmaHesaplama
        fields = ('loan_type','principal','tenure','interest','paid','session')
    
    def __init__(self, *args, **kwargs):
        super(KrediYapilandirmaHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(KrediYapilandirmaHesaplamaForm, self).save()

class KKNakitAvansHesaplamaForm(forms.ModelForm):
    class Meta:
        model = KKNakitAvansHesaplama
        fields = ('principal','tenure','interest','session')
    
    def __init__(self, *args, **kwargs):
        super(KKNakitAvansHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(KKNakitAvansHesaplamaForm, self).save()

class GecikmeFaiziHesaplamaForm(forms.ModelForm):
    class Meta:
        model = GecikmeFaiziHesaplama
        fields = ('loan_type','principal','tenure','interest','paid','delinquent','session')
    
    def __init__(self, *args, **kwargs):
        super(GecikmeFaiziHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(GecikmeFaiziHesaplamaForm, self).save()

class ErkenOdemeCezasiHesaplamaForm(forms.ModelForm):
    class Meta:
        model = ErkenOdemeCezasiHesaplama
        fields = ('principal','tenure','interest','paid','session')
    
    def __init__(self, *args, **kwargs):
        super(ErkenOdemeCezasiHesaplamaForm, self).__init__(*args, **kwargs)
        
    def save(self):
        super(ErkenOdemeCezasiHesaplamaForm, self).save()
