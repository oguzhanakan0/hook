from django.db import models
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.contrib.sessions.models import Session
from loans.models import LoanType, Tenure, Loan, Bank
from django.contrib.sessions.models import Session
from django.db.models.signals import post_save, pre_delete, post_init
from django.contrib.sessions.backends.db import SessionStore
import uuid

"""
 ██╗      ██████╗  ██████╗ ██╗  ██╗██╗   ██╗██████╗     ████████╗ █████╗ ██████╗ ██╗     ███████╗███████╗
 ██║     ██╔═══██╗██╔═══██╗██║ ██╔╝██║   ██║██╔══██╗    ╚══██╔══╝██╔══██╗██╔══██╗██║     ██╔════╝██╔════╝
 ██║     ██║   ██║██║   ██║█████╔╝ ██║   ██║██████╔╝       ██║   ███████║██████╔╝██║     █████╗  ███████╗
 ██║     ██║   ██║██║   ██║██╔═██╗ ██║   ██║██╔═══╝        ██║   ██╔══██║██╔══██╗██║     ██╔══╝  ╚════██║
 ███████╗╚██████╔╝╚██████╔╝██║  ██╗╚██████╔╝██║            ██║   ██║  ██║██████╔╝███████╗███████╗███████║
 ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝            ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚══════╝╚══════╝╚══════╝
"""

class SubscriptionType(models.Model):
	subscription_type          = models.CharField(primary_key=True, max_length = 40, unique = True)
	description				   = models.CharField(max_length = 120)

	def __str__(self):
		return self.subscription_type

class SaveType(models.Model):
	save_type          = models.CharField(primary_key=True, max_length = 40, unique = True)

	def __str__(self):
		return self.save_type

class RequestSource(models.Model):
	request_source	           = models.CharField(primary_key=True, max_length = 40, unique = True)
	description				   = models.CharField(max_length = 120)

	def __str__(self):
		return self.request_source

"""
 
 ██████╗ ███████╗ ██████╗ ██╗   ██╗██╗      █████╗ ██████╗     ████████╗ █████╗ ██████╗ ██╗     ███████╗███████╗
 ██╔══██╗██╔════╝██╔════╝ ██║   ██║██║     ██╔══██╗██╔══██╗    ╚══██╔══╝██╔══██╗██╔══██╗██║     ██╔════╝██╔════╝
 ██████╔╝█████╗  ██║  ███╗██║   ██║██║     ███████║██████╔╝       ██║   ███████║██████╔╝██║     █████╗  ███████╗
 ██╔══██╗██╔══╝  ██║   ██║██║   ██║██║     ██╔══██║██╔══██╗       ██║   ██╔══██║██╔══██╗██║     ██╔══╝  ╚════██║
 ██║  ██║███████╗╚██████╔╝╚██████╔╝███████╗██║  ██║██║  ██║       ██║   ██║  ██║██████╔╝███████╗███████╗███████║
 ╚═╝  ╚═╝╚══════╝ ╚═════╝  ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝       ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚══════╝╚══════╝╚══════╝
                                                                                                                
 
"""

class Subscription(models.Model):
	id			   			   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session 				   = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	time 					   = models.DateTimeField(auto_now=True)
	subscription_type		   = models.ForeignKey(SubscriptionType,primary_key=False, on_delete=models.PROTECT)
	email					   = models.EmailField()
	marketing_check	           = models.BooleanField(default=False)
	permission_check           = models.BooleanField(default=False)

class AlertSubscription(models.Model):
	id			   			   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session 				   = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	time 					   = models.DateTimeField(auto_now=True)
	email					   = models.EmailField()
	subscription_type		   = models.ForeignKey(SubscriptionType,primary_key=False, on_delete=models.PROTECT) # all-alerts or custom-alerts
	GPL_check				   = models.BooleanField(default=False)
	CAR_check				   = models.BooleanField(default=False)
	CA2_check				   = models.BooleanField(default=False)
	MOR_check				   = models.BooleanField(default=False)
	marketing_check	           = models.BooleanField(default=False)
	principal	      		   = models.PositiveIntegerField(null=True,blank=True)
	tenure	    	  		   = models.PositiveIntegerField(null=True,blank=True)
	permission_check           = models.BooleanField(default=False)


class LoanRequest(models.Model):
	id     		   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	tenure         = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'tenure', to_field = 'tenure')
	principal      = models.PositiveIntegerField()
	time   		   = models.DateTimeField(auto_now_add=True)
	request_source = models.ForeignKey(RequestSource, on_delete=models.PROTECT)
	session 	   = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')

	@property
	def available_tenures(self):
		return self.loan_type.tenures.all()

class LoanDetailRequest(models.Model):
	id     		   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	slug           = models.SlugField(null=True,blank=True)
	tenure         = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'tenure', to_field = 'tenure', null=True, blank=True)
	principal      = models.PositiveIntegerField()
	# loan		   = models.ForeignObject(Loan, from_fields=['slug', 'tenure','principal'], to_fields=['slug', 'tenure','principal'], related_name='abc', on_delete=models.CASCADE)
	time   		   = models.DateTimeField(auto_now_add=True)
	request_source = models.ForeignKey(RequestSource, on_delete=models.PROTECT)
	session 	   = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan_type	   = models.ForeignKey(LoanType, on_delete=models.PROTECT, null=True, blank=True, db_column = 'loan_type', to_field = 'loan_type')
	bank           = models.ForeignKey(Bank, on_delete=models.PROTECT, null=True, blank=True, db_column = 'bank_slug', to_field = 'slug')

	@property
	def related_loan(self):
		if self.bankwise_request:
			return Loan.objects.filter(tenure=self.tenure,bank=self.bank,principal__gte=self.principal,loan_type=self.loan_type).order_by('principal','tenure','interest').first()
		else:
			return Loan.objects.filter(tenure=self.tenure,slug=self.slug,principal__gte=self.principal).order_by('principal').first()

class Save(models.Model):
	id     		   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session 	   = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan		   = models.CharField(max_length = 200, unique = True, db_column='loan_id')
	tenure         = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'tenure', to_field = 'tenure')
	principal      = models.PositiveIntegerField()
	time           = models.DateTimeField(auto_now=False, auto_now_add=False)
	action		   = models.ForeignKey(SaveType, on_delete=models.PROTECT)

class Application(models.Model):
	id     		   = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session 	   = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan		   = models.CharField(max_length = 200, db_column='loan_id')
	tenure         = models.ForeignKey(Tenure, on_delete=models.PROTECT, db_column = 'tenure', to_field = 'tenure')
	principal      = models.PositiveIntegerField()
	time           = models.DateTimeField(auto_now=True)
	request_source = models.ForeignKey(RequestSource, on_delete=models.PROTECT)
	success        = models.BooleanField(default=False)

# HESAPLAMA ARACLARI FORMLARI

class BasitKrediHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	principal      = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField()
	interest       = models.FloatField()
	time           = models.DateTimeField(auto_now=True)

class KrediVergisiHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	principal      = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField()
	interest       = models.FloatField()
	time           = models.DateTimeField(auto_now=True)

class ErkenKapamaAraOdemeHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	principal      = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField()
	interest       = models.FloatField()
	paid           = models.PositiveIntegerField()
	time           = models.DateTimeField(auto_now=True)

class KrediPesinatHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	value          = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField(null=True,blank=True)
	time           = models.DateTimeField(auto_now=True)

class KrediYapilandirmaHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	principal      = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField()
	interest       = models.FloatField()
	paid           = models.PositiveIntegerField()
	time           = models.DateTimeField(auto_now=True)

class KKNakitAvansHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	principal      = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField()
	interest       = models.FloatField()
	time           = models.DateTimeField(auto_now=True)

class GecikmeFaiziHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	loan_type      = models.ForeignKey(LoanType, on_delete=models.PROTECT, db_column = 'loan_type', to_field = 'loan_type')
	principal      = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField()
	interest       = models.FloatField()
	paid           = models.PositiveIntegerField()
	delinquent     = models.PositiveIntegerField()
	time           = models.DateTimeField(auto_now=True)

class ErkenOdemeCezasiHesaplama(models.Model):
	id             = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	session        = models.ForeignKey(Session, on_delete=models.PROTECT, db_column = 'session_key', to_field = 'session_key')
	principal      = models.PositiveIntegerField()
	tenure         = models.PositiveIntegerField()
	interest       = models.FloatField()
	paid           = models.PositiveIntegerField()
	time           = models.DateTimeField(auto_now=True)


## SESSION RELATED

class SessionInfo(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    session = models.OneToOneField(Session, on_delete=models.PROTECT)

def session_create_listener(instance, **kwargs):
    store = SessionStore(session_key=instance.session_key)

    try:
        instance.sessioninfo
    except SessionInfo.DoesNotExist:
        sessioninfo = SessionInfo(session=instance) 
        sessioninfo.save()

post_save.connect(session_create_listener, sender=Session)