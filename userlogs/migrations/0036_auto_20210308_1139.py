# Generated by Django 3.1.6 on 2021-03-08 08:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userlogs', '0035_remove_erkenkapamaaraodemehesaplama_ekaoh_type'),
    ]

    operations = [
        migrations.RenameField(
            model_name='erkenkapamaaraodemehesaplama',
            old_name='installment',
            new_name='interest',
        ),
    ]
