# Generated by Django 3.1.6 on 2021-03-10 07:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userlogs', '0036_auto_20210308_1139'),
    ]

    operations = [
        migrations.RenameField(
            model_name='gecikmefaizihesaplama',
            old_name='installment',
            new_name='interest',
        ),
    ]
