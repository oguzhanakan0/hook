# Generated by Django 3.1.6 on 2021-03-07 05:47

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0124_auto_20210306_2333'),
        ('sessions', '0001_initial'),
        ('userlogs', '0031_basitkredihesaplama_erkenkapamaaraodemehesaplama_erkenodemecezasihesaplama_gecikmefaizihesaplama_kkn'),
    ]

    operations = [
        migrations.CreateModel(
            name='KrediPesinatHesaplama',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid1, editable=False, primary_key=True, serialize=False)),
                ('value', models.PositiveIntegerField()),
                ('time', models.DateTimeField(auto_now=True)),
                ('loan_type', models.ForeignKey(db_column='loan_type', on_delete=django.db.models.deletion.PROTECT, to='loans.loantype', to_field='loan_type')),
                ('session', models.ForeignKey(db_column='session_key', on_delete=django.db.models.deletion.PROTECT, to='sessions.session')),
            ],
        ),
        migrations.DeleteModel(
            name='KonutKredisiPesinatHesaplama',
        ),
    ]
