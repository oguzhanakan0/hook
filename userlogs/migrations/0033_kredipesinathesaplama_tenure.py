# Generated by Django 3.1.6 on 2021-03-07 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userlogs', '0032_auto_20210307_0847'),
    ]

    operations = [
        migrations.AddField(
            model_name='kredipesinathesaplama',
            name='tenure',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
    ]
