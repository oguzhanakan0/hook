# Register your models here.
from django.contrib import admin
from .models import *

# Basic Models
admin.site.register(SubscriptionType)
admin.site.register(SaveType)
admin.site.register(RequestSource)