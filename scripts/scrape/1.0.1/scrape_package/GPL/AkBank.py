from .. import scrape_tools
from ..scrape_tools import BaseScrape
from ..scrape_tools import CampaignControl

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time


class AkBank(BaseScrape):

    def _control_product_locator(self):
        if scrape_tools._element_found(self.driver, ('xpath','//div[@class="header multiple-module-head"]//div[@class="custom-select-item-text"]')):
            self.alert_log.append(5)

    def _control_campaign_locator(self):
        if scrape_tools._element_found(self.driver, ('xpath','//div[@class="header multiple-module-head"]//div[@class="custom-select-item-text"]')):
            self.alert_log.append(5)

    def _create_available_campaigns(self):
        try:
            camp_select = Select(self.driver.find_element_by_xpath('//select[@class="select active"]'))
        except:
            self.driver.find_element_by_xpath(
                '//div[@class="header multiple-module-head"]//div[@class="custom-select-item-text"]').click()
            camp_select = Select(self.driver.find_element_by_xpath('//select[@class="select active"]'))
        camp_list = [i.text for i in camp_select.options]
        other_products = {'Konut Kredisi', 'Taşıt Kredisi'}
        self.available_campaigns = list(set(camp_list).difference(other_products))

    def _control_campaign_process(self):
        CampContObj = CampaignControl(self.defined_campaigns, self.available_campaigns)
        CampContObj.campaign_control()
        self.final_campaigns = CampContObj.final_campaigns
        self.alert_log = self.alert_log + CampContObj.campaign_alert_log

    def _control_principal_tenure_locator(self):
        principal_elements = self.driver.find_elements_by_xpath('//div[@class="textbox"]/input')
        tenure_slider_elements = self.driver.find_elements_by_xpath('//a[@class="ui-slider-handle ui-state-default ui-corner-all"]')
        tenure_inc_elements = self.driver.find_elements_by_xpath('//a[@class="step-btn increase"]')
#        tenure_dec_elements = self.driver.find_elements_by_xpath('//a[@class="step-btn decrease"]')
        for locator in [principal_elements, tenure_inc_elements, tenure_slider_elements]:
            if not locator:
                self.alert_log.append(10)
            elif sum([i.is_displayed() for i in locator]) == 0:
                self.alert_log.append(10)

    def _control_interest_locator(self):
        interest_elements = self.driver.find_elements_by_class_name('interest-rate')
        if not interest_elements:
            self.alert_log.append(11)
        elif sum([i.is_displayed() for i in interest_elements]) == 0:
            self.alert_log.append(11)

    def interest_control_step(self):
        for met in ['_control_principal_tenure_locator', '_control_interest_locator']:
            if not list(filter(lambda x: x not in [7, 8], self.alert_log)):
                print('Running ', met)
                self.__getattribute__(met)()

    def _set_campaign(self, campaign):
        try:
            camp_select = Select(self.driver.find_element_by_xpath('//select[@class="select active"]'))
        except:
            self.driver.find_element_by_xpath(
                '//div[@class="header multiple-module-head"]//div[@class="custom-select-item-text"]').click()
            camp_select = Select(self.driver.find_element_by_xpath('//select[@class="select active"]'))
        camp_select.select_by_visible_text(campaign)
        time.sleep(1)


    def _set_principal(self, principal):
        principal_elements = self.driver.find_elements_by_xpath('//div[@class="textbox"]/input')
        principal_element = list(filter(lambda i: i.is_displayed(), principal_elements))[0]
        principal_elements.clear()
        for i in range(7):
            principal_element.send_keys(Keys.BACK_SPACE)
        time.sleep(1)
        principal_element.send_keys(principal)
        time.sleep(1)

    def _set_tenure(self, tenure):
        tenure_inc_elements = self.driver.find_elements_by_xpath('//a[@class="step-btn increase"]')
        tenure_inc_element = list(filter(lambda i: i.is_displayed(), tenure_inc_elements))[0]
        tenure_slider_elements = self.driver.find_elements_by_xpath(
            '//a[@class="ui-slider-handle ui-state-default ui-corner-all"]')
        tenure_slider_element = list(filter(lambda i: i.is_displayed(), tenure_slider_elements))[0]
#        tenure_dec_elements = self.driver.find_elements_by_xpath('//a[@class="step-btn decrease"]')
#        tenure_dec_element = list(filter(lambda i: i.is_displayed(), tenure_dec_elements))[0]

        move = ActionChains(self.driver)
        move.click_and_hold(tenure_slider_element).move_by_offset(-200, 0).release().perform()
        time.sleep(1)
        for i in range(tenure - 1):
            tenure_inc_element.click()
        time.sleep(1)

    def _read_interest(self):
        interest_elements = self.driver.find_elements_by_class_name('interest-rate')
        interest_element = list(filter(lambda i: i.is_displayed(), interest_elements))[0]
        interest = interest_element.text
        interest = interest.replace('%', '').replace(',', '.').strip()
        return float(interest)



def run_scrape(driver, principal_list, tenure_list, campaign_list=None):
    print('Running AkBank')
    ab = AkBank(
        driver,
        'https://www.akbank.com/kredi-hesaplama',
        'Kredi Hesapla | Akbank Kredi Hesaplama',
        ''' 'Aradığınız sayfa maalesef bulunamadı.' in [i.text for i in FFdriver.find_elements_by_tag_name('p')] ''',
        ('xpath','//div[@class="header multiple-module-head"]//div[@class="custom-select-item-text"]'),
        campaign_list
    )
    ab.page_product_step()
    ab.campaign_control_step()
    ab.interest_control_step()
    ab.control_scrape_initiation()
    ab.scrape(principal_list, tenure_list)
    return ab.alert_log, ab.scrape_status, ab.result
