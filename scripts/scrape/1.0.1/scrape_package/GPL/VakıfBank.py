from .. import scrape_tools
from ..scrape_tools import BaseScrape
from ..scrape_tools import CampaignControl

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time

class VakıfBank(BaseScrape):

    def _control_product_locator(self):
        if scrape_tools._element_found(self.driver, ('id','ctl00_ctl10_ctl00_ddlKredi')):
            self.alert_log.append(5)

    def _control_campaign_locator(self):
        if scrape_tools._element_found(self.driver, ('id','ctl00_ctl10_ctl00_ddlKredi')):
            self.alert_log.append(6)

    def _create_available_campaigns(self):
        camp_select = Select(self.driver.find_element_by_id('ctl00_ctl10_ctl00_ddlKredi'))
        campaigns = [i.text for i in camp_select.options]
        self.available_campaigns = list(filter(lambda x: x not in ['Seçiniz','SARIPANJUR KONUT KREDİSİ KAMPANYASI', 'TAKSİTLİ EK HESAP'], campaigns))

    def _control_campaign_process(self):
        CampContObj = CampaignControl(self.defined_campaigns, self.available_campaigns)
        CampContObj.campaign_control()
        self.final_campaigns = CampContObj.final_campaigns
        self.alert_log = self.alert_log + CampContObj.campaign_alert_log

    def _control_interest_locator(self, interest_locator):
        pass

    def _control_submit_locator(self, submit_locator):
        scrape_tools._element_found(self.driver, submit_locator)

    def _set_campaign(self, campaign):
        if not self.driver.find_elements_by_xpath('//ul[@id="select2-ctl00_ctl10_ctl00_ddlKredi-results"]/li'):
            self.driver.find_element_by_id('select2-ctl00_ctl10_ctl00_ddlKredi-container').click()

        campaigns = self.driver.find_elements_by_xpath('//ul[@id="select2-ctl00_ctl10_ctl00_ddlKredi-results"]/li')
        campaigns_text = [i.text for i in campaigns]
        campaigns[campaigns_text.index(campaign)].click()

    def _set_principal(self, principal):
        principal_element = self.driver.find_element_by_xpath('//div[@class="slider tutarSlider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"]/span')
        principal_element.click()
        principal_element.send_keys(Keys.ARROW_RIGHT)

        initial_pos = self.driver.find_element_by_id('txtTutarVisible').get_attribute('value')
        initial_pos = int(initial_pos.replace('.',''))
        steps = int(abs((principal - initial_pos) / 500))

        if principal > initial_pos:
            key = Keys.ARROW_RIGHT
        elif principal < initial_pos:
            key = Keys.ARROW_LEFT

        if principal != initial_pos:
            for i in range(steps):
                principal_element.send_keys(key)

        principal_element.send_keys(Keys.RETURN)
        time.sleep(1)

    def _set_tenure(self, tenure):
        tenure_element = self.driver.find_element_by_xpath('//div[@class="slider vadeSlider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"]/span')
        tenure_element.click()
        tenure_element.send_keys(Keys.ARROW_RIGHT)

        initial_pos = self.driver.find_element_by_id('ctl00_ctl10_ctl00_txtVade').get_attribute('value')
        initial_pos = int(initial_pos)
        steps = int(abs((tenure - initial_pos) / 1))

        if tenure > initial_pos:
            key = Keys.ARROW_RIGHT
        elif tenure < initial_pos:
            key = Keys.ARROW_LEFT

        if tenure != initial_pos:
            for i in range(steps):
                tenure_element.send_keys(key)

        tenure_element.send_keys(Keys.RETURN)
        time.sleep(1)

    def _read_interest(self):
        self.driver.find_element_by_id('ctl00_ctl10_ctl00_btnHesapla').click()
        time.sleep(1)
        text_pos = self.driver.find_element_by_xpath('//td/span[contains(text(),"Faiz Oranı")]')
        table = [i.text for i in text_pos.parent.find_elements_by_tag_name('td')]
        interest = table[table.index('Faiz Oranı')+1]
        interest = interest.replace('%', '').replace(',', '.').strip()
        return float(interest)


    def scrape(self, principal_list, tenure_list):
        print('Running scrape')
        if self.scrape_status == 0:
            try:
                i = 0
                for campaign in self.final_campaigns:
                    self._set_campaign(campaign)
                    for principal in principal_list:
                        for tenure in tenure_list:
                            self._set_principal(principal)
                            self._set_tenure(tenure)
                            interest = self._read_interest()
                            self.result.loc[i] = [campaign, principal, tenure, interest]
                            i += 1
                self.scrape_status = 1
            except:
                self.scrape_status = 2
        else:
            self.scrape_status = 0

def run_scrape(driver, principal_list, tenure_list, campaign_list=None):
    print('Running YapiKredi')
    vb = VakıfBank(
        driver,
        'https://www.vakifbank.com.tr/kredi-hesaplama-araci.aspx?pageID=1333',
        'Kredi Hesaplama Aracı',
        ''' 'Görüntülemek istediğiniz sayfayı\nbulmanıza yardımcı olabiliriz.' in [i.text for i in FFdriver.find_elements_by_tag_name('p')]''',
        ('id','ctl00_ctl10_ctl00_ddlKredi'),
        campaign_list
    )
    vb.page_product_step()
    vb.campaign_control_step()
    vb.interest_control_step(
        ('xpath', '//div[@class="slider tutarSlider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"]/span'),
        ('xpath', '//div[@class="slider vadeSlider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"]/span'),
        ('xpath','//td/span[contains(text(),"Faiz Oranı")]'),
        ('id','ctl00_ctl10_ctl00_btnHesapla')
    )
    vb.control_scrape_initiation()
    vb.scrape(principal_list, tenure_list)
    return vb.alert_log, vb.scrape_status, vb.result

