from .. import scrape_tools
from ..scrape_tools import BaseScrape
from ..scrape_tools import CampaignControl

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time


class ING(BaseScrape):

    def _control_product_locator(self):
        if scrape_tools._element_found(self.driver, ('xpath','//h3[contains(text(), "İhtiyaç Kredisi Hesaplama Aracı")]')):
            self.alert_log.append(5)

    def _set_product(self):
        try:
            WebDriverWait(self.driver, 1).until(EC.visibility_of_element_located(('xpath','//div[contains(@id, "sbHolder_mytermList")]')))
        except:
            self.driver.find_element_by_xpath('//h3[contains(text(), "İhtiyaç Kredisi Hesaplama Aracı")]').click()
            try:
                WebDriverWait(self.driver, 1).until(EC.visibility_of_element_located(('xpath','//div[contains(@id, "sbHolder_mytermList")]')))
            except:
                self.alert_log.append(5)

    def _set_principal(self, principal):
        principal_element = self.driver.find_elements_by_xpath('//span[@class="ui-slider-handle ui-corner-all ui-state-default"]')[0]
        principal_element.click()

        initial_pos = self.driver.find_elements_by_xpath('//div[@class="tip-value"]')[0].text
        initial_pos = int(initial_pos.replace('.',''))
        steps = int(abs((principal - initial_pos)/500))

        if principal > initial_pos:
            key = Keys.ARROW_RIGHT
        elif principal < initial_pos:
            key = Keys.ARROW_LEFT

        if principal != initial_pos:
            for i in range(steps):
                principal_element.send_keys(key)

        self.driver.find_elements_by_xpath('//ul[@class="summary-list cf"]/li')[0].click()
        time.sleep(1)

    def _set_tenure(self, tenure):
        self.driver.find_element_by_xpath('//a[contains(@id, "sbToggle_mytermList")]').click()
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(('xpath','//ul[@class="sbOptions"]/li/a[@rel={}]'.format(tenure))))
        self.driver.find_element_by_xpath('//ul[@class="sbOptions"]/li/a[@rel={}]'.format(tenure)).click()
        time.sleep(1)


    def _read_interest(self):
        interest = self.driver.find_element_by_xpath('//span[@data-output="rate"]').text
        interest = interest.replace(',', '.').strip()
        return float(interest)



def run_scrape(driver, principal_list, tenure_list, campaign_list=None):
    print('Running ING')
    ing = ING(
        driver,
        'https://www.ing.com.tr/tr/kredi-hesaplama',
        'Kredi Hesaplama | Hesaplama Araçları | ING',
        ''' 'Sayfa Bulunamadı' in [i.text for i in self.driver.find_elements_by_tag_name('h4')] ''',
        ('xpath','//h3[contains(text(), "İhtiyaç Kredisi Hesaplama Aracı")]'),
    )
    ing.page_product_step()
    ing.campaign_control_step()
    ing.interest_control_step(
        ('xpath', '//span[@class="ui-slider-handle ui-corner-all ui-state-default"]'),
        ('xpath', '//a[contains(@id, "sbToggle_mytermList")]'),
        ('xpath','//span[@data-output="rate"]'),
    )
    ing.control_scrape_initiation()
    ing.scrape(principal_list, tenure_list)
    return ing.alert_log, ing.scrape_status, ing.result