from .. import scrape_tools
from ..scrape_tools import BaseScrape
from ..scrape_tools import CampaignControl

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time

class HalkBank(BaseScrape):

    def _control_product_locator(self):
        if scrape_tools._element_found(self.driver, ('id','ihtiyacT')):
            self.alert_log.append(5)

    def _set_product(self):
        self.driver.find_element_by_id('ihtiyacT').click()

    def _control_campaign_locator(self):
        if scrape_tools._element_found(self.driver, ('id','ddlCreditCategories')):
            self.alert_log.append(6)

    def _create_available_campaigns(self):
        camp_select = Select(self.driver.find_element_by_id('ddlCreditCategories'))
        self.available_campaigns = [i.text for i in camp_select.options]

    def _control_campaign_process(self):
        CampContObj = CampaignControl(self.defined_campaigns, self.available_campaigns)
        CampContObj.campaign_control()
        self.final_campaigns = CampContObj.final_campaigns
        self.alert_log = self.alert_log + CampContObj.campaign_alert_log

    def _control_submit_locator(self, submit_locator):
        scrape_tools._element_found(self.driver, submit_locator)

    def _set_campaign(self, campaign):
        camp_select = Select(self.driver.find_element_by_id('ddlCreditCategories'))
        camp_select.select_by_visible_text(campaign)
        time.sleep(1)

    def _set_principal(self, principal):
        principal_element = self.driver.find_element_by_id('Amount_MonthAmount')
        principal_element.clear()
        time.sleep(1)
        principal_element.send_keys(principal)

    def _set_tenure(self, tenure):
        tenure_select = Select(self.driver.find_element_by_id('ddlCreditTerm'))
        tenure_select.select_by_value(str(tenure))

    def _read_interest(self):
        self.driver.find_element_by_id('yenidenAc').click()
        time.sleep(1)
        interest =  self.driver.find_element_by_id('lblFaizOraniText').text
        interest = interest.replace(',','.').strip()
        return float(interest)

def run_scrape(driver, principal_list, tenure_list, campaign_list=None):
    print('Running HalkBank')
    hb = HalkBank(
        driver,
        'https://www.halkbank.com.tr/4517-kredi_hesaplama',
        'TÜRKİYE HALK BANKASI',
        ''' 'Ulaşmak istediğiniz sayfa bulunamadı.' in [i.text for i in FFdriver.find_elements_by_tag_name('h1')]''',
        ('id','md-kredi'),
        campaign_list
    )
    hb.page_product_step()
    hb.campaign_control_step()
    hb.interest_control_step(
        ('id', 'Amount_MonthAmount'),
        ('id', 'ddlCreditTerm'),
        ('id','lblFaizOraniText'),
        ('id','yenidenAc')
    )
    hb.control_scrape_initiation()
    hb.scrape(principal_list, tenure_list)
    return hb.alert_log, hb.scrape_status, hb.result


