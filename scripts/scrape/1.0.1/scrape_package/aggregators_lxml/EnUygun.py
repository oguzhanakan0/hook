from .util_tools import request_headers, check_campaign_list, campaigns_to_remove, fill_campaign_list, convert_bank_names

from lxml import html
import requests
import pandas as pd


class Enygun:
    def __init__(self):
        self.source_name='EnUygun'
        self.alert_log = []
        self.result = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])
        self.scrape_status = 0
        self.bank_dict = {
            'Akbank':4,
            'Alternatif Bank':12,
            'DenizBank':5,
            'Enparacom':26,
            'Fibabanka':16,
            'Garanti BBVA':2,
            'HSBC':18,
            'Halkbank':8,
            'ICBC Turkey':19,
            'ING Bank':1,
            'QNB Finansbank':6,
            'TEB':10,
            'Türkiye İş Bankası':9,
            'Vakıfbank':7,
            'Yapı Kredi':3,
            'Ziraat Bankası':11
        }

    
    def _goto_page(self, url, timeout=5):
        try:
            page = requests.get(url, headers=request_headers, timeout=timeout)
            self.tree = html.fromstring(page.content)
        except requests.exceptions.Timeout:
            self.alert_log.append(14)
        except:
            self.alert_log.append(1)

    def _get_banks(self):
        bank_elements = self.tree.xpath('//table[@class="table table-striped hidden-xs credit-compare-table"]//tr')
        bank_elements = list(filter(lambda x: x.get('data-bank-id') != None, bank_elements))
        return [x.xpath('.//img')[0].get('alt') for x in bank_elements]

    def _get_campaign_names(self):
        bank_elements = self.tree.xpath('//table[@class="table table-striped hidden-xs credit-compare-table"]//tr')
        bank_elements = list(filter(lambda x: x.get('data-bank-id') != None, bank_elements))
        return [x.xpath('.//td')[2].xpath('.//div')[1].text for x in bank_elements]

    def _get_rates(self):
        bank_elements = self.tree.xpath('//table[@class="table table-striped hidden-xs credit-compare-table"]//tr')
        bank_elements = list(filter(lambda x: x.get('data-bank-id') != None, bank_elements))
        interest_list = [x.xpath('.//td')[3].xpath('.//div')[0].text[5:].strip() for x in bank_elements]
        return [float(x.replace('%','').replace(' ','').replace(',','.').replace('*','')) for x in interest_list]

    def _scrape(self, principal_list, tenure_list, product_name, timeout=5):
        for principal in principal_list:
            for tenure in tenure_list:
                url = self.url_pattern.format(str(tenure), str(principal))
                self._goto_page(url, timeout)

                if not self.alert_log:
                    bank_list = self._get_banks()
                    nof_banks = len(bank_list)
                    campaign_list = self._get_campaign_names()
                    bank_list_adj = convert_bank_names(self.bank_dict, bank_list)
                    campaign_list = fill_campaign_list(bank_list_adj, product_name, campaign_list)
                    rate_list = self._get_rates()
                    try:
                        temp_df = pd.DataFrame({
                            'bank':bank_list, 
                            'campaign':campaign_list,
                            'principal':[principal]*nof_banks, 
                            'tenure':[tenure]*nof_banks, 
                            'interest':rate_list
                        })
                        
                    except:
                        self.alert_log.append(13)
                        temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])    
                else:
                    temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])

                self.result = self.result.append(temp_df)
        
        if not self.alert_log:
            self.result.reset_index(inplace=True, drop=True)
            self.result.bank = self.result.bank.replace(self.bank_dict)
            self.result = self.result[self.result.bank.isin(self.bank_dict.values())]
            self.result.sort_values('bank', inplace=True)
            self.result.reset_index(inplace=True, drop=True) 
            self.scrape_status = 1
        else:
            pass



class EnUygunGPL(Enygun):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.enuygun.com/kredi/ihtiyac/karsilastir/{}-ay-vade-ile-{}-tl/'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='İhtiyaç', timeout=timeout)  

class EnUygunMOR(Enygun):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.enuygun.com/kredi/konut/karsilastir/{}-ay-vade-ile-{}-tl/'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='Konut', timeout=timeout)  

class EnUygunCAR(Enygun):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.enuygun.com/kredi/tasit/karsilastir/{}-ay-vade-ile-{}-tl/'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='Taşıt', timeout=timeout)  

class EnUygunCA2(Enygun):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.enuygun.com/kredi/tasit/karsilastir/{}-ay-vade-ile-{}-tl-ikinci-el/'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='2. El Taşıt', timeout=timeout) 