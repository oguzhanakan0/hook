request_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', 
    "Upgrade-Insecure-Requests": "1",
    "DNT": "1",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate"
}

campaigns_to_remove = ['hangi','hangi kredi','hangikredi','hesap','hesapkurdu','hesap kurdu','en uygun','enuygun']

def check_campaign(campaign, to_remove_list):
    i = 0
    for to_remove in to_remove_list:
        if campaign is None:
            pass
        elif to_remove in campaign.lower():
            i+=1
        else:
            None
    return 1 if i>0 else 0


def check_campaign_list(campaign_list, to_remove_list):
    campaign_updated = []
    for campaign in campaign_list:
        if check_campaign(campaign, to_remove_list) == 1:
            campaign_updated.append(None)
        else:
            campaign_updated.append(campaign)
    return campaign_updated

def convert_bank_names(bank_dict, bank_list):
    for i in range(len(bank_list)):
        if bank_list[i] in bank_dict.keys():
            bank_list[i] = bank_dict[bank_list[i]]
        else:
            pass
    return bank_list

def fill_campaign_list(bank_list, product_name, campaign_list=None):
    katilim_banks = [20,21,22,23,24]
    if campaign_list:
        campaigns_updated = []
        controls = list(zip(bank_list, campaign_list))
        for control in controls:
            if control[1]:
                campaigns_updated.append(control[1])
            else:
                campaign_upd = product_name + ' ' + 'Finansmanı' if control[0] in katilim_banks else product_name + ' ' + 'Kredisi'
                campaigns_updated.append(campaign_upd)
    else:
        campaigns_updated = [product_name + ' ' + 'Finansmanı' if bank in katilim_banks else product_name + ' ' + 'Kredisi' for bank in bank_list]
    return campaigns_updated