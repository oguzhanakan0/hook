from .util_tools import request_headers, check_campaign_list, campaigns_to_remove, fill_campaign_list, convert_bank_names

from lxml import html
import requests
import pandas as pd

class HangiKredi:
    def __init__(self):
        self.source_name = 'HangiKredi'
        self.alert_log = []
        self.result = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])
        self.scrape_status = 0
        self.bank_dict = {
            'Akbank':4,
            'Albaraka Türk:':21,
            'Alternatif Bank':12,
            'Anadolubank':13,
            'Burgan Bank':17,
            'DenizBank':5,
            'Enpara.com':26,
            'Fibabanka':16,
            'Garanti BBVA':2,
            'HSBC':18,
            'Halkbank':8,
            'ICBC Bank Turkey':19,
            'ING':1,
            'Kuveyt Türk':22,
            'Odeabank':15,
            'QNB Finansbank':6,
            'TEB - Türk Ekonomi Bankası':10,
            'Türkiye Finans':20,
            'Vakıf Katılım':23,
            'VakıfBank':7,
            'Yapı Kredi':3,
            'Ziraat Bankası':11,
            'İş Bankası':9,
            'Şekerbank':14
        }

    def _goto_page(self, url, timeout=5):
        try:
            page = requests.get(url, headers=request_headers, timeout=timeout)
            self.tree = html.fromstring(page.content)
        except requests.exceptions.Timeout:
            self.alert_log.append(14)
        except:
            self.alert_log.append(1)

    def _get_banks(self):
        pass

    def _get_campaign_names(self):
        pass

    def _get_rates(self):
        pass

    def _scrape(self, principal_list, tenure_list, product_name, timeout=5):
        for principal in principal_list:
            for tenure in tenure_list:
                url = self.url_pattern.format(str(principal), str(tenure))
                self._goto_page(url, timeout)

                if not self.alert_log:
                    bank_list = self._get_banks()
                    nof_banks = len(bank_list)
                    campaign_list = self._get_campaign_names()
                    bank_list_adj = convert_bank_names(self.bank_dict, bank_list)
                    if len(campaign_list) == nof_banks:
                        campaign_list = fill_campaign_list(bank_list_adj, product_name, campaign_list)
                    else:
                        campaign_list = fill_campaign_list(bank_list_adj, product_name)
                    rate_list = self._get_rates()
                    try:
                        temp_df = pd.DataFrame({
                            'bank':bank_list, 
                            'campaign':campaign_list,
                            'principal':[principal]*nof_banks, 
                            'tenure':[tenure]*nof_banks, 
                            'interest':rate_list
                        })
                    except:
                        self.alert_log.append(13)
                        temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])    
                else:
                    temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])

                self.result = self.result.append(temp_df)
        
        if not self.alert_log:
            self.result.reset_index(inplace=True, drop=True)
            self.result.bank = self.result.bank.replace(self.bank_dict)
            self.result = self.result[self.result.bank.isin(self.bank_dict.values())]
            self.result.sort_values('bank', inplace=True)
            self.result.reset_index(inplace=True, drop=True)
            self.scrape_status = 1
        else:
            pass  


class HangiKrediGPL(HangiKredi):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hangikredi.com/kredi/ihtiyac-kredisi/sorgulama?Tutar={}&Vade={}'

    def _get_banks(self):
        bank_elements = self.tree.xpath('//div[@class="list-product-item__column list-product-item__first-column"]/div/a')
        bank_list = [x.get('title') for x in bank_elements]
        return [x[:-15].strip() for x in bank_list]

    def _get_campaign_names(self):
        campaign_elements = self.tree.xpath('//div[@class="list-product-item__column list-product-item__first-column"]/div/p')
        campaigns = [x.text for x in campaign_elements]
        return check_campaign_list(campaigns, campaigns_to_remove)

    def _get_rates(self):
        interest_elements = self.tree.xpath('//div[@class="list-product-item__column list-product-item__second-column"]/div/p')
        interest_list = [x.text for x in interest_elements]
        interest_list = list(filter(lambda x: x != '', interest_list))
        interest_list =  [x.replace('%','').replace(' ','').replace(',','.') for x in interest_list]
        return [float(x) for x in list(filter(lambda x: x != '', interest_list))]

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='İhtiyaç', timeout=timeout)  


class HangiKrediMOR(HangiKredi):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hangikredi.com/kredi/konut-kredisi/sorgulama?Tutar={}&Vade={}'

    def _get_banks(self):
        bank_elements = self.tree.xpath('//div[@class="housingResults"]//div[@class="housingBank"]//a')
        return [x.get('title') for x in bank_elements]

    def _get_campaign_names(self):
        campaign_elements = self.tree.xpath('//div[@class="housingResults"]//div[@class="housingBank"]//p')
        campaigns = [x.text for x in campaign_elements]
        return check_campaign_list(campaigns, campaigns_to_remove)

    def _get_rates(self):
        interest_elements = self.tree.xpath('//div[@class="housingResults"]//div[@class="housingFaiz short"]//strong')
        interest_list = [x.text for x in interest_elements]
        interest_list = list(filter(lambda x: x != '', interest_list))
        interest_list =  [x.replace('%','').replace(' ','').replace(',','.') for x in interest_list]
        return [float(x) for x in list(filter(lambda x: x != '', interest_list))]

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='Konut', timeout=timeout)  


class HangiKrediCar(HangiKredi):
    def __init__(self):
        super().__init__()

    def _get_banks(self):
        bank_elements = self.tree.xpath('//table[contains(@id,"bank-product-list")]//tr/td/a')
        bank_list = [x.get('title') for x in bank_elements]
        return [x[:-13].strip() for x in bank_list]

    def _get_campaign_names(self):
        campaign_elements = self.tree.xpath('//table[contains(@id,"bank-product-list")]//tr/td//div[@class="textUrunAdi"]')
        campaign_list = [x.text for x in campaign_elements]
        campaign_list = list(filter(lambda x: x != '', campaign_list))
        campaign_list = check_campaign_list(campaign_list, campaigns_to_remove)
        return campaign_list

    def _get_rates(self):
        interest_elements = self.tree.xpath('//table[contains(@id,"bank-product-list")]//tr/td[contains(@class,"tdFaiz")]//div/span')
        interest_list = [x.text for x in interest_elements]
        interest_list = list(filter(lambda x: x != '', interest_list))
        interest_list =  [x.replace('%','').replace(' ','').replace(',','.') for x in interest_list]
        return [float(x) for x in list(filter(lambda x: x != '', interest_list))]


class HangiKrediCAR(HangiKrediCar):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hangikredi.com/kredi/tasit-kredisi/sorgulama?Tutar={}&Vade={}&VehicleStatus=0&CarBrand=&CarModelYear=&CarModel='

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='0 km Taşıt', timeout=timeout) 

class HangiKrediCA2(HangiKrediCar):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hangikredi.com/kredi/tasit-kredisi/sorgulama?Tutar={}&Vade={}&VehicleStatus=1&CarBrand=&CarModelYear=&CarModel='

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='2. El Taşıt', timeout=timeout) 