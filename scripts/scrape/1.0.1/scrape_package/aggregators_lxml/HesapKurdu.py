from .util_tools import request_headers, check_campaign_list, campaigns_to_remove, fill_campaign_list, convert_bank_names

from lxml import html
import requests
import pandas as pd

class HesapKurdu:
    def __init__(self):
        self.source_name = 'HesapKurdu'
        self.alert_log = []
        self.result = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])
        self.scrape_status = 0
        self.bank_dict = {
            'Akbank':4,
            'Burgan Bank':17,
            'DenizBank':5,
            'Fibabanka':16,
            'Garanti BBVA':2,
            'Halkbank':8,
            'ICBC Turkey':19,
            'ING':1,
            'Odeabank':15,
            'QNB Finansbank':6,
            'TEB':10,
            'TurkishBank':25,
            'Türkiye Finans Katılım Bankası':20,
            'Vakıf Katılım':23,
            'Vakıfbank':7,
            'Yapı Kredi':3,
            'Ziraat Bankası':11,
            'Ziraat Katılım':24,
            'İş Bankası':9,
            'Şekerbank':14,
            'HSBC':18
        }

    def _goto_page(self, url, timeout=5):
        try:
            page = requests.get(url, headers=request_headers, timeout=timeout)
            self.tree = html.fromstring(page.content)
        except requests.exceptions.Timeout:
            self.alert_log.append(14)
        except:
            self.alert_log.append(1)

    def _get_banks(self):
        bank_elements = self.tree.xpath('//div[contains(@class,"product-card")]//div[@class="product-card__content"]//img')
        bank_elements = list(filter(lambda x: x.get('title') != None, bank_elements))
        return [x.get('title') for x in bank_elements]

    def _get_rates(self):
        interest_elements = self.tree.xpath('//div[contains(@class,"product-card")]//div[@class="product-card__content--center col-sm-8 col-12"]')
        interest_list = [x.findall('div')[1].findall('span')[1].text for x in interest_elements]
        return [float(x.replace('%','').replace(' ','').replace(',','.').replace('*','')) for x in interest_list]

    def _scrape(self, principal_list, tenure_list, product_name, timeout=5):
        for principal in principal_list:
            for tenure in tenure_list:
                url = self.url_pattern.format(str(tenure), str(principal))
                self._goto_page(url, timeout)

                if not self.alert_log:
                    bank_list = self._get_banks()
                    bank_list_adj = convert_bank_names(self.bank_dict, bank_list)
                    campaign_list = fill_campaign_list(bank_list_adj, product_name)
                    nof_banks = len(bank_list)
                    rate_list = self._get_rates()
                    try:
                        temp_df = pd.DataFrame({
                            'bank':bank_list, 
                            'campaign':campaign_list,
                            'principal':[principal]*nof_banks, 
                            'tenure':[tenure]*nof_banks, 
                            'interest':rate_list
                        })
                    except:
                        self.alert_log.append(13)
                        temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])    
                else:
                    temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])

                self.result = self.result.append(temp_df)
        
        if not self.alert_log:
            self.result.reset_index(inplace=True, drop=True)
            self.result.bank = self.result.bank.replace(self.bank_dict)
            self.result = self.result[self.result.bank.isin(self.bank_dict.values())]
            self.result.sort_values('bank', inplace=True)
            self.result.reset_index(inplace=True, drop=True) 
            self.scrape_status = 1
        else:
            pass

class HesapKurduGPL(HesapKurdu):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hesapkurdu.com/ihtiyac-kredisi/{}-ay-{}-tl'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='İhtiyaç', timeout=timeout)  


class HesapKurduMOR(HesapKurdu):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hesapkurdu.com/konut-kredisi/{}-ay-{}-tl'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='Konut', timeout=timeout) 

class HesapKurduCAR(HesapKurdu):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hesapkurdu.com/tasit-kredisi/{}-ay-{}-tl-sifir-arac'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='0 km Taşıt', timeout=timeout) 


class HesapKurduCA2(HesapKurdu):
    def __init__(self):
        super().__init__()
        self.url_pattern = 'https://www.hesapkurdu.com/tasit-kredisi/{}-ay-{}-tl-ikinci-el'

    def scrape(self, principal_list, tenure_list, timeout=5):
        self._scrape(principal_list, tenure_list, product_name='2. El Taşıt', timeout=timeout) 