from .EnUygun import EnUygunGPL, EnUygunMOR, EnUygunCAR, EnUygunCA2
from .HangiKredi import HangiKrediGPL, HangiKrediMOR, HangiKrediCAR, HangiKrediCA2
from .HesapKurdu import HesapKurduGPL, HesapKurduMOR, HesapKurduCAR, HesapKurduCA2


import itertools
import numpy as np
import pandas as pd


class AggProduct:
    def __init__(self, product_name, principal_list, tenure_list):
        self.product_name = product_name
        self.principal_list = principal_list
        self.tenure_list = tenure_list
        self.alert_log = []
        self.scrape_status = []

    def _initiate_instances(self):
        exec('self.eUygun = EnUygun{}()'.format(self.product_name))
        exec('self.hKredi = HangiKredi{}()'.format(self.product_name))
        exec('self.hKurdu = HesapKurdu{}()'.format(self.product_name))

    def _run_instances(self, timeout=5):
        self.to_compare = [self.eUygun, self.hKredi, self.hKurdu]

        for obj in self.to_compare:
            print(obj.source_name, ' started')
            obj.scrape(self.principal_list, self.tenure_list, timeout=timeout)
            if obj.alert_log != []:
                nof_alerts = len(obj.alert_log)
                self.alert_log = self.alert_log + list(zip([obj.source_name]*nof_alerts, obj.alert_log))

    def _check_scrape_status(self):       
        for obj in self.to_compare:
            self.scrape_status.append([obj.source_name, obj.scrape_status])
        self.to_compare = list(filter(lambda x: x.scrape_status ==1, self.to_compare))

    def _construct_basic_results(self, bank_list):
        self.result = pd.DataFrame(list(itertools.product(bank_list, self.principal_list, self.tenure_list)), columns=['bank','principal','tenure'])
        self.result['campaign'] = np.nan
        self.result['interest'] = np.nan

        for obj in self.to_compare:
            self.result = self.result.merge(obj.result, on=['bank','principal','tenure'], how='left', suffixes=('','_'+obj.source_name))

        self.result.drop(['campaign','interest'], axis=1, inplace=True)
        interest_cols = list(filter(lambda x: x[:8] == 'interest', self.result.columns))
        self.result = self.result[~(self.result[interest_cols].isnull().sum(axis=1) == len(interest_cols))]
        self.result.reset_index(inplace=True, drop=True)
        
    def _compare_interests(self):
        interest_cols = list(filter(lambda x: x[:8] == 'interest', self.result.columns))
        df =  self.result[interest_cols].mode(axis=1)
        self.result['interest'] = df.min(axis=1)

        def find_col_name(row):
            idx = row.name
            col_name = self.result.columns[row.eq(row['interest'])][0]
            col_idx = self.result.columns.tolist().index(col_name)
            return row.iloc[col_idx - 1]

        def find_source(row):
            idx = row.name
            col_name = self.result.columns[row.eq(row['interest'])][0]
            return col_name[9:]

        self.result['campaign'] = self.result.apply(find_col_name, axis=1)
        self.result['source'] = self.result.apply(find_source, axis=1)
        self.result = self.result[~self.result.sort_values(['bank','principal','tenure','interest','campaign'])[['bank','principal','tenure','interest']].duplicated()]
        self.result['product'] = self.product_name
        self.result = self.result[['product', 'bank', 'campaign', 'principal', 'tenure', 'source', 'interest']]
        self.result.reset_index(inplace=True, drop=True)

    def _min_principal_tenure_control(self, df):
        min_results = df.groupby('bank')['principal','tenure'].min()
        for bank in min_results.index.tolist():
            min_principal = min_results.loc[bank].principal
            min_tenure = min_results.loc[bank].tenure
            if df[(df.bank == bank) & (df.principal == min_principal) & (df.tenure == min_tenure)].shape[0] == 0:               
                min_tenure_for_pr = df[(df.bank == bank) & (df.principal == min_principal)].tenure.min()
                dff = df[(df.bank == bank) & (df.principal == min_principal) & (df.tenure == min_tenure_for_pr)]
                dff['tenure'] = min_tenure
                dff['source'] = 'Correction'
                df = df.append(dff)
        return df

    def _max_principal_tenure_control(self, df):
        max_results = df.groupby('bank')['principal','tenure'].max()
        for bank in max_results.index.tolist():
            max_principal = max_results.loc[bank].principal
            max_tenure = max_results.loc[bank].tenure
            if df[(df.bank == bank) & (df.principal == max_principal) & (df.tenure == max_tenure)].shape[0] == 0:               
                max_tenure_for_pr = df[(df.bank == bank) & (df.principal == max_principal)].tenure.max()
                dff = df[(df.bank == bank) & (df.principal == max_principal) & (df.tenure == max_tenure_for_pr)]
                dff['tenure'] = max_tenure
                dff['source'] = 'Correction'
                df = df.append(dff)
        return df


    def _compare(self):
        bank_list = []
        for obj in self.to_compare:
            bank_list = bank_list + obj.result.bank.tolist()
        bank_list = list(set(bank_list))

        if bank_list != []:
            self._construct_basic_results(bank_list)
            self._compare_interests()
            self.result = self._min_principal_tenure_control(self.result)
            self.result = self._max_principal_tenure_control(self.result)

        else:
            self.result = pd.DataFrame(columns=['product', 'bank', 'campaign', 'principal', 'tenure', 'source', 'interest'])
            self.alert_log.append(['Aggregator',15])    
    

    def run_scrape(self, timeout=5):
        self._initiate_instances()
        self._run_instances(timeout=timeout)
        self._check_scrape_status()
        self._compare()