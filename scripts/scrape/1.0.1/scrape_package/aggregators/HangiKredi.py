from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import pandas as pd

class HangiKredi:
    def __init__(self, driver):
        self.source_name = 'HangiKredi'
        self.driver = driver
        self.alert_log = []
        self.result = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])
        self.scrape_status = 0
        self.bank_dict = {
            'Akbank':4,
            'Albaraka Türk:':21,
            'Alternatif Bank':12,
            'Anadolubank':13,
            'Burgan Bank':17,
            'DenizBank':5,
            'Enpara.com':26,
            'Fibabanka':16,
            'Garanti BBVA':2,
            'HSBC':18,
            'Halkbank':8,
            'ICBC Bank Turkey':19,
            'ING':1,
            'Kuveyt Türk':22,
            'Odeabank':15,
            'QNB Finansbank':6,
            'TEB - Türk Ekonomi Bankası':10,
            'Türkiye Finans':20,
            'Vakıf Katılım':23,
            'VakıfBank':7,
            'Yapı Kredi':3,
            'Ziraat Bankası':11,
            'İş Bankası':9,
            'Şekerbank':14
        }

    def _goto_page(self, url):
        try:
            self.driver.get(url)
        except TimeoutException:
            self.alert_log.append(14)
        except WebDriverException:
            self.alert_log.append(2)
        except:
            self.alert_log.append(1)

    def _page_control(self):
        pass

    def _check_more_button(self):
        try:
            more_button = self.driver.find_element_by_xpath('//button[@class="btn btn-primary form-control showMoreAdap"]')
            more_button.click()
        except:
            pass   

    def _get_banks(self):
        pass

    def _get_campaign_names(self):
        pass

    def _get_rates(self):
        pass

    def scrape(self, principal_list, tenure_list):
        for principal in principal_list:
            for tenure in tenure_list:
                url = self.url_pattern.format(str(principal), str(tenure))
                self._goto_page(url)
                self._page_control()
                self._check_more_button()

                if not self.alert_log:
                    bank_list = self._get_banks()
                    nof_banks = len(bank_list)
                    campaign_list = self._get_campaign_names()
                    rate_list = self._get_rates()
                    try:
                        temp_df = pd.DataFrame({
                            'bank':bank_list, 
                            'campaign':campaign_list,
                            'principal':[principal]*nof_banks, 
                            'tenure':[tenure]*nof_banks, 
                            'interest':rate_list
                        })
                    except:
                        self.alert_log.append(13)
                        temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])    
                else:
                    temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])

                self.result = self.result.append(temp_df)
        
        if not self.alert_log:
            self.result.reset_index(inplace=True, drop=True)
            self.result.bank = self.result.bank.replace(self.bank_dict)
            self.result = self.result[self.result.bank.isin(self.bank_dict.values())]
            self.result.sort_values('bank', inplace=True)
            self.result.reset_index(inplace=True, drop=True)
            self.scrape_status = 1
        else:
            pass        

class HangiKrediGPL(HangiKredi):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hangikredi.com/kredi/ihtiyac-kredisi/sorgulama?Tutar={}&Vade={}'

    def _page_control(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(('xpath','//div[@class="list-product-item__column list-product-item__first-column"]')))
        except TimeoutException:
            self.alert_log.append(4)

    def _get_banks(self):
        bank_elements = self.driver.find_elements_by_xpath('//div[@class="list-product-item__column list-product-item__first-column"]/div/a')
        bank_list = [x.get_attribute('title') for x in bank_elements]
        return [x[:-15].strip() for x in bank_list]

    def _get_campaign_names(self):
        campaign_elements = self.driver.find_elements_by_xpath('//div[@class="list-product-item__column list-product-item__first-column"]/div/p')
        return [x.text for x in campaign_elements]

    def _get_rates(self):
        interest_elements = self.driver.find_elements_by_xpath('//div[@class="list-product-item__column list-product-item__second-column"]/div/p')
        interest_list = [x.text for x in interest_elements]
        interest_list = list(filter(lambda x: x != '', interest_list))
        return [float(x.replace('%','').replace(' ','').replace(',','.')) for x in interest_list]



class HangiKrediMOR(HangiKredi):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hangikredi.com/kredi/konut-kredisi/sorgulama?Tutar={}&Vade={}'

    def _page_control(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(('xpath','//div[@class="housingResults"]')))
        except TimeoutException:
            self.alert_log.append(4)

    def _get_banks(self):
        bank_elements = self.driver.find_elements_by_xpath('//div[@class="housingResults"]//div[@class="housingBank"]//a')
        return [x.get_attribute('title') for x in bank_elements]

    def _get_campaign_names(self):
        campaign_elements = self.driver.find_elements_by_xpath('//div[@class="housingResults"]//div[@class="housingBank"]//p')
        return [x.text for x in campaign_elements]

    def _get_rates(self):
        interest_elements = self.driver.find_elements_by_xpath('//div[@class="housingResults"]//div[@class="housingFaiz short"]//strong')
        interest_list = [x.text for x in interest_elements]
        interest_list = list(filter(lambda x: x != '', interest_list))
        return [float(x.replace('%','').replace(' ','').replace(',','.')) for x in interest_list]


class HangiKrediCar(HangiKredi):
    def __init__(self, driver):
        super().__init__(driver)

    def _page_control(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(('xpath','//table[contains(@id,"bank-product-list")]')))
        except TimeoutException:
            self.alert_log.append(4)

    def _check_more_button(self):
        try:
            more_button = self.driver.find_element_by_xpath('//button[@class="btn btn-primary form-control showMoreAdap"]')
            more_button.click()
        except:
            pass    

    def _get_banks(self):
        bank_elements = self.driver.find_elements_by_xpath('//table[contains(@id,"bank-product-list")]//tr/td/a')
        bank_list = [x.get_attribute('title') for x in bank_elements]
        return [x[:-13].strip() for x in bank_list]

    def _get_campaign_names(self):
        campaign_elements = self.driver.find_elements_by_xpath('//table[contains(@id,"bank-product-list")]//tr/td//div[@class="textUrunAdi"]')
        campaign_list = [x.text for x in campaign_elements]
        campaign_list = list(filter(lambda x: x != '', campaign_list))
        return campaign_list

    def _get_rates(self):
        interest_elements = self.driver.find_elements_by_xpath('//table[contains(@id,"bank-product-list")]//tr/td[contains(@class,"tdFaiz")]//span')
        interest_list = [x.text for x in interest_elements]
        interest_list = list(filter(lambda x: x != '', interest_list))
        return [float(x.replace('%','').replace(' ','').replace(',','.')) for x in interest_list]


class HangiKrediCAR(HangiKrediCar):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hangikredi.com/kredi/tasit-kredisi/sorgulama?Tutar={}&Vade={}&VehicleStatus=0&CarBrand=&CarModelYear=&CarModel='

class HangiKrediCA2(HangiKrediCar):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hangikredi.com/kredi/tasit-kredisi/sorgulama?Tutar={}&Vade={}&VehicleStatus=1&CarBrand=&CarModelYear=&CarModel='