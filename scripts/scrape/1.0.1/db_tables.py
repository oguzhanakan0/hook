import sqlite3
import pandas as pd

conn = sqlite3.connect('AggregatorScrape.db')

## REFERENCES
banks = pd.DataFrame(
[
    [1, 'ING'],
    [2, 'Garanti BBVA'],
    [3, 'Yapı Kredi'],
    [4, 'Akbank'],
    [5, 'Denizbank'],
    [6, 'QNB Finansbank'],
    [7, 'Vakıfbank'],
    [8, 'Halkbank'],
    [9, 'İş Bankası'],
    [10, 'TEB'],
    [11, 'Ziraat Bankası'],
    [12, 'Alternatif Bank'],
    [13, 'Anadolubank'],
    [14, 'Şekerbank'],
    [15, 'Odeabank'],
    [16, 'Fibabanka'],
    [17, 'Burgan Bank'],
    [18, 'HSBC'],
    [19, 'ICBC'],
    [20, 'Türkiye Finans'],
    [21, 'Albaraka Türk'],
    [22, 'Kuveyt Türk'],
    [23, 'Vakıf Katılım'],
    [24, 'Ziraat Katılım'],
    [25, 'TurkishBank'],
    [26, 'Enpara']
],
columns=['bank_id', 'bank_name'])

products = pd.DataFrame({'product_id':[1,2,3,4], 'product':['GPL','Mortgage','CarNew', 'CarSecondHand']})

principals = pd.DataFrame(
    {
        'product': [1]*4 + [2]*5 + [3]*5 + [4]*5,
        'principal': [5000,20000,50000,100000] + [50000, 100000,250000,500000,1000000] + [20000,50000,100000,150000,250000] +  [20000,50000,100000,150000,250000] 
    }
)

tenures = pd.DataFrame(
    {
        'product': [1]*5 + [2]*11 + [3]*7 + [4]*7,
        'tenure': [3,6,12,24,36] + [6,12,24,36,48,60,72,84,96,108,120] + [3,6,12,24,36,48,60] + [3,6,12,24,36,48,60]
    }
)

alert_defs = pd.DataFrame([
	[1, 'Unknown error', 'STOP'],
	[2, 'Address not found', 'STOP'],
	[3, '404 in bank domain', 'STOP'],
	[4, 'Page control element not visible', 'STOP'],
	[5, 'Product element not found', 'STOP'],
	[6, 'Campaign element not found', 'STOP'],
	[7, 'Additional campaign', 'CONTINUE'],
	[8, 'Removed campaign', 'CONTINUE'],
	[9, 'No campaign left', 'STOP'],
	[10, 'Principal/tenure element not found', 'STOP'],
	[11, 'Interest element not found', 'STOP'],
	[12, 'Submit element not found', 'STOP'],
	[13, 'Aggregator list has not been collected correctly', 'STOP'],
    [14, 'Timeout error'],
    [15, 'No result from aggregators']
],
columns=['alert_id', 'definition', 'action'])





## RESULTS
scrape_results = pd.DataFrame(columns=['product','bank','campaign','principal','tenure','source','interest','date','program'])
scrape_alerts = pd.DataFrame(columns=['product','source','alert_id','date','program'])
scrape_status = pd.DataFrame(columns=['product','source','status','date','program'])


## TO DB

banks.to_sql('banks', conn, if_exists='replace', index=False)
products.to_sql('products', conn, if_exists='replace', index=False)
alert_defs.to_sql('alert_defs', conn, if_exists='replace', index=False)
principals.to_sql('principals', conn, if_exists='replace', index=False)
tenures.to_sql('tenures', conn, if_exists='replace', index=False)

scrape_results.to_sql('scrape_results', conn, if_exists='replace', index=False)
scrape_alerts.to_sql('scrape_alerts', conn, if_exists='replace', index=False)
scrape_status.to_sql('scrape_status', conn, if_exists='replace', index=False)