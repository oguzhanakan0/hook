from .. import scrape_tools
from ..scrape_tools import BaseScrape
from ..scrape_tools import CampaignControl

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time

class FinansBank(BaseScrape):

    def _control_product_locator(self):
        product_elements = self.driver.find_elements_by_xpath('//a[@data-toggle="tab"]')
        if not product_elements:
            self.alert_log.append(5)

    def _set_product(self):
        product_elements = self.driver.find_elements_by_xpath('//a[@data-toggle="tab"]')
        if sum(['ihtiyac' in i for i in [i.get_attribute('href') for i in product_elements]]) > 0:
            try:
                self.driver.find_element_by_xpath('//a[@class="notifyclose HeaderpopupClose"]').click()
            except:
                pass
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                ('xpath','//a[@href="#ihtiyac"]')))
            self.driver.find_element_by_xpath('//a[@href="#ihtiyac"]').click()
        else:
            self.alert_log.append(5)

    def _control_principal_tenure_locator(self):
        principal_elements = self.driver.find_elements_by_xpath('//input[contains(@id,"loan-landing-credit")]')
        tenure_elements = self.driver.find_elements_by_xpath('//span[contains(@id, "select2-loan-landing-maturity")]')
        for locator in [principal_elements, tenure_elements]:
            if not locator:
                self.alert_log.append(10)

    def _control_interest_locator(self):
        if scrape_tools._element_found(self.driver, ('xpath','//div[@data-layer-key="krediFaiz"]')):
            self.alert_log.append(11)

    def _control_submit_locator(self):
        submit_elements = self.driver.find_elements_by_xpath('//button[contains(@class,"btn-calculate")]')
        if not submit_elements:
            self.alert_log.append(12)

    def interest_control_step(self):
        for met in ['_control_principal_tenure_locator', '_control_interest_locator', '_control_submit_locator']:
            if list(filter(lambda x: x not in [7, 8], self.alert_log)) == []:
                print('Running ', met)
                self.__getattribute__(met)()

    def _set_principal(self, principal):
        principal_elements = self.driver.find_elements_by_xpath('//input[contains(@id,"loan-landing-credit")]')
        principal_element = list(filter(lambda i: i.is_displayed(), principal_elements))[0]
        for i in range(7):
            principal_element.send_keys(Keys.BACK_SPACE)
        principal_element.send_keys(principal)
        time.sleep(1)

    def _set_tenure(self, tenure):
        tenure_elements = self.driver.find_elements_by_xpath('//span[contains(@id, "select2-loan-landing-maturity")]')
        tenure_element = list(filter(lambda i: i.is_displayed(), tenure_elements))[0]
        tenure_element.click()

        time.sleep(1)
        tenures = self.driver.find_elements_by_xpath('//ul[contains(@id,"select2-loan-landing-maturity")]/li[contains(@id,"{}")]'.format(tenure))
        tenure_index = [i.text for i in tenures].index('{} Ay'.format(tenure))
        tenures[tenure_index].click()

    def _read_interest(self):
        submit_elements = self.driver.find_elements_by_xpath('//button[contains(@class,"btn-calculate")]')
        submit_element = list(filter(lambda i: i.is_displayed(), submit_elements))[0]
        submit_element.click()
        time.sleep(1)

        interest = self.driver.find_element_by_xpath('//div[@data-layer-key="krediFaiz"]').text
        interest = interest.replace('%','').replace(',','.').strip()
        return float(interest)


def run_scrape(driver, principal_list, tenure_list, campaign_list=None):
    print('Running FinansBank')
    fb = FinansBank(
        driver,
        'https://www.qnbfinansbank.com/bireysel-tuketici-kredileri',
        'Tüketici Kredileri | QNB Finansbank',
        ''' 'Aradığınız sayfaya ulaşılamıyor!' in [i.text for i in FFdriver.find_elements_by_tag_name('p')] ''',
        ('xpath', '//a[@data-toggle="tab"]')
    )
    fb.page_product_step()
    fb.campaign_control_step()
    fb.interest_control_step()
    fb.control_scrape_initiation()
    fb.scrape(principal_list, tenure_list)
    return fb.alert_log, fb.scrape_status, fb.result
