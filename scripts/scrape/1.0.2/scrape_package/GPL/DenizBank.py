from .. import scrape_tools
from ..scrape_tools import BaseScrape
from ..scrape_tools import CampaignControl

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

import time


class DenizBank(BaseScrape):

    def _control_product_locator(self):
        WebDriverWait(self.driver, 10).until_not(EC.presence_of_element_located((By.XPATH,'//div[@class="blockUI blockOverlay"]')))
        if scrape_tools._element_found(self.driver, ('id','s2id_ddlKrediSelect')):
            self.alert_log.append(5)

    def _control_campaign_locator(self):
        WebDriverWait(self.driver, 10).until_not(
            EC.presence_of_element_located((By.XPATH, '//div[@class="blockUI blockOverlay"]')))
        if scrape_tools._element_found(self.driver, ('id','s2id_ddlKrediSelect')):
            self.alert_log.append(5)

    def _create_available_campaigns(self):
        self.driver.find_element_by_id('s2id_ddlKrediSelect').click()
        camp_list = self.driver.find_elements_by_xpath('//ul[@class="select2-results"]/li/div')
        camp_list = [i.get_attribute('textContent') for i in camp_list]
        for i in ['Mortgage','Taşıt']:
            if i in camp_list:
                camp_list.remove(i)
        self.driver.find_element_by_id('select2-drop-mask').click()
        self.available_campaigns = camp_list

    def _control_campaign_process(self):
        CampContObj = CampaignControl(self.defined_campaigns, self.available_campaigns)
        CampContObj.campaign_control()
        self.final_campaigns = CampContObj.final_campaigns
        self.alert_log = self.alert_log + CampContObj.campaign_alert_log

    def _control_submit_locator(self, submit_locator):
        if scrape_tools._element_found(self.driver, submit_locator):
            self.alert_log.append(12)

    def _set_campaign(self, campaign):
        self.driver.find_element_by_id('s2id_ddlKrediSelect').click()
        camp_list = self.driver.find_elements_by_xpath('//ul[@class="select2-results"]/li/div')
        idx = [i.get_attribute('textContent') for i in camp_list].index(campaign)
        camp_list[idx].click()
        WebDriverWait(self.driver, 10).until_not(
            EC.presence_of_element_located((By.XPATH, '//div[@class="blockUI blockOverlay"]')))

    def _set_principal(self, principal):
        principal_element = self.driver.find_element_by_id('kreditutari')
        for i in range(7):
            principal_element.send_keys(Keys.BACK_SPACE)
        principal_element.send_keys(principal)

    def _set_tenure(self, tenure):
        tenure_element = self.driver.find_element_by_id('vade')
        tenure_element.clear()
        time.sleep(1)
        tenure_element.send_keys(tenure)

    def _read_interest(self):
        self.driver.find_element_by_xpath('//a[@class="calculator tertiary-action"]').click()
        WebDriverWait(self.driver, 10).until_not(
            EC.presence_of_element_located((By.XPATH, '//div[@class="blockUI blockOverlay"]')))
        interest = self.driver.find_element_by_xpath('//span[@class="faizOrani bold"]').text
        interest = interest.replace('%', '').replace(',', '.').strip()
        return float(interest)


def run_scrape(driver, principal_list, tenure_list, campaign_list=None):
    print('Running DenizBank')
    db = DenizBank(
        driver,
        'https://www.denizbank.com/hesaplama-araclari/no-flash/bireysel-web.html?v2',
        '',
        ''' 'Aradığınız sayfa maalesef bulunamadı.' in [i.text for i in self.driver.find_elements_by_tag_name('p')] ''',
        ('id', 's2id_ddlKrediSelect'),
        campaign_list
    )
    db.page_product_step()
    db.campaign_control_step()
    db.interest_control_step(
        ('id', 'kreditutari'),
        ('id', 'vade'),
        ('xpath', '//span[@class="faizOrani bold"]'),
        ('xpath', '//a[@class="calculator tertiary-action"]')
    )
    db.control_scrape_initiation()
    db.scrape(principal_list, tenure_list)
    return db.alert_log, db.scrape_status, db.result