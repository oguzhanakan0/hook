from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import pandas as pd

class Kredya:
    def __init__(self, driver):
        self.source_name = 'Kredya'
        self.driver = driver
        self.alert_log = []
        self.result = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])
        self.scrape_status = 0
        self.bank_dict = {
            'Akbank':4,
            'Albaraka Türk ':21,
            'Burgan Bank':17,
            'Deniz Bank':5,
            'Fibabanka':16,
            'Garanti BBVA':2,
            'Halkbank':8,
            'Ing':1,
            'Kuveyt Türk':22,
            'QNB Finansbank':6,
            'Türk Ekonomi Bankası':10,
            'Türkiye Finans':20,
            'Türkiye İş Bankası':9,
            'Vakıf Katılım':23,
            'Vakıfbank':7,
            'Yapı Kredi':3,
            'Ziraat Bankası':11,
            'Ziraat Katılım':24
        }

    def _goto_page(self, url):
        try:
            self.driver.get(url)
        except TimeoutException:
            self.alert_log.append(14)
        except WebDriverException:
            self.alert_log.append(2)
        except:
            self.alert_log.append(1)

    def _page_control(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(('xpath','//div[@class="bank-result-list"]')))
        except TimeoutException:
            self.alert_log.append(4)

    def _get_banks(self):
        bank_elements = self.driver.find_elements_by_xpath('//div[@class="bank-result-list"]//div[contains(@class,"general-list-row body-row credit-open-detail")]/div[@class="general-list-col bank-info"]/img')
        return [x.get_attribute('alt') for x in bank_elements]

    def _get_campaign_names(self):
        campaign_elements = self.driver.find_elements_by_xpath('//div[@class="bank-result-list"]//div[contains(@class,"general-list-row body-row credit-open-detail")]/div[@class="general-list-col bank-info"]/span')
        campaign_list = [x.text for x in campaign_elements]
        campaign_list = list(filter(lambda x: x != '', campaign_list))
        return campaign_list

    def _get_rates(self):
        interest_elements = self.driver.find_elements_by_xpath('//div[@class="bank-result-list"]//div[contains(@class,"general-list-row body-row credit-open-detail")]')
        interest_list = [x.find_elements_by_tag_name('div')[1].find_element_by_tag_name('b').text for x in interest_elements]
        return [float(x.replace('%','').replace(' ','').replace(',','.')) for x in interest_list]

    def scrape(self, principal_list, tenure_list):
        for principal in principal_list:
            for tenure in tenure_list:
                url = self.url_pattern.format(str(tenure), str(principal))
                self._goto_page(url)
                self._page_control()

                if not self.alert_log:
                    bank_list = self._get_banks()
                    nof_banks = len(bank_list)
                    campaign_list = self._get_campaign_names()
                    rate_list = self._get_rates()
                    try:
                        temp_df = pd.DataFrame({
                            'bank':bank_list, 
                            'campaign':campaign_list,
                            'principal':[principal]*nof_banks, 
                            'tenure':[tenure]*nof_banks, 
                            'interest':rate_list
                        })
                    except:
                        self.alert_log.append(13)
                        temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])    
                else:
                    temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])

                self.result = self.result.append(temp_df)
        
        if not self.alert_log:
            self.result.reset_index(inplace=True, drop=True)
            self.result.bank = self.result.bank.replace(self.bank_dict)
            self.result = self.result[self.result.bank.isin(self.bank_dict.values())]
            self.result.sort_values('bank', inplace=True)
            self.result.reset_index(inplace=True, drop=True)
            self.scrape_status = 1
        else:
            pass

class KredyaGPL(Kredya):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.kredya.com/bankalar/ihtiyac-kredisi/{}-ay-vadeli-{}-tl'

class KredyaMOR(Kredya):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.kredya.com/bankalar/konut-kredisi/{}-ay-vadeli-{}-tl'

class KredyaCAR(Kredya):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.kredya.com/bankalar/tasit-kredisi/{}-ay-vadeli-{}-tl'

