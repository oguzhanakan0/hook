from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import pandas as pd

class HesapKurdu:
    def __init__(self, driver):
        self.source_name = 'HesapKurdu'
        self.driver = driver
        self.alert_log = []
        self.result = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])
        self.scrape_status = 0
        self.bank_dict = {
            'Akbank':4,
            'Burgan Bank':17,
            'DenizBank':5,
            'Fibabanka':16,
            'Garanti BBVA':2,
            'Halkbank':8,
            'ICBC Turkey':19,
            'ING':1,
            'Odeabank':15,
            'QNB Finansbank':6,
            'TEB':10,
            'TurkishBank':25,
            'Türkiye Finans Katılım Bankası':20,
            'Vakıf Katılım':23,
            'Vakıfbank':7,
            'Yapı Kredi':3,
            'Ziraat Bankası':11,
            'Ziraat Katılım':24,
            'İş Bankası':9,
            'Şekerbank':14
        }

    def _goto_page(self, url):
        try:
            self.driver.get(url)
        except TimeoutException:
            self.alert_log.append(14)
        except WebDriverException:
            self.alert_log.append(2)
        except:
            self.alert_log.append(1)

    def _page_control(self):
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(('xpath','//div[@class="product-card"]')))
        except TimeoutException:
            self.alert_log.append(4)

    def _get_banks(self):
        bank_elements = self.driver.find_elements_by_xpath('//div[@class="product-card"]//div[@class="product-card__content"]//img')
        return [x.get_attribute('title') for x in bank_elements]

    def _get_rates(self):
        interest_elements = self.driver.find_elements_by_xpath('//div[@class="product-card"]//div[@class="product-card__content--center col-sm-8 col-12"]/div/following-sibling::div')
        interest_list = [i.text for i in interest_elements]
        interest_list = list(filter(lambda x: interest_list.index(x) % 2 == 0, interest_list))
        interest_list = [i[i.find('\n')+1:i.find('\n', i.find('\n')+1)] for i in interest_list]
        return [float(x.replace('%','').replace(' ','').replace(',','.').replace('*','')) for x in interest_list]

    def _scrape(self, principal_list, tenure_list, product_name):
        for principal in principal_list:
            for tenure in tenure_list:
                url = self.url_pattern.format(str(tenure), str(principal))
                self._goto_page(url)
                self._page_control()

                if not self.alert_log:
                    bank_list = self._get_banks()
                    nof_banks = len(bank_list)
                    rate_list = self._get_rates()
                    try:
                        temp_df = pd.DataFrame({
                            'bank':bank_list, 
                            'campaign':[product_name]*nof_banks,
                            'principal':[principal]*nof_banks, 
                            'tenure':[tenure]*nof_banks, 
                            'interest':rate_list
                        })
                    except:
                        self.alert_log.append(13)
                        temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])    
                else:
                    temp_df = pd.DataFrame(columns=['bank','campaign','principal','tenure','interest'])

                self.result = self.result.append(temp_df)
        
        if not self.alert_log:
            self.result.reset_index(inplace=True, drop=True)
            self.result.bank = self.result.bank.replace(self.bank_dict)
            self.result = self.result[self.result.bank.isin(self.bank_dict.values())]
            self.result.sort_values('bank', inplace=True)
            self.result.reset_index(inplace=True, drop=True) 
            self.scrape_status = 1
        else:
            pass


class HesapKurduGPL(HesapKurdu):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hesapkurdu.com/ihtiyac-kredisi/{}-ay-{}-tl'

    def scrape(self, principal_list, tenure_list):
        self._scrape(principal_list, tenure_list, product_name='İhtiyaç Kredisi')  


class HesapKurduMOR(HesapKurdu):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hesapkurdu.com/konut-kredisi/{}-ay-{}-tl'

    def scrape(self, principal_list, tenure_list):
        self._scrape(principal_list, tenure_list, product_name='Konut Kredisi') 

class HesapKurduCAR(HesapKurdu):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hesapkurdu.com/tasit-kredisi/{}-ay-{}-tl-sifir-arac'

    def scrape(self, principal_list, tenure_list):
        self._scrape(principal_list, tenure_list, product_name='0 km Taşıt Kredisi') 


class HesapKurduCA2(HesapKurdu):
    def __init__(self, driver):
        super().__init__(driver)
        self.url_pattern = 'https://www.hesapkurdu.com/tasit-kredisi/{}-ay-{}-tl-ikinci-el'

    def scrape(self, principal_list, tenure_list):
        self._scrape(principal_list, tenure_list, product_name='2. El Taşıt Kredisi') 