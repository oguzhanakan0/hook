from .EnUygun import EnUygunGPL, EnUygunMOR, EnUygunCAR, EnUygunCA2
from .HangiKredi import HangiKrediGPL, HangiKrediMOR, HangiKrediCAR, HangiKrediCA2
from .HesapKurdu import HesapKurduGPL, HesapKurduMOR, HesapKurduCAR, HesapKurduCA2
from .Kredya import KredyaGPL, KredyaMOR, KredyaCAR

import itertools
import numpy as np
import pandas as pd


class AggProduct:
    def __init__(self, driver, product_name, principal_list, tenure_list):
        self.driver = driver
        self.product_name = product_name
        self.principal_list = principal_list
        self.tenure_list = tenure_list
        self.alert_log = []
        self.scrape_status = []

    def _initiate_instances(self):
        exec('self.eUygun = EnUygun{}(self.driver)'.format(self.product_name))
        exec('self.hKredi = HangiKredi{}(self.driver)'.format(self.product_name))
        exec('self.hKurdu = HesapKurdu{}(self.driver)'.format(self.product_name))
        if self.product_name != 'CA2':
            exec('self.kredya = Kredya{}(self.driver)'.format(self.product_name))

    def _run_instances(self):
        if self.product_name != 'CA2':
            self.to_compare = [self.eUygun, self.hKredi, self.hKurdu, self.kredya]
        else:
            self.to_compare = [self.eUygun, self.hKredi, self.hKurdu]

        for obj in self.to_compare:
            print(obj.source_name, ' started')
            obj.scrape(self.principal_list, self.tenure_list)
            if obj.alert_log != []:
                nof_alerts = len(obj.alert_log)
                self.alert_log = self.alert_log + list(zip([obj.source_name]*nof_alerts, obj.alert_log))

    def _check_scrape_status(self):       
        for obj in self.to_compare:
            self.scrape_status.append([obj.source_name, obj.scrape_status])
        self.to_compare = list(filter(lambda x: x.scrape_status ==1, self.to_compare))

    def _compare(self):
        bank_list = []
        for obj in self.to_compare:
            bank_list = bank_list + obj.result.bank.tolist()
        bank_list = list(set(bank_list))

        self.result = pd.DataFrame(list(itertools.product(bank_list, self.principal_list, self.tenure_list)), columns=['bank','principal','tenure'])
        self.result['campaign'] = np.nan
        self.result['interest'] = np.nan

        for obj in self.to_compare:
            self.result = self.result.merge(obj.result, on=['bank','principal','tenure'], how='left', suffixes=('','_'+obj.source_name))

        self.result.drop(['campaign','interest'], axis=1, inplace=True)
        interest_cols = list(filter(lambda x: x[:8] == 'interest', self.result.columns))
        self.result = self.result[~(self.result[interest_cols].isnull().sum(axis=1) == len(interest_cols))]
        self.result.reset_index(inplace=True, drop=True)
        self.result['interest'] = self.result[interest_cols].mode(axis=1)[0]

        def find_col_name(row):
            idx = row.name
            col_name = self.result.columns[row.eq(row['interest'])][0]
            col_idx = self.result.columns.tolist().index(col_name)
            return row.iloc[col_idx - 1]

        def find_source(row):
            idx = row.name
            col_name = self.result.columns[row.eq(row['interest'])][0]
            return col_name[9:]

        self.result['campaign'] = self.result.apply(find_col_name, axis=1)
        self.result['source'] = self.result.apply(find_source, axis=1)
        self.result = self.result[~self.result.sort_values(['bank','principal','interest','interest'])[['bank','principal','tenure','interest']].duplicated()]
        self.result['product'] = self.product_name
        self.result = self.result[['product', 'bank', 'campaign', 'principal', 'tenure', 'source', 'interest']]
        self.result.reset_index(inplace=True, drop=True)

    
    def run_scrape(self):
        self._initiate_instances()
        self._run_instances()
        self._check_scrape_status()
        self._compare()