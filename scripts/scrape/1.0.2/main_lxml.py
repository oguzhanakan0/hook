#!/usr/bin/env python
# coding: utf-8

# In[10]:


"""
run script via:
> python manage.py shell < scripts/scrape/1.0.1/main_lxml.py >> scripts/scrape/1.0.1/data/scrape_job_log/scrape_job_log.txt 
"""
print('== job start ==')
import django
django.setup()
import sys
import os
sys.path.append("scripts/scrape/1.0.1/")
print("Current Working Directory " , os.getcwd())
import scrape_package as sp
import pandas as pd
import datetime
import sqlite3
import warnings
from loans.models import *
from scraping.models import *
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
warnings.filterwarnings("ignore")

products = [e.loan_type for e in LoanType.objects.all()]

alertlog, scrapelog, loans = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()

start = datetime.datetime.now()
for product_name in products:
    print('-- {} --'.format(product_name))
    criticals = Critical.objects.filter(loan_type=product_name)
    tenure_list = sorted(set([e.tenure.tenure for e in criticals]))
    principal_list = sorted(set([e.principal.principal for e in criticals]))
    print(principal_list)
    print(tenure_list)
    print('aggregation started')
    agg = sp.aggregators_lxml.aggregator.AggProduct(product_name, principal_list, tenure_list)
    agg.run_scrape(timeout=10)
    print('aggregation ended')
    print('creating: alert_log table')
    if not agg.alert_log:
        print('no alert log')
        pass
    else:
        al = pd.DataFrame(agg.alert_log, columns=['source', 'alert_id'])
        al['product'] = product_name
        al = al[['product','source','alert_id']]
        if alertlog.empty:
            alertlog = al
        else:
            alertlog = pd.concat([alertlog,al])
    print('created: alert_log table')
    print('creating: scrape_status table')
    sc = pd.DataFrame(agg.scrape_status, columns=['source', 'status'])
    sc['product'] = product_name
    sc = sc[['product','source','status']]
    if scrapelog.empty:
        scrapelog = sc
    else:
        scrapelog = pd.concat([scrapelog,sc])
    print('created: scrape_status table')
    print('creating: loans table')
    if sc.status.sum() == 0:
        print('scrape status 0. passing')
        pass
    else:
        lo = agg.result.copy()
        lo['interest'] = round(lo['interest']/100,4)
        print('lo.shape:')
        print(lo.shape)
        if loans.empty:
            loans = lo
        else:
            loans = pd.concat([loans,lo])
        print('cumulative shape:')
        print(loans.shape)
    end = datetime.datetime.now()
    print('Time passed')
    print(end - start)


loans.to_pickle('scripts/scrape/1.0.1/data/'+datetime.date.today().strftime('%Y%m%d')+'_loans.pkl')
alertlog.to_pickle('scripts/scrape/1.0.1/data/'+datetime.date.today().strftime('%Y%m%d')+'_alertlog.pkl')
scrapelog.to_pickle('scripts/scrape/1.0.1/data/'+datetime.date.today().strftime('%Y%m%d')+'_scrapelog.pkl')
print('== job end ==')
