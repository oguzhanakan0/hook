#!/bin/bash
echo $(date): daily report job started && printf "\n" &&
DD=$(date +%d)
MM=$(date +%m)
YYYY=$(date +%Y)
set PYTHONIOENCODING=utf-8 &&
source ~/anaconda3/etc/profile.d/conda.sh && conda activate digi &&
cd /hook &&
python manage.py shell_plus < scripts/py/jobs/exec_daily_report.py >> scripts/py/jobs/logs/daily_report_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): end job.
