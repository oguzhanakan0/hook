echo %date% %time%: scrape_job.bat started && ^
set PYTHONIOENCODING=utf-8 && ^
C:\Anaconda3\Scripts\activate && ^
echo %date% %time%: anaconda activated && ^
conda activate digi && ^
echo %date% %time%: digi env activated && ^
cd C:\hook && ^
echo %date% %time%: running scripts/scrape/1.0.1/main_lxml.py && ^
python manage.py shell < scripts/scrape/1.0.1/main_lxml.py >> scripts/scrape/1.0.1/logs/scrape_job_log_%date:~10,4%_%date:~4,2%_%date:~7,2%.txt && ^
echo %date% %time%: running scripts/sql/deactivate_all_loans.sql && ^
psql postgresql://postgres:1122@localhost:5432/hook < scripts/sql/deactivate_all_loans.sql >> scripts/sql/logs/deactivate_all_loans_log_%date:~10,4%_%date:~4,2%_%date:~7,2%.txt && ^
echo %date% %time%: running scripts/py/jobs/exec_transfer_scraped_data.py && ^
python manage.py shell < scripts/py/jobs/exec_transfer_scraped_data.py >> scripts/py/jobs/logs/transfer_scraped_data_log_%date:~10,4%_%date:~4,2%_%date:~7,2%.txt && ^
echo %date% %time%: running scripts/sql/ensure_empty_summary_and_matrix.sql && ^
psql postgresql://postgres:1122@localhost:5432/hook < scripts/sql/ensure_empty_summary_and_matrix.sql >> scripts/sql/logs/ensure_empty_summary_and_matrix_log_%date:~10,4%_%date:~4,2%_%date:~7,2%.txt && ^
echo %date% %time%: running scripts/py/jobs/exec_loan_summary.py && ^
python manage.py shell < scripts/py/jobs/exec_loan_summary.py >> scripts/py/jobs/logs/loan_summary_log_%date:~10,4%_%date:~4,2%_%date:~7,2%.txt && ^
echo %date% %time%: running scripts/py/jobs/exec_loan_summary_matrix.py && ^
python manage.py shell < scripts/py/jobs/exec_loan_summary_matrix.py >> scripts/py/jobs/logs/loan_summary_matrix_log_%date:~10,4%_%date:~4,2%_%date:~7,2%.txt && ^
echo %date% %time%: running scripts/sql/set_cheapest_loans.sql && ^
psql postgresql://postgres:1122@localhost:5432/hook < scripts/sql/set_cheapest_loans.sql >> scripts/sql/logs/set_cheapest_loans_log_%date:~10,4%_%date:~4,2%_%date:~7,2%.txt && ^
echo %date% %time%: restarting apache server && ^
C:\wamp64\bin\apache\apache2.4.46\bin\httpd -k restart
echo scrape_job.bat ended at: %date% %time%