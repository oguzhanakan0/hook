#!/bin/bash
echo $(date): scrape_job.sh started && printf "\n" &&
DD=$(date +%d)
MM=$(date +%m)
YYYY=$(date +%Y)
set PYTHONIOENCODING=utf-8 &&
source ~/anaconda3/etc/profile.d/conda.sh && conda activate digi &&
cd /hook &&
# cd ~/Desktop/hook &&
./scripts/shell_jobs/scrape_job.sh >> /hook/scripts/shell_jobs/logs/scrape_job_bat_log_$YYYY\_$MM\_$DD.txt &&
cd /hook &&
# cd ~/Desktop/hook &&
echo $(date): sending email... && printf "\n" &&
python manage.py shell < scripts/py/jobs/exec_send_email_scrape_results.py >> scripts/py/jobs/logs/send_email_scrape_results_log_$YYYY\_$MM\_$DD.txt
echo $(date): end job.
