#!/bin/bash
DD=$(date +%d) &&
MM=$(date +%m) &&
YYYY=$(date +%Y) &&
set PYTHONIOENCODING=utf-8 &&
source ~/anaconda3/etc/profile.d/conda.sh && conda activate digi &&
echo $(date): digi environment activated && printf "\n" &&
cd /hook &&
# cd ~/Desktop/hook &&

echo $(date): running main_lxml.py  && printf "\n" &&
python manage.py shell < scripts/scrape/1.0.1/main_lxml.py >> scripts/scrape/1.0.1/logs/scrape_job_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finished main_lxml.py && printf "\n" &&

echo $(date): running deactivate_all_loans.sql && printf "\n" &&
psql postgresql://postgres:1122@localhost:5432/hook < scripts/sql/deactivate_all_loans.sql >> scripts/sql/logs/deactivate_all_loans_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finished deactivate_all_loans.sql && printf "\n" &&

echo $(date): running transfer_scraped_data.py && printf "\n" &&
python manage.py shell < scripts/py/jobs/exec_transfer_scraped_data.py >> scripts/py/jobs/logs/transfer_scraped_data_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finshed transfer_scraped_data.py && printf "\n" &&

echo $(date): running fill_missing_criticals.py && printf "\n" &&
python manage.py shell_plus < scripts/py/jobs/exec_fill_missing_criticals.py >> scripts/py/jobs/logs/fill_missing_criticals_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finshed fill_missing_criticals.py && printf "\n" &&

echo $(date): running ensure_empty_summary_and_matrix.sql && printf "\n" &&
psql postgresql://postgres:1122@localhost:5432/hook < scripts/sql/ensure_empty_summary_and_matrix.sql >> scripts/sql/logs/ensure_empty_summary_and_matrix_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finished ensure_empty_summary_and_matrix.sql && printf "\n" &&

echo $(date): running loan_summary.py && printf "\n" &&
python manage.py shell < scripts/py/jobs/exec_loan_summary.py >> scripts/py/jobs/logs/loan_summary_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finished loan_summary.py && printf "\n" &&

echo $(date): running loan_summary_matrix.py && printf "\n" &&
python manage.py shell < scripts/py/jobs/exec_loan_summary_matrix.py >> scripts/py/jobs/logs/loan_summary_matrix_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finished loan_summary_matrix.py && printf "\n" &&

# echo $(date): running set_cheapest_loans.sql && printf "\n" &&
# psql postgresql://postgres:1122@localhost:5432/hook < scripts/sql/set_cheapest_loans.sql >> scripts/sql/logs/set_cheapest_loans_log_$YYYY\_$MM\_$DD.txt &&
# echo $(date): finished set_cheapest_loans.sql && printf "\n" &&

echo $(date): running interest_change_buckets.py && printf "\n" &&
python manage.py shell_plus < scripts/py/jobs/exec_interest_change_buckets.py >> scripts/py/jobs/logs/interest_change_buckets_log_$YYYY\_$MM\_$DD.txt &&
echo $(date): finished interest_change_buckets.py && printf "\n" &&

echo $(date): restarting apache2 server... && printf "\n" &&
sudo systemctl restart apache2 &&
echo $(date): success! printf "\n"
