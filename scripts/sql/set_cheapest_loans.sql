-- This query sets the cheapest tags for each loan in loans_loan table.
update loans_loan set header_type=null where header_type='cheapest';
update loans_loan 
	set header_type='cheapest'
where id in (
	select x.id from
		-- First table is to get bank-wise loan id's for ech tenure-principal bucket
		(select 
			id, loan_type, bank_name, tenure, principal, min(interest) min_interest 
			from loans_loan
			where active=true
			group by 1,2,3,4,5
		) as x
			left join
		-- Second table is to get minimum interest rates for each tenure-principal bucket
		(
			select loan_type, tenure, principal, min(interest) min_interest 
			from loans_loan
			where active=true
			group by 1,2,3
		) as y
		on x.loan_type=y.loan_type
		and x.tenure=y.tenure
		and x.principal=y.principal
	where x.min_interest = y.min_interest
);