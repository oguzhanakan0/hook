#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import pandas as pd
# from loans.views import *
from scripts.py.loan_calc_functions import *
from loans.templatetags.pandas_filters import *
# os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"


# In[68]:


# l1_tenure = 3
# l1_principal = 35000
# l2_tenure = 6
# l2_principal = 45000
# l1 = Loan.objects.filter(tenure__gte=l1_tenure,principal__gte=l1_principal).order_by('tenure','principal').first()
# l2 = Loan.objects.filter(tenure__gte=l2_tenure,principal__gte=l2_principal).order_by('tenure','principal').first()


# In[141]:


def get_principal_comparison_text(l1,l2):
    diff = l1.principal.principal - l2.principal.principal
    no_winner = False
    c = 'gray'
    if diff==0: no_winner = True
    elif diff<0: midtext, c = "az", "red"
    elif diff>0: midtext, c = "fazla", "green"
    else: midtext, c = "NA", "gray"
    if not no_winner:
        text = (# '<span class="display-6 font-weight-bold">Kredi Tutarı:</span><br>'+
                # "A kredisinde, B kredisine göre " +
                '<span style="font-weight:600;color:'+c+'">'+as_currency_plain_short(abs(diff)) + " TL daha " + 
                midtext + " miktarda nakitin olur.</span>" )
                # " (A: "+as_currency_plain_short(l1.principal.principal) +
                # " TL, B: "+as_currency_plain_short(l2.principal.principal)+ " TL)" )
                # '<b>'+as_currency_plain_short(l1.principal.principal) + " TL</b> tutarlı A kredisinde, " + 
                # '<b>'+as_currency_plain_short(l2.principal.principal) + " TL</b> tutarlı B kredisine göre " + )
    else:
        text = "A ve B kredilerinde eşit miktarda nakitin olur. ("+as_currency_plain_short(l1.principal.principal) +")"
    return text, c


# In[147]:


def get_tenure_comparison_text(l1,l2):
    diff = l1.tenure.tenure - l2.tenure.tenure
    no_winner = False
    c = 'gray'
    if diff==0: no_winner = True
    elif diff<0: midtext, c = "az", "green"
    elif diff>0: midtext, c = "fazla", "red"
    else: midtext = "NA"
    if not no_winner:
        text = (# '<span class="display-6 font-weight-bold">Vade:</span><br>'+
                # "A kredisinde, B kredisine göre " +
                '<span style="font-weight:600;color:'+c+'">'+str(abs(diff)) + " ay daha " + 
                midtext + " sayıda ödeme yaparsın.</span>" )
                # " (A: "+str(l1.tenure.tenure) +
                # " ay, B: "+str(l2.tenure.tenure)+ " ay)" )
                # '<b>'+str(l1.tenure.tenure) + " ay</b> vadeli A kredisinde, " + 
                # '<b>'+str(l2.tenure.tenure) + " ay</b> vadeli B kredisine göre " + 
    else:
        text = "A ve B kredilerinde eşit sayıda ödeme yaparsın. ("+str(l1.tenure.tenure) +" ay)"
    return text, c


# In[148]:


def get_total_payment_comparison_text(l1,l2):
    diff = l1.total_payment - l2.total_payment
    no_winner = False
    c = 'gray'
    if diff==0: no_winner = True
    elif diff<0: midtext, c = "az", "green"
    elif diff>0: midtext, c = "fazla", "red"
    else: midtext = "NA"
    if not no_winner:
        text = (# '<span class="display-6 font-weight-bold">Toplam Ödeme:</span><br>'+
                # "A kredisinde, B kredisine göre " +
                '<span style="font-weight:600;color:'+c+'">'+as_currency_plain_short(abs(diff)) + " TL daha " + 
                midtext + " toplam ödeme yaparsın.</span>" )
                # " (A: "+as_currency_plain_short(l1.total_payment) +
                # " TL, B: "+as_currency_plain_short(l2.total_payment)+ " TL)" )
                # '<b>'+as_currency_plain_short(l1.total_payment) + " TL</b> toplam ödemesi olan A kredisinde, " + 
                # '<b>'+as_currency_plain_short(l2.total_payment) + " TL</b> toplam ödemesi olan B kredisine göre " + 
    else:
        text = "A ve B kredilerinde eşit miktarda toplam ödeme yaparsın. (" + as_currency_plain_short(l1.total_payment) + " TL)"
    return text, c


# In[150]:


def get_installment_comparison_text(l1,l2):
    diff = l1.installment - l2.installment
    no_winner = False
    c = 'gray'
    if diff==0: no_winner = True
    elif diff<0: midtext, c = "az", "green"
    elif diff>0: midtext, c = "fazla", "red"
    else: midtext = "NA"
    if not no_winner:
        text = (# '<span class="display-6 font-weight-bold">Aylık Taksit:</span><br>'+
                # "A kredisinde, B kredisine göre " +
                '<span style="font-weight:600;color:'+c+'">'+as_currency_plain_short(abs(diff)) + " TL daha " + 
                midtext + " aylık taksit ödemesi yaparsın.</span>" )
                # " (A: "+as_currency_plain_short(l1.installment) +
                # " TL, B: "+as_currency_plain_short(l2.installment)+ " TL)" )
                # '<b>'+as_currency_plain_short(l1.installment) + " TL</b> aylık taksiti olan A kredisinde, " + 
                # '<b>'+as_currency_plain_short(l2.installment) + " TL</b> aylık taksiti olan B kredisine göre her ay " + 
    else:
        text = "A ve B kredilerinde her ay eşit miktarda ödeme yaparsın. ("+as_currency_plain_short(l1.installment) + " TL)"
    return text, c


# In[151]:


def get_total_interest_comparison_text(l1,l2):
    diff = l1.total_interest - l2.total_interest
    no_winner = False
    c = 'gray'
    if diff==0: no_winner = True
    elif diff<0: midtext, c = "az", "green"
    elif diff>0: midtext, c = "fazla", "red"
    else: midtext = "NA"
    if not no_winner:
        text = (# '<span class="display-6 font-weight-bold">Toplam Faiz:</span><br>'+
                # "A kredisinde, B kredisine göre " +
                '<span style="font-weight:600;color:'+c+'">'+as_currency_plain_short(abs(diff)) + " TL daha " + 
                midtext + " toplam faiz ödemesi yaparsın.</span>" )
                # " (A: "+as_currency_plain_short(l1.total_interest) +
                # " TL, B: "+as_currency_plain_short(l2.total_interest)+ " TL)" )
                # '<b>'+as_currency_plain_short(l1.total_interest) + " TL</b> toplam faizi olan A kredisinde, " + 
                # '<b>'+as_currency_plain_short(l2.total_interest) + " TL</b> toplam faizi olan B kredisine göre " + 
    else:
        text = as_currency_plain_short(l1.total_interest) + " TL toplam faizi olan A ve B kredilerinde toplam eşit miktarda faiz ödersin."
    return text, c


# In[156]:


def compare_loans(l1,l2, l1_principal, l1_tenure, l2_principal, l2_tenure):
    """Wrapper function"""
    l1 = calculate_loan_details(l1, l1_principal, l1_tenure)
    l2 = calculate_loan_details(l2, l2_principal, l2_tenure)
    # comparison_dict:
    c = {
        "principal":get_principal_comparison_text(l1,l2),
        "total_payment":get_total_payment_comparison_text(l1,l2),
        "installment":get_installment_comparison_text(l1,l2),
        "tenure":get_tenure_comparison_text(l1,l2),
        "total_interest":get_total_interest_comparison_text(l1,l2)
    }

    res = f"""
        <div class="row" style="">
            <div class="col-sm-12 align-center"><i>Özetle;</i><br><b>A Kredisinde, B Kredisine göre</b></div>
            <div class="col-sm-6 align-right">
    """
    for item in c.keys():
        if c[item][1]=="green":
            res+= c[item][0] + "<br>"
    
    res+="""</div><div class="col-sm-6 align-left">"""

    for item in c.keys():
        if c[item][1]=="red":
            res+= c[item][0] + "<br>"

    res+="""</div></div>"""
    
    return res, l1, l2


# In[157]:


# loan_comparison(l1,l2)

