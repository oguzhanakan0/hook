#!/usr/bin/env python
# coding: utf-8

#  # START

# In[1]:


import os
import sys
import pandas as pd
import numpy as np
import datetime as dt
from babel.numbers import format_currency
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"


# In[2]:


print(f'interest_change_buckets job started at: {dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')


# In[17]:


print(f'making sure there is no InterestBreakTable exists for today.')
objects = InterestBreakTable.objects.filter(date=dt.date.today())
print(f'found: {len(objects)}, removing...')
objects.delete()
print(f'cool.')


# In[3]:


def as_currency_plain_short(value):
    try:
        return format_currency(value, 'TRY', u'#,##0', locale='tr_TR').split(',')[0]
    except:
        return "-"


# In[4]:


def as_percentage(value):
    try:
        return "%{:.2f}".format(value*100).replace('.',',')
    except:
        return ""


# In[5]:


def add_tenure_suffix_plain(value):
    try:
        return str(int(value)) + " ay"
    except:
        return '-'


# In[6]:


def numIslandsRec(grid, nextLocation, target_val, island_members):
    (x, y) = nextLocation
    grid[x][y] = 0
    island_members.append((x,y))
    if x+1 < len(grid) and grid[x+1][y] == target_val:
        grid, island_members = numIslandsRec(grid, (x+1, y), target_val,island_members)
    if x-1 >= 0 and grid[x-1][y] == target_val:
        grid, island_members = numIslandsRec(grid, (x-1, y), target_val,island_members)
    if y+1 < len(grid[0]) and grid[x][y+1] == target_val:
        grid, island_members = numIslandsRec(grid, (x, y+1), target_val,island_members)
    if y-1 >= 0 and grid[x][y-1] == target_val:
        grid, island_members = numIslandsRec(grid, (x, y-1), target_val,island_members)

    return grid, island_members


# In[7]:


def get_locs(grid, target_val=None):
    if not target_val: return
    num_rows = len(grid)
    num_cols = len(grid[0])
    total_nums = 0
    locs = {}
    for row in range(num_rows):
        for col in range(num_cols):
            if grid[row][col] == target_val:
                grid, island_members = numIslandsRec(grid, (row, col), target_val, [])
                total_nums += 1
                locs[total_nums]={}
                locs[total_nums]['start']=(row,col)
                locs[total_nums]['members']=island_members
    # print(f"total islands for {target_val}==>{total_nums}")
    # print(f"locs: {locs}")
    return locs


# In[8]:


class Cell:
    def __init__(self, val, loc, hidden_val):
        self.val=val
        self.loc=loc
        self.hidden_val=hidden_val
        self.top, self.right, self.bottom, self.left = [True]*4
        self.bg='table-bg-gray' # bootstrap sari renk
        
    @property
    def all_border(self):
        return True if all([self.top,self.right,self.bottom,self.left]) else False

    def set_borders(self, search_locations=None):
        # print(f'setting borders for {self.loc}, search_locations:{search_locations}')
        if (self.loc[0]-1,self.loc[1]) in search_locations:
            self.top=False
        if (self.loc[0],self.loc[1]+1) in search_locations:
            self.right=False
        if (self.loc[0]+1,self.loc[1]) in search_locations:
            self.bottom=False
        if (self.loc[0],self.loc[1]-1) in search_locations:
            self.left=False
        # print(f'borders: {self.top},{self.right},{self.bottom},{self.left}')
        
    def set_bg(self, mi, ma):
        if self.hidden_val==mi:
            self.bg='table-bg-green'
        elif self.hidden_val==ma:
            self.bg='table-bg-red'
        
    def get_borders(self):
        if self.all_border: return 'border'
        borders=[]
        if self.top: borders.append('border-top')
        if self.right: borders.append('border-right')
        if self.bottom: borders.append('border-bottom')
        if self.left: borders.append('border-left')
        return ' '.join(borders)


# In[9]:


def generate_thead(columns):
    res='<thead><tr><th>Tutar&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>&nbsp;&nbsp;Vade&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></th>'
    for col in columns:
        res+=f'<th>{col}</th>'
    res+='</tr></thead>'
    return res


# In[10]:


def generate_tr(label, values, borders,bgs):
    res=f'<tr><th>{label}</th>'
    for val, bor, bg in zip(values,borders,bgs):
        # print(f'adding {val} to the table')
        res+=f'<td class="{bg} {bor}">{as_percentage(val)}</td>'
    res+=f'</tr>'
    return res


# In[11]:


def generate_table(df, index, columns):
    res="""<section class="cid-s3nMbhVkww"><div class="container scroll"><table class="table equal-width display-8" cellspacing="0" id="interest_breakdown" show="summary">"""
    res+=generate_thead(columns)+'<tbody>'
    for x in sorted(df.x.unique()):
        res+=generate_tr(index[x],df[df.x==x].val,df[df.x==x].border,df[df.x==x].bg)
    res+='</tbody></table></div></section>'
    return res


# In[12]:


def generate_and_save(bank, loan_type, criticals):
    try:
        ################# first we create a 'df' that is extracted from loans table for a bank and loan_type.
        res = []
        criticals = criticals.filter(loan_type=loan_type).order_by('principal','tenure')
        for crit in criticals:
            loan = Loan.objects.filter(tenure__gte=crit.tenure,principal__gte=crit.principal,
                                       bank=bank,loan_type=loan_type,active=True)\
                   .order_by('tenure','principal').first()
            if loan:
                res.append((crit.principal.principal,crit.tenure.tenure,loan.interest))
            # else:
                # print(f"no loan found for principal: {crit.principal} and tenure: {crit.tenure} for bank: {bank.name}")
        if len(res)==0: print(f"{loan_type} - {bank}: No loan found. breaking."); return
        df = pd.DataFrame(res,columns=['principal','tenure','interest'])
        df = df.pivot(index='principal',columns='tenure',values='interest')
        ################# we set index while removing the min
        df2 = df.reset_index().copy()
        df2['min_principal'] = df2.principal.shift(1,fill_value=0)
        index = [(as_currency_plain_short(e+1)+' - '+as_currency_plain_short(f))+' TL'                  for e,f in zip(df2.min_principal,df2.principal)][1:]
        del df2
        ################# we set columns while removing the min
        df2 = df.copy()
        cols = df2.columns.tolist()
        colsshift = ([e for e in cols])[:-1]
        for i in range(1,len(colsshift)): colsshift[i]+=1
        cols=cols[1:]
        columns = [str(e)+'<br>-<br>'+str(f) for e,f in zip(colsshift,cols)]
        del df2, cols, colsshift
        ################# we remove 1st row & column
        df = df.iloc[1:,1:]
        df.index = index
        df.columns = columns
        ################# we build the grid from values
        grid = df.values.tolist()
        target_vals = np.unique(grid)
        if len(target_vals)<=1: print(f"{loan_type} - {bank}: No interest change over principals/tenures. breaking."); return
        ################# we prepare table_dict (where islands will go in)
        table_dict = {}
        for target_val in target_vals:
            table_dict[target_val]=get_locs(grid, target_val)
        ################# we find islands and populate table_dict
        grid = df.values.tolist() # this is duplicate FOR A REASON MF
        cells = []
        mi, ma = np.min(df.values.tolist()), np.max(df.values.tolist())
        for interest in table_dict:
            this_dict = table_dict[interest]
            for island in this_dict:
                island_dct = this_dict[island]
                for loc in island_dct['members']:
                    cell = Cell(val='',loc=loc,hidden_val=interest)
                    if loc==island_dct['start']: cell.val=interest
                    cell.set_borders(search_locations=island_dct['members'])
                    cell.set_bg(mi,ma)
                    cells.append(cell)
        ################# we build df that is crucial for the final HTML table
        table_info = []
        for cell in cells:
            table_info.append([cell.val,cell.loc[0],cell.loc[1],cell.get_borders(),cell.bg])
        final_df = pd.DataFrame(table_info,columns=['val','x','y','border','bg'])
        final_df.sort_values(['x','y'],inplace=True)
        del cells, table_info, grid, target_vals, table_dict
        ################# we generate table and save to db
        table_html = generate_table(final_df, index, columns)
        InterestBreakTable(loan_type=loan_type,bank=bank,table_html=table_html).save()
        print(f'{loan_type} - {bank}: saved')
    except:
        print(f"{loan_type} - {bank}: Unexpected error:", sys.exc_info()[0])


# In[13]:


banks = Bank.objects.all()
loan_types = LoanType.objects.all()
criticals = Critical.objects.all()


# In[14]:


for bank in banks:
    for loan_type in loan_types:
        generate_and_save(bank, loan_type, criticals)


# In[15]:


print(f'interest_change_buckets job ended at: {dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')


# #  END
