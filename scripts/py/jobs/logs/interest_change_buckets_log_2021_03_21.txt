BASE_DIR: /hook
System date: 2021-03-21
# Shell Plus Model Imports
from blog.models import BlogCategory, BlogPage, BlogPageTag, FormField, FormPage, LandingPage, PostPage
from taggit.models import Tag, TaggedItem
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
from django.contrib.sessions.models import Session
from loans.models import Action, Bank, Critical, InstallmentType, Insurance, InterestBreakTable, Link, LinkLog, Loan, LoanDetail, LoanDetailLog, LoanHeader, LoanLog, LoanMatrix, LoanMatrixTable, LoanSummary, LoanType, Principal, Program, Tenure
from scraping.models import AlertLog, AlertType, ProductCampaign, Result, ScrapeLog, Source
from seo.models import FAQItem
from userlogs.models import Application, BasitKrediHesaplama, ErkenKapamaAraOdemeHesaplama, ErkenOdemeCezasiHesaplama, GecikmeFaiziHesaplama, KKNakitAvansHesaplama, KrediPesinatHesaplama, KrediVergisiHesaplama, KrediYapilandirmaHesaplama, LoanDetailRequest, LoanRequest, RequestSource, Save, SaveType, Subscription, SubscriptionType
from wagtail.contrib.forms.models import FormSubmission
from wagtail.contrib.redirects.models import Redirect
from wagtail.core.models import Collection, CollectionViewRestriction, GroupApprovalTask, GroupCollectionPermission, GroupPagePermission, Page, PageLogEntry, PageRevision, PageViewRestriction, Site, Task, TaskState, Workflow, WorkflowPage, WorkflowState, WorkflowTask
from wagtail.documents.models import Document
from wagtail.embeds.models import Embed
from wagtail.images.models import Image, Rendition, UploadedImage
from wagtail.search.models import Query, QueryDailyHits
from wagtail.users.models import UserProfile
# Shell Plus Django Imports
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import Avg, Case, Count, F, Max, Min, Prefetch, Q, Sum, When
from django.utils import timezone
from django.urls import reverse
from django.db.models import Exists, OuterRef, Subquery
>>> interest_change_buckets job started at: 2021-03-21 01:12:53
making sure there is no InterestBreakTable exists for today.
found: 0, removing...
cool.
Taşıt Kredisi 2. El - TurkishBank: saved
Taşıt Kredisi 0 Km - TurkishBank: saved
Konut Kredisi - TurkishBank: No loan found. breaking.
İhtiyaç Kredisi - TurkishBank: No loan found. breaking.
Taşıt Kredisi 2. El - Enpara: No loan found. breaking.
Taşıt Kredisi 0 Km - Enpara: No loan found. breaking.
Konut Kredisi - Enpara: No loan found. breaking.
İhtiyaç Kredisi - Enpara: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - ICBC: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - ICBC: No loan found. breaking.
Konut Kredisi - ICBC: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - ICBC: No loan found. breaking.
Taşıt Kredisi 2. El - HSBC: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - HSBC: No loan found. breaking.
Konut Kredisi - HSBC: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - HSBC: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Burgan Bank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Burgan Bank: No interest change over principals/tenures. breaking.
Konut Kredisi - Burgan Bank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Burgan Bank: No loan found. breaking.
Taşıt Kredisi 2. El - Fibabanka: saved
Taşıt Kredisi 0 Km - Fibabanka: saved
Konut Kredisi - Fibabanka: saved
İhtiyaç Kredisi - Fibabanka: No loan found. breaking.
Taşıt Kredisi 2. El - Odeabank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Odeabank: No interest change over principals/tenures. breaking.
Konut Kredisi - Odeabank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Odeabank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Şekerbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Şekerbank: No loan found. breaking.
Konut Kredisi - Şekerbank: saved
İhtiyaç Kredisi - Şekerbank: saved
Taşıt Kredisi 2. El - Anadolubank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Anadolubank: No loan found. breaking.
Konut Kredisi - Anadolubank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Anadolubank: No loan found. breaking.
Taşıt Kredisi 2. El - Alternatif Bank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Alternatif Bank: No interest change over principals/tenures. breaking.
Konut Kredisi - Alternatif Bank: saved
İhtiyaç Kredisi - Alternatif Bank: No loan found. breaking.
Taşıt Kredisi 2. El - Ziraat Bankası: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Ziraat Bankası: No interest change over principals/tenures. breaking.
Konut Kredisi - Ziraat Bankası: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Ziraat Bankası: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - TEB: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - TEB: No loan found. breaking.
Konut Kredisi - TEB: saved
İhtiyaç Kredisi - TEB: No loan found. breaking.
Taşıt Kredisi 2. El - İş Bankası: saved
Taşıt Kredisi 0 Km - İş Bankası: saved
Konut Kredisi - İş Bankası: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - İş Bankası: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Halkbank: saved
Taşıt Kredisi 0 Km - Halkbank: No interest change over principals/tenures. breaking.
Konut Kredisi - Halkbank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Halkbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Vakıfbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Vakıfbank: No interest change over principals/tenures. breaking.
Konut Kredisi - Vakıfbank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Vakıfbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - QNB Finansbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - QNB Finansbank: No interest change over principals/tenures. breaking.
Konut Kredisi - QNB Finansbank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - QNB Finansbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Denizbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Denizbank: No interest change over principals/tenures. breaking.
Konut Kredisi - Denizbank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Denizbank: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Akbank: saved
Taşıt Kredisi 0 Km - Akbank: saved
Konut Kredisi - Akbank: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Akbank: saved
Taşıt Kredisi 2. El - Yapı Kredi: saved
Taşıt Kredisi 0 Km - Yapı Kredi: saved
Konut Kredisi - Yapı Kredi: saved
İhtiyaç Kredisi - Yapı Kredi: saved
Taşıt Kredisi 2. El - Garanti BBVA: saved
Taşıt Kredisi 0 Km - Garanti BBVA: saved
Konut Kredisi - Garanti BBVA: saved
İhtiyaç Kredisi - Garanti BBVA: saved
Taşıt Kredisi 2. El - ING: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - ING: No loan found. breaking.
Konut Kredisi - ING: saved
İhtiyaç Kredisi - ING: saved
Taşıt Kredisi 2. El - Ziraat Katılım: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Ziraat Katılım: No interest change over principals/tenures. breaking.
Konut Kredisi - Ziraat Katılım: No loan found. breaking.
İhtiyaç Kredisi - Ziraat Katılım: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Vakıf Katılım: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Vakıf Katılım: No interest change over principals/tenures. breaking.
Konut Kredisi - Vakıf Katılım: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Vakıf Katılım: No loan found. breaking.
Taşıt Kredisi 2. El - Kuveyt Türk: No interest change over principals/tenures. breaking.
Taşıt Kredisi 0 Km - Kuveyt Türk: No loan found. breaking.
Konut Kredisi - Kuveyt Türk: No interest change over principals/tenures. breaking.
İhtiyaç Kredisi - Kuveyt Türk: No interest change over principals/tenures. breaking.
Taşıt Kredisi 2. El - Albaraka Türk: No loan found. breaking.
Taşıt Kredisi 0 Km - Albaraka Türk: No loan found. breaking.
Konut Kredisi - Albaraka Türk: No loan found. breaking.
İhtiyaç Kredisi - Albaraka Türk: No loan found. breaking.
Taşıt Kredisi 2. El - Türkiye Finans: No loan found. breaking.
Taşıt Kredisi 0 Km - Türkiye Finans: No loan found. breaking.
Konut Kredisi - Türkiye Finans: No loan found. breaking.
İhtiyaç Kredisi - Türkiye Finans: No loan found. breaking.
interest_change_buckets job ended at: 2021-03-21 01:13:43
>>> 