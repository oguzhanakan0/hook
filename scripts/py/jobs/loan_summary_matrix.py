#!/usr/bin/env python
# coding: utf-8
# # Loan Matrix Routine
# #### This script runs everyday and inserts summary of loans_loan table into loans_loanmatrix table.
import datetime
print(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")+": loan_summary_matrix.py started")
print("===================================")
import django
django.setup()
import os
import pandas as pd
from loans.models import *
from datetime import date as dt

def generate_thead(tenures_,loan_type):
    from loans.models import Critical, LoanType
    def add_tenure_suffix(value):
        try:
            if int(value) % 12 == 0:
                return str(int(int(value)/12)) +" yıl"
            else:
                return str(int(value)) + " ay"
        except:
            return ''
    def th(value, lt):
        # print(loan_type)
        return '<th class="mbr-semibold display-7'+('' if any(x.critical_mobile for x in Critical.objects.filter(loan_type=lt,tenure=value)) else ' mobile-hidden')+'">'+add_tenure_suffix(value)+'</th>'
    return_str = ""
    return_str += """<th class="mbr-semibold display-7 border-right">Tutar \ Vade</th>""" # first column
    lt = LoanType.objects.get(loan_type_long=loan_type).loan_type
    for tenure_ in tenures_:
        return_str += th(tenure_, lt)
    return return_str

def generate_tbody(principals_,tenures_,loan_matrix_,loan_type):
    from loans.models import Critical
    
    def tr(value):
        return "<tr>"+str(value)+"</tr>"
    
    def as_currency_plain_short(value):
        from babel.numbers import format_currency
        try:
            return format_currency(int(value), 'TRY', u'#,##0', locale='tr_TR').split(',')[0] + ' TL'
        except:
            return "-"
    
    def td_index(value,loan_type):
        # print(f"loan_type: {str(type(loan_type))}, principal:{value}, td_index returning :")
        res = '<td class = "mbr-semibold display-7 border-right'+('' if any(x.critical_mobile for x in Critical.objects.filter(loan_type=loan_type,principal=value)) else ' mobile-hidden')+'">'
        res += as_currency_plain_short(value)
        res += "</td>"
        return res
    
    def as_percentage(value):
        try:
            return "%{:.2f}".format(value*100).replace('.',',')
        except:
            return "-"
    
    def generate_icon_with_tooltip(loan, principal, tenure):
        def as_currency_plain(value):
            from babel.numbers import format_currency
            try:
                return format_currency(value, 'TRY', u'#,##0.00 ¤', locale='tr_TR')
            except:
                return "-"
        from loans.views import calculate_loan_details, calculate_installment
        loan = calculate_loan_details(loan, principal, tenure)
        # /kredi-tipi=GPL&banka-adi=qnb-finansbank&tutar=10000&vade=12
        return f"""
        <a class="hasTooltip" href="/kredi/detay/kredi-tipi={str(loan.loan_type.loan_type)}&banka-adi={str(loan.bank.slug)}&tutar={str(principal)}&vade={str(tenure)}/" style="color:var(--primary-text-color) !important;">
            <img alt="{loan.bank.name}" src="/media/{str(loan.bank.icon.file)}" width="25" height="25">
            <div class="someTooltip">
                <div class="d-flex flex-column justify-content-between" style="min-width:250px;">
                    <div class="p-2 d-flex flex-row justify-content-center border-bottom align-items-center">
                        <img alt="{loan.bank.name}" src="/media/{str(loan.bank.icon.file)}" width="25" height="25">
                    <span class="display-7"> Ödeme Detayları</span>
                    </div>
                    <div class="p-2 d-flex flex-row align-items-end justify-content-between text-white tooltip-bg-primary">
                        <span class="display-8">Kredi Tutarı</span>
                        <span class="display-7">{as_currency_plain(principal)}</span>
                    </div>
                    <div class="p-2 d-flex flex-row align-items-end justify-content-between tooltip-bg-secondary">
                        <span class="display-8">Toplam Faiz ve Vergiler</span>
                        <span class="display-7">{as_currency_plain(loan.total_interest)}</span>
                    </div>
                    <div class="p-2 d-flex flex-row align-items-end justify-content-between text-white tooltip-bg-primary">
                        <span class="display-8">Dosya Masrafı</span>
                        <span class="display-7">{as_currency_plain(loan.allocation_fee)}</span>
                    </div>
                    <div class="p-2 d-flex flex-row align-items-end justify-content-between tooltip-bg-secondary border-bottom">
                        <span class="display-8">Toplam Ödeme</span>
                        <span class="display-7">{as_currency_plain(loan.total_payment)}</span>
                    </div>
                    <div class="p-2 d-flex flex-row align-items-end justify-content-center">
                    <span class="display-7">
                        <u>Krediye git</u>
                    </span>
                    </div>
                </div>
            </div>
        </a>
        """
    
    def td_cell(lm_item,principal_,tenure_, loan_type):
        # print(f"loan_type: {loan_type}, principal:{principal_}, tenure:{tenure_}, td_cell returning :")
        res = '<td class = "display-7 font-weight-bold'+('' if Critical.objects.get(loan_type=loan_type,principal=principal_,tenure=tenure_).critical_mobile else ' mobile-hidden')+'">'
        res += as_percentage(lm_item.min_interest)
        res += " "
        # print(res)
        icon_list = [generate_icon_with_tooltip(e,principal_,tenure_) for e in [lm_item.loan_1, lm_item.loan_2, lm_item.loan_3] if e!=None ]
        res += " ".join(icon_list)
        res += "</td>"
        return res
    
    def get_lm_item(principal_,tenure_,loan_matrix_):
        res = loan_matrix_.filter(principal=principal_,tenure=tenure_)
        if len(res) == 1:
            return res[0]
        else:
            raise TypeError('This is impossible. Please make sure that there is only 1 row for this tenure & principal & loan_type. Number of loan_matrixes found:'+str(len(res)))
    
    res = ""
    for principal_ in principals_:
        this_row = ""
        this_row += td_index(principal_,loan_type)
        for tenure_ in tenures_:
            lm_item = get_lm_item(principal_,tenure_,loan_matrix_)
            this_row += td_cell(lm_item,principal_, tenure_, loan_type)
        res += tr(this_row)
    return res

today = dt.today()
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
# criticals = Critical.objects.filter(loan_type='GPL')
# tenures = [e.tenure for e in criticals]
# principals = [e.principal for e in criticals]
matrices = [] # unused, may be used for debugging
# 1. Get all loan types.
loan_types = LoanType.objects.all()
# 2. For each loan type;
for loan_type in loan_types:
    # 2.1. Get all possible tenures..
    criticals = Critical.objects.filter(loan_type=loan_type)
    tenures = [e.tenure for e in criticals]
    # 2.2. .. and principals.
    principals = [e.principal for e in criticals]
    dmatrix = dict()
    # 2.3. For each tenure;
    # 2.3.1. For each principal;
    for tenure, principal in zip(tenures,principals):
        dmatrix[tenure] = dict()
        # 2.3.1.1. Filter loans according to loan_type, tenure, and principal. Here, the important thing is we should bring all
        #          banks' loans so that if two of the banks offer same interest, we will not miss it.
        loans = list(Loan.objects.filter(tenure__gte=tenure, principal__gte=principal, loan_type=loan_type,active=True).order_by('bank_id','tenure','principal','interest').distinct('bank_id'))
        # 2.3.1.2. Following algorithm determines which loans are with minimum interest rate.
        loans_cp = loans.copy()
        min_interest = None;
        min_loan = Loan.objects.filter(tenure__gte=tenure, principal__gte=principal, loan_type=loan_type,active=True).order_by('tenure','principal','interest').first()
        if min_loan: min_interest = min_loan.interest
        # print("min interest for tenure: "+str(tenure.tenure)+" and principal: "+str(principal.principal))
        # print(min_interest)
        if not len(loans)==0:
            for loan in loans_cp:
                # print("checking "+str(loan)+" with interest: "+str(loan.interest)) 
                if loan.interest > min_interest:
                    # print("loan removed from loans")
                    loans.remove(loan)
        dmatrix[tenure][principal] = loans
        # 2.3.1.3. Create a LoanMatrix object and save it.
        lm = LoanMatrix()
        lm.loan_type = loan_type
        lm.tenure = tenure
        lm.principal = principal
        lm.min_interest = min_interest
        for i in range(1,(len(loans)+1)):
            exec("lm.loan_"+str(i)+"=loans[i-1]")
        lm.save()
        print("saved for loan_type: "+str(loan_type)+" - tenure: "+str(tenure)+" - principal: "+str(principal))
    matrices.append(dmatrix)
    loan_matrix_ = LoanMatrix.objects.filter(date=today,loan_type=loan_type)
    tenures_ = loan_matrix_.values_list('tenure', flat=True).distinct().order_by('tenure')
    principals_ = loan_matrix_.values_list('principal', flat=True).distinct().order_by('principal')
    html_string = ""
    html_string += """<table class="table align-center" id = loan_matrix_"""+str(loan_type)+">"
    html_string += """<thead>"""
    html_string += generate_thead(tenures_,loan_type)
    html_string += """</thead>"""
    html_string += """<tbody>"""
    html_string += generate_tbody(principals_,tenures_,loan_matrix_,loan_type)
    html_string += "</tbody></table>"
    lmt = LoanMatrixTable()
    lmt.loan_type = loan_type
    lmt.table_html = html_string
    lmt.save()
    print("saved for "+str(loan_type))
print("===================================")
print(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")+": loan_summary_matrix.py ended")
