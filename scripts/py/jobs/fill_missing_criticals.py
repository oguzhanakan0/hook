#!/usr/bin/env python
# coding: utf-8

# # START

# In[9]:


import os
import pandas as pd
import datetime as dt
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"


# In[10]:


print(f'fill_missing_criticals job started at: {dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')


# In[31]:


def remove_if_exists(slug,principal,tenure):
    try:
        print('duplicate(s) found! deleting..')
        Loan.objects.filter(slug=slug,principal=principal,tenure=tenure).delete()
    except:
        pass


# In[30]:


def insert_missing_loan(bank,loan_type,principal,tenure,criticals):
    upper_buckets = criticals.filter(principal__gte=principal, tenure__gte=tenure)
    for upper_bucket in upper_buckets:
        # print(f"checking upper bucket: ({upper_bucket.principal.principal:6},{upper_bucket.tenure.tenure:3})")
        try:
            loan = Loan.objects.get(bank=bank,loan_type=loan_type,active=True,
                                    principal=upper_bucket.principal,tenure=upper_bucket.tenure)
            remove_if_exists(loan.slug,principal,tenure)
            # print('loan found!')
            loan.pk = None
            loan.principal = principal
            loan.tenure = tenure
            loan.save()
            print(f'SUCCESS INSERTING LOAN: used ({upper_bucket.principal.principal:6},{upper_bucket.tenure.tenure:3}).');print('')
            return True
        except:
            # print(f"Unexpected error: {str(e)}")
            pass
    print('ERROR INSERTING LOAN: none found. breaking.');print('')
    return False


# In[35]:


def insert_missing_loans(bank, loan_type, criticals):
    print("# "+loan_type.loan_type)
    # 1. Filter loans w.r.to loan_type and bank
    loans = Loan.objects.filter(loan_type=loan_type,bank=bank,active=True)
    # 2. If there is no loan, break
    if len(loans)==0: print(f"{bank} DOES NOT HAVE ANY active {loan_type.loan_type}");print(''); return;
    # if len(loans)==len(criticals[loan_type]): print(f"{bank} HAS a record for EVERY critical bucket of {loan_type.loan_type}");print(''); return;

    # 3. For each critical in this loan_type
    i=0
    for critical in criticals[loan_type]:
        if i%5==0: print('')
        i+=1
        print(f'({critical.principal.principal:6},{critical.tenure.tenure:3}):', end=' ')
        loans_crit = loans.filter(principal=critical.principal,tenure=critical.tenure)
        # 4. If there exist in this critical
        if len(loans_crit)==1:
            print('OK',end='\t ')
            continue
            # print(f"{bank} HAS a record for {loan_type.loan_type, critical.principal.principal, critical.tenure.tenure}")

        # 5. If the critical bucket is missing, we try to insert a new one using 
        elif len(loans_crit)==0:
            i=0;
            print('NOT OK - see below')
            print('');
            print(f"{bank} DOES NOT HAVE a record for ({critical.principal.principal:6},{critical.tenure.tenure:3})")
            insert_missing_loan(bank,loan_type,critical.principal,critical.tenure, criticals[loan_type])

        # 6. THIS IS A VERY CRITICAL ERROR: IT SHOULD NOT HAPPEN BY DESIGN,
        #    BUT IF IT HAPPENS, PLEASE LET OGUZ KNOW ABOUT IT IMMEDIATELY.\
        else:
            i=0;
            print('');
            print(f"{bank} HAS MULTIPLE records for ({critical.principal.principal:6},{critical.tenure.tenure:3}) ({len(loans_crit)})")
            print("REMOVING duplicates..")
            try:
                loans_ordered = loans_crit.order_by('pk')
                for i in range(1,len(loans_crit)):
                    l = loans_ordered[i]
                    l.delete()
                print("SUCCESS")
            except Exception as e:
                print("UNEXPECTED ERROR: "+str(e))
    return


# In[36]:


def printfancy(this):
    print('')
    print("#"*(len(str(this))+8))
    print(f"### {this} ###")
    print("#"*(len(str(this))+8))


# In[37]:


banks = Bank.objects.all()
loan_types = LoanType.objects.all()

criticals = {}
for lt in loan_types:
    criticals[lt]=Critical.objects.filter(loan_type=lt).order_by('principal','tenure')

for bank in banks:
    printfancy(bank)
    for loan_type in loan_types:
        try:
            insert_missing_loans(bank,loan_type,criticals)
        except Exception as e:
            print(f'UNEXPECTED ERROR: {str(e)}')


# In[7]:


print(f'fill_missing_criticals job ended at: {dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')


# # END 
