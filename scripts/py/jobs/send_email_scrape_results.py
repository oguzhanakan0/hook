#!/usr/bin/env python
# coding: utf-8

# In[10]:


# Python code to illustrate Sending mail with attachments 
# from your Gmail account
# libraries to be imported 
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import os
os.chdir("/hook")
import datetime


# In[13]:


fromaddr = "oakan13@ku.edu.tr"
toaddr = "oguzhan.akan17@gmail.com, oakan13@ku.edu.tr, emredanaci22@gmail.com, safakenes@gmail.com, aysenurgulnarboun@gmail.com"
gmailpass = "evtttklkxcdrrzmi"
file_ext = datetime.date.today().strftime("_%Y_%m_%d.txt")
attach_these = [
    "scripts/scrape/1.0.1/logs/scrape_job_log"+file_ext,
    "scripts/py/jobs/logs/transfer_scraped_data_log"+file_ext,
    "scripts/py/jobs/logs/loan_summary_log"+file_ext,
    "scripts/py/jobs/logs/loan_summary_matrix_log"+file_ext,
    "scripts/sql/logs/deactivate_all_loans_log"+file_ext,
    "scripts/sql/logs/set_cheapest_loans_log"+file_ext,
    "scripts/sql/logs/ensure_empty_summary_and_matrix_log"+file_ext,
]
# instance of MIMEMultipart 
msg = MIMEMultipart()
# storing the senders email address   
msg['From'] = fromaddr 
# storing the receivers email address  
msg['To'] = toaddr 
# storing the subject
msg['Subject'] = "DigiKredi - Daily Job End - scrape_job_run.bat - "+datetime.date.today().strftime("%d/%m/%Y")
# string to store the body of the mail
try:
    job_log = open('scripts/shell_jobs/logs/scrape_job_bat_log'+file_ext).read()
except:
    job_log = 'scripts/shell_jobs/logs/scrape_job_bat_log'+file_ext+ " NOT FOUND"
body = f"""
    <div style="border:1px solid black;border-radius:12px;padding:12px;margin:12px;max-width:600px;">
    <p>scrape_job_run.bat finished. Attachments should include the following:
    <ul>
        <li>scripts/scrape/1.0.1/logs/scrape_job_log</li>
        <li>scripts/py/jobs/logs/data_transfer_log</li>
        <li>scripts/py/jobs/logs/loan_summary_log</li>
        <li>scripts/py/jobs/logs/loan_summary_matrix_log</li>
        <li>scripts/sql/jobs/logs/deactivate_all_loans_log</li>
        <li>scripts/sql/jobs/logs/set_cheapest_loans_log</li>
        <li>scripts/sql/jobs/logs/ensure_empty_summary_and_matrix_log</li>
    </ul>
    </p>
    <p>scrape_job_bat_log:
    {job_log}
    </p>
    </div>
"""
# attach the body with the msg instance 
msg.attach(MIMEText(body, 'html')) 
# open the file to be sent
for filename in attach_these:
    try:
        attachment = open(filename, "rb") 
        # instance of MIMEBase and named as p 
        p = MIMEBase('application', 'octet-stream') 
        # To change the payload into encoded form 
        p.set_payload((attachment).read()) 
        # encode into base64 
        encoders.encode_base64(p) 
        p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
        # attach the instance 'p' to instance 'msg' 
        msg.attach(p)
    except:
        print('cant attach: '+filename)
        pass

s = smtplib.SMTP('smtp.gmail.com', 587)
# start TLS for security
s.starttls()
# Authentication
s.login(fromaddr, gmailpass)
# Converts the Multipart msg into a string
text = msg.as_string()
# sending the mail
s.sendmail(fromaddr, toaddr.split(','), text)
# terminating the session
s.quit()
