#!/usr/bin/env python
# coding: utf-8
"""
run script via:
> python manage.py shell < scripts/py/jobs/transfer_scraped_data.py >> scripts/scrape/1.0.1/data/data_transfer_log/data_transfer_log.txt 
"""
print('== job start ==')

class TransferObject:
    def __init__(self):
        pass
    # ### Functions
    def save_loan(self,row):
        from loans.models import Loan, LoanType, Principal, Tenure, Program, Bank
        from scraping.models import Result, Source, AlertLog, AlertType, ScrapeLog
        from django.core.exceptions import ObjectDoesNotExist
        from unicode_tr import unicode_tr
        from unicode_tr.extras import slugify
        from django.contrib import auth
        # import datetime as dt
        from django.utils import timezone
        import pytz
        User = auth.get_user_model()
        loan = Loan()
        try:
            loan = Loan.objects.get(bank=Bank.objects.get(scrape_id=row.bank).name,name=unicode_tr(row.campaign).title(),tenure=Tenure.objects.get(tenure=row.tenure),principal=Principal.objects.get(principal=row.principal))
            print(str(row.name)+": loan object exists with id="+str(loan.id)+". updating.")
            if not round(loan.interest,4) == round(row.interest,4):
                loan.last_interest = loan.interest
                loan.last_interest_change = timezone.now()
                loan.interest = round(row.interest,4)
                loan.active = True
                loan.save()
                return 'update-interest'
            else:
                print(str(row.name)+": interest is still the same. passing update.")
                loan.active = True
                loan.save()
                return 'no-touch'
        except ObjectDoesNotExist:
            print(str(row.name)+": loan object for name='"+unicode_tr(row.campaign).title()+"', tenure="+str(Tenure.objects.get(tenure=row.tenure))+", principal="+str(Principal.objects.get(principal=row.principal))+" does not exist. inserting.")
            try:
                loan.name        = unicode_tr(row.campaign).title()
                loan.principal   = Principal.objects.get(principal=row.principal)
                loan.tenure      = Tenure.objects.get(tenure=row.tenure)
                loan.loan_type   = LoanType.objects.get(loan_type=row['product'])
                loan.bank        = Bank.objects.get(scrape_id=row.bank)
                loan.slug        = slugify(str(loan.bank)+"-"+row.campaign)
                loan.interest    = round(row.interest,4)
                loan.is_base     = True if (row.principal==0 or row.tenure==0) else False
                loan.upd_user    = User.objects.get(username='admin')
                loan.upd_program = Program.objects.get(program='python@transfer_scraped_data.py')
                loan.active      = True
                loan.save()
                print(str(row.name)+": loan saved")
                return 'insert'
            except Exception as e:
                print(str(row.name)+": failed")
                print(str(row.name)+": "+str(e))
                return 'save-failed'
                
    def save_scrapelog(self,row):
        from loans.models import Loan, LoanType, Principal, Tenure, Program, Bank
        from scraping.models import Result, Source, AlertLog, AlertType, ScrapeLog
        from django.contrib import auth
        User = auth.get_user_model()
        scrapelog = ScrapeLog()
        try:
            try: scrapelog.bank        = Bank.objects.get(scrape_id=row.bank)
            except: scrapelog.source   = Source.objects.get(source=row.source)
            scrapelog.loan_type   = LoanType.objects.get(loan_type=row['product'])
            scrapelog.result      = Result.objects.get(result=row.status)
            scrapelog.upd_user    = User.objects.get(username='admin')
            scrapelog.upd_program = Program.objects.get(program='python@transfer_scraped_data.py')
            scrapelog.save()
            print(str(row.name)+": scrapelog saved")
        except Exception as e:
            print(str(row.name)+": failed")
            print(str(row.name)+": "+str(e))
            
    def save_alertlog(self,row):
        from loans.models import Loan, LoanType, Principal, Tenure, Program, Bank
        from scraping.models import Result, Source, AlertLog, AlertType, ScrapeLog
        from django.contrib import auth
        User = auth.get_user_model()
        alertlog = AlertLog()
        try:
            alertlog.alert_type  = AlertType.objects.get(alert_type=row.alert_id)
            try: alertlog.bank        = Bank.objects.get(scrape_id=row.bank)
            except: alertlog.source   = Source.objects.get(source=row.source)
            alertlog.loan_type   = LoanType.objects.get(loan_type=row['product'])
            alertlog.upd_user    = User.objects.get(username='admin')
            alertlog.upd_program = Program.objects.get(program='python@transfer_scraped_data.py')
            alertlog.save()
            print(str(row.name)+": alertlog saved")
        except Exception as e:
            print(str(row.name)+": failed")
            print(str(row.name)+": "+str(e))

    def summarize_loans(self,df):
        print("> shape = "+str(df.shape))
        print("> unique bank # = "+str(len(df.bank.unique())))

    # ### Import Loans Data
    def transfer_data(self):
        import django
        django.setup()
        import pandas as pd
        import os
        import datetime
        sys.path.append("/hook")
        from unicode_tr import unicode_tr
        from unicode_tr.extras import slugify
        from django.core.exceptions import ObjectDoesNotExist
        from django.contrib import auth
        from loans.models import Loan, LoanType, Principal, Tenure, Program, Bank
        from scraping.models import Result, Source, AlertLog, AlertType, ScrapeLog
        import collections
        os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
        scrape_version = '1.0.1'
        hook_path = '/hook/'
        # hook_path = '/Users/oguzhanakan/Desktop/hook/'
        os.chdir(hook_path)
        loans_data_path = 'scripts/scrape/'+scrape_version+'/data/'+datetime.date.today().strftime('%Y%m%d')+'_loans.pkl'
        scrapelog_data_path = 'scripts/scrape/'+scrape_version+'/data/'+datetime.date.today().strftime('%Y%m%d')+'_scrapelog.pkl'
        alertlog_data_path = 'scripts/scrape/'+scrape_version+'/data/'+datetime.date.today().strftime('%Y%m%d')+'_alertlog.pkl'
        User = auth.get_user_model()

        s_loans = pd.read_pickle(hook_path+loans_data_path)
        s_scrapelog = pd.read_pickle(hook_path+scrapelog_data_path)
        s_alertlog = pd.read_pickle(hook_path+alertlog_data_path)

        s_loans.reset_index(drop=True,inplace=True)
        s_scrapelog.reset_index(drop=True,inplace=True)
        s_alertlog.reset_index(drop=True,inplace=True)

        # ### Save Loans
        print("="*40)
        print("> date: "+datetime.date.today().strftime('%Y-%m-%d'))
        self.summarize_loans(s_loans)
        print("> loans data insertion started.")
        res=[]
        for row in s_loans.index:
            r = self.save_loan(s_loans.loc[row])
            res.append(r)
        counter=collections.Counter(res)
        print("> loans data insertion summary:")
        print(counter)
        print("> loans data insertion finished.")

        print("> scrapelog insertion started.")
        for row in s_scrapelog.index:
            self.save_scrapelog(s_scrapelog.loc[row])
        print("> scrapelog insertion finished.")

        print("> alertlog insertion started.")
        for row in s_alertlog.index:
            self.save_alertlog(s_alertlog.loc[row])
        print("> alertlog insertion finished.")
        print('== job end ==')


to = TransferObject()
to.transfer_data()