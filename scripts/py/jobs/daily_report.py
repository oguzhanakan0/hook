#!/usr/bin/env python
# coding: utf-8

# # START

# In[1]:


import os
import pandas as pd
from datetime import date as dt
from datetime import datetime as dtm
from datetime import timedelta
import pytz
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"


# In[2]:


HOOK_HOME = '/Users/oguzhanakan/Desktop/hook/'
HOOK_HOME = '/hook/'
PATH_TO_CSS = 'scripts/reports/css/reporting.css'
PATH_TO_REPORTS = 'scripts/reports/'
os.chdir(HOOK_HOME)
N = 7


# In[3]:


end = dtm.today().replace(tzinfo=pytz.UTC)
start = end+timedelta(days=-N)


# ### Generate Dates Between [Start,End]

# In[4]:


dates=[]
for i in range((end-start).days): dates.append((start + timedelta(days=i)).strftime('%Y-%m-%d'))
main=pd.DataFrame(dates,columns=['date'])
main.set_index('date',inplace=True)


# # LOAN REQUESTS

# ### LoanRequest Logs

# In[5]:


lr = LoanRequest.objects.filter(time__gte=start, time__lt=end)
lr = pd.DataFrame(lr.values())
lr['date'] = lr.time.apply(lambda x: x.strftime('%Y-%m-%d'))

S_requests = lr.pivot_table(index='date',
                            columns='loan_type_id',
                            values='id',
                            aggfunc='count',
                            fill_value=0,
                            margins=True)
S_unique_requests = lr.pivot_table(index='date',
                                   values='session_id',
                                   aggfunc=lambda x: len(x.unique()),
                                   fill_value=0,
                                   margins=True)
S_unique_requests.columns=['distinct_requests']


# ### Unique Sessions

# In[6]:


sesinfo = SessionInfo.objects.filter(created__gte=start, created__lt=end)
# sesids = sesinfo.values_list('session_id', flat=True) # not used but might be useful - returns session_ids only
sesinfo = pd.DataFrame(sesinfo.values())
sesinfo['date'] = sesinfo.created.apply(lambda x: x.strftime('%Y-%m-%d'))

S_unique_sessions = sesinfo.pivot_table(index='date',
                                   values='session_id',
                                   aggfunc='count',
                                   fill_value=0,
                                   margins=True)

S_unique_sessions.columns = ['sessions_created']


# In[7]:


S_requests_final = main.merge(S_requests,left_index=True,right_index=True, how='left')    .merge(S_unique_requests,left_index=True,right_index=True, how='left')    .merge(S_unique_sessions,left_index=True,right_index=True, how='left')    .fillna(0)    .sort_index(ascending=False)
S_requests_final = S_requests_final.astype(int,copy=False)


# # LOAN DETAIL REQUESTS

# ### LoanDetailRequst Logs

# In[9]:


ldr = LoanDetailRequest.objects.filter(time__gte=start, time__lt=end)
ldr = pd.DataFrame(ldr.values())
ldr['date'] = ldr.time.apply(lambda x: x.strftime('%Y-%m-%d'))


# In[10]:


S_detail_requests = ldr.pivot_table(index='date',
                                    columns='loan_type_id',
                                    values='id',
                                    aggfunc='count',
                                    fill_value=0,
                                    margins=True)

S_unique_detail_requests = ldr.pivot_table(index='date',
                                           values='session_id',
                                           aggfunc=lambda x: len(x.unique()),
                                           fill_value=0,
                                           margins=True)
S_unique_detail_requests.columns=['distinct_requests']


# In[11]:


S_detail_requests_final = main.merge(S_detail_requests,left_index=True,right_index=True, how='left')    .merge(S_unique_detail_requests,left_index=True,right_index=True, how='left')    .fillna(0)    .sort_index(ascending=False)
S_detail_requests_final = S_detail_requests_final.astype(int,copy=False)


# # Applications

# In[13]:


apps = Application.objects.filter(time__gte=start+timedelta(days=-10), time__lt=end)
apps = pd.DataFrame(apps.values())
apps['date'] = apps.time.apply(lambda x: x.strftime('%Y-%m-%d'))


# In[14]:


S_apps_overall = apps.pivot_table(index='date',
                                 columns='success',
                                 values=['id','session_id'],
                                 aggfunc=lambda x: len(x.unique()),
                                 fill_value=0,
                                 margins=True)


# In[15]:


S_apps_overall.reset_index(inplace=True)

main2 = main.reset_index()
main2.columns = pd.MultiIndex.from_arrays([['date'],['']])

S_apps_overall = main2.merge(S_apps_overall, on=[('date', '')], how='left')
S_apps_overall.fillna(0,inplace=True)
S_apps_overall.set_index([('date','')],inplace=True)
S_apps_overall = S_apps_overall.astype(int)
S_apps_overall.index.name=None
S_apps_overall.columns.set_names(['','Success'], inplace=True)
S_apps_overall.columns = S_apps_overall.columns.set_levels(['','Total','Distinct'],level=0)


# ### BURDAN DEVAM EDILECEK

# In[17]:


S_apps_by_request_source = apps.pivot_table(index='date',
                                             columns='request_source_id',
                                             values='id',
                                             aggfunc='count',
                                             fill_value=0,
                                             margins=True)


# # Generate Report

# In[18]:


def generate_html_tables(**kwargs):
    res = ''
    for key in kwargs:
        kwargs[key][1].index.name=None
        res += f'<h2>{kwargs[key][0]}</h2>'
        res += '<div class="table-wrapper">'
        res += kwargs[key][1].to_html()
        res += '</div>'
    return res


# In[36]:


report = """
<style>
INSERT_CSS
</style>
<div id='header'>
    <h2>DigiKredi Daily Report</h2>
    <h1>Generated at: INSERT_DATE</h1>
</div>
"""


# In[37]:


with(open(PATH_TO_CSS)) as file:
    css = file.read()


# In[38]:


report = report.replace('INSERT_CSS',css).replace('INSERT_DATE',end.strftime('%B %d, %Y'))


# In[39]:


report += generate_html_tables(
    apps=['Applications',S_apps_overall],
    reqs=['Loan Requests and Sessions Created',S_requests_final],
    dets=['Loan Detail Requests',S_detail_requests_final],
)


# ### Write Report to File (For Archiving)

# In[40]:


path_to_report = PATH_TO_REPORTS+'daily_report_'+end.strftime('%Y_%m_%d')+'.html'
with open(path_to_report,mode='w') as file:
    file.write(report)


# ### Send Report via Email

# In[41]:


import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders

fromaddr = "oakan13@ku.edu.tr"
toaddr = "oguzhan.akan17@gmail.com, oakan13@ku.edu.tr, emredanaci22@gmail.com, safakenes@gmail.com, aysenurgulnarboun@gmail.com"
# toaddr = "oguzhan.akan17@gmail.com"
gmailpass = "evtttklkxcdrrzmi"

msg = MIMEMultipart()
msg['From'] = fromaddr 
msg['To'] = toaddr 
msg['Subject'] = "DigiKredi - Daily Report - "+end.strftime("%d/%m/%Y")
body = f"""
    <div style="border:1px solid black;border-radius:12px;padding:12px;margin:12px;max-width:600px;">
    <p>
        Hello,<br>
        Daily report is generated. Please see attached.
    </p>
    </div>
"""




attach_these = [
    path_to_report
]
for filename in attach_these:
    try:
        attachment = open(filename, "rb") 
        p = MIMEBase('application', 'octet-stream') 
        p.set_payload((attachment).read()) 
        encoders.encode_base64(p) 
        p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
        msg.attach(p)
    except:
        body='Hello, I cannot produce the daily report today. Please go check.'
        print('cant attach: '+filename)
        pass

msg.attach(MIMEText(body, 'html')) 

s = smtplib.SMTP('smtp.gmail.com', 587)
s.starttls()
s.login(fromaddr, gmailpass)
text = msg.as_string()
s.sendmail(fromaddr, toaddr.split(','), text)
s.quit()

