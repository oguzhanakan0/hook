#!/usr/bin/env python
# coding: utf-8
# # Loan Summary Table Routine
# #### This script runs everyday and inserts summary of loans_loan table into loans_loansummary table.
import datetime
print(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")+": loan_summary.py started")
print("===================================")
import django
django.setup()
import os
import pandas as pd
from loans.models import *
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
loans_df = pd.DataFrame(list(Loan.objects.filter(active=True).values()))
loan_types = loans_df.loan_type_id.unique()
banks = loans_df.bank_id.unique()
for loan_type in loan_types:
    print("working on: "+loan_type)
    for bank in banks:
        # try:
        filtered_df = loans_df[(loans_df.bank_id==bank)&(loans_df.loan_type_id==loan_type)&(loans_df.is_base==False)]
        if not filtered_df.shape[0]==0:
            min_max_principal = filtered_df.principal_id.aggregate(['min','max'])
            min_max_tenure = filtered_df.tenure_id.aggregate(['min','max'])
            min_max_interest = filtered_df.interest.aggregate(['min','max'])
            # Introduce a LoanSummary object and save to db
            ls = LoanSummary()
            ls.bank = Bank.objects.get(name=bank)
            ls.loan_type = LoanType.objects.get(loan_type=loan_type)
            ls.min_principal = Principal.objects.get(principal=min_max_principal['min'])
            ls.max_principal = Principal.objects.get(principal=min_max_principal['max'])
            ls.min_tenure = Tenure.objects.get(tenure=min_max_tenure['min'])
            ls.max_tenure = Tenure.objects.get(tenure=min_max_tenure['max'])
            ls.min_interest = min_max_interest['min']
            ls.max_interest = min_max_interest['max']
            # we need to add available tenures:
            ls.save()
            tenures = filtered_df.tenure_id.unique()
            for this_tenure in tenures:
                ls.tenures.add(Tenure.objects.get(tenure=this_tenure))
            ls.save()
            print("saved for bank: "+bank)
        else:
            print("no loan found for bank: "+bank)
        #except:
        #    print("error in bank : "+bank)
print("===================================")
print(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")+": loan_summary.py ended")
