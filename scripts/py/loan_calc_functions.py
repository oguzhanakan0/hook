from loans.models import Principal, Tenure

def calculate_installment(principal, tenure, interest, bsmv=0.05, kkdf=0.15):
	total_interest = interest + interest * kkdf + interest * bsmv
	installment = (total_interest * principal) / (1 - (1 + total_interest) ** (-1 * tenure))
	return round(installment,2)

def calculate_loan_details(loan, principal, tenure):
	# print('== calculate_loan_details ==')
	# print(str(loan))
	# print('loan.id: '+str(loan.id))
	# print('principal: '+str(principal))
	# print('tenure: '+str(tenure))
	loan.principal = Principal(principal=principal)
	loan.installment = calculate_installment(principal, tenure, loan.interest)
	loan.allocation_fee = round(principal*0.005,2)
	loan.total_payment = round(loan.installment * tenure +  loan.allocation_fee,2)
	loan.total_interest = round(loan.total_payment - principal - loan.allocation_fee,2)
	# loan.interest_str = "{:.2%}".format(loan.interest)
	return loan