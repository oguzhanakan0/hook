import pandas as pd
def payment_plan_calculators(amount, maturity, interest, bsmv, kkdf):
    '''
    Belli tutar, vade ve faizde ödeme planı hesaplar. BSMV ve KKDF vergilerini input olarak alır. Bu vergi oranları konut kredilerinde 0, diğer kredilerde 5% ve 15% olarak hesaplanır.
    '''
    total_interest = interest + interest * kkdf + interest * bsmv
    monthly_payment = (total_interest * amount) / (1 - (1 + total_interest) ** (-1 * maturity))
    payment_plan = pd.DataFrame(index=range(maturity), columns=['monthly_pay','principal_before','principal_paid','interest_paid','kkdf','bsmv','principal_left'])

    for i in range(maturity):
        if i == 0:
            principal_before = amount
        else:
            principal_before = payment_plan.loc[i-1, 'principal_left']

        interest_paid = principal_before * interest
        kkdf_paid = interest_paid * kkdf
        bsmv_paid = interest_paid * bsmv
        principal_paid = monthly_payment - interest_paid - kkdf_paid - bsmv_paid
        principal_left = principal_before - principal_paid

        payment_plan.loc[i] = [monthly_payment, principal_before, principal_paid, interest_paid, kkdf_paid, bsmv_paid, principal_left]
        payment_plan = payment_plan.astype('float')

    payment_plan.drop('principal_before', axis=1, inplace=True)
    payment_plan = payment_plan.round(0)
    payment_plan.iloc[-1,-1]=0
    return payment_plan


def early_closure(prev_amount, prev_maturity, prev_interest, nof_installments_paid, early_payment_fine=0, bsmv=0.05, kkdf=0.15):
    '''
    Konut harici kredilerde erken kapama için gereken tutarı hesaplar. Bu kredilerde erken ödeme komisyonu 0 olarak belirlenmiştir. BSMV ve KKDF vergi oranları default gelir.
    Output: Erken ödeme için gerekli tutar
            Erken ödeme komisyonu (default 0)
            Şimdiye kadar toplam ödenen faiz
            Erken ödemeyle beraber toplam maliyet
            Erken ödeme sayesinde edilen kar (ilk ödeme planına kıyasla)
    '''
    prev_plan = payment_plan_calculators(prev_amount, prev_maturity, prev_interest, bsmv, kkdf)
    principal_left = prev_plan.iloc[nof_installments_paid-1, 5]
    early_payment_fine = principal_left * early_payment_fine 
    early_payment_amount = principal_left + early_payment_fine
    total_interest_paid = prev_plan.interest_paid[:nof_installments_paid].sum()
    total_cost = prev_plan.monthly_pay[:nof_installments_paid].sum() + early_payment_amount
    profit = prev_plan.monthly_pay.sum() - total_cost
    return early_payment_amount, early_payment_fine, total_interest_paid, total_cost, profit

def mortgage_early_closure_fee(prev_amount, prev_maturity, prev_interest, nof_installments_paid, early_payment_fine=None):
    if early_payment_fine:
        pass
    elif prev_maturity - nof_installments_paid <= 36:
        early_payment_fine = 0.01
    else:
        early_payment_fine = 0.02 
    prev_plan = payment_plan_calculators(prev_amount, prev_maturity, prev_interest, bsmv=0, kkdf=0)
    principal_left = prev_plan.iloc[nof_installments_paid-1, 5]
    early_payment_fine = principal_left * early_payment_fine
    early_payment_amount = principal_left + early_payment_fine
    return principal_left, early_payment_fine, early_payment_amount


def mortgage_early_closure(prev_amount, prev_maturity, prev_interest, nof_installments_paid, early_payment_fine=None, bsmv=0, kkdf=0):
    '''
    Konut kredilerinde erken kapama için gereken tutarı hesaplar. Bu kredilerde erken ödeme komisyonu kalan takside baglı olarak degisir. BSMV ve KKDF vergi oranları 0 default gelir.
    Output: Erken ödeme için gerekli tutar
            Erken ödeme komisyonu (default 0)
            Şimdiye kadar toplam ödenen faiz
            Erken ödemeyle beraber toplam maliyet
            Erken ödeme sayesinde edilen kar (ilk ödeme planına kıyasla)
    '''
    if early_payment_fine is not None:
        pass
    elif prev_maturity - nof_installments_paid <= 36:
        early_payment_fine = 0.01
    else:
        early_payment_fine = 0.02  
    
    prev_plan = payment_plan_calculators(prev_amount, prev_maturity, prev_interest, bsmv, kkdf)
    principal_left = prev_plan.iloc[nof_installments_paid-1, 5]
    early_payment_fine = principal_left * early_payment_fine 
    early_payment_amount = principal_left + early_payment_fine
    total_interest_paid = prev_plan.interest_paid[:nof_installments_paid].sum()
    total_cost = prev_plan.monthly_pay[:nof_installments_paid].sum() + early_payment_amount
    profit = prev_plan.monthly_pay.sum() - total_cost
    return early_payment_amount, early_payment_fine, total_interest_paid, total_cost, profit

def restructure_get_principal_left(prev_amount, prev_maturity, prev_interest, nof_installments_paid, bsmv=0.05, kkdf=0.15):
    prev_plan = payment_plan_calculators(prev_amount, prev_maturity, prev_interest, bsmv, kkdf)
    principal_left = prev_plan.iloc[nof_installments_paid-1, 5]
    return prev_plan, principal_left

def restructure_calculation(prev_amount, prev_maturity, prev_interest, nof_installments_paid, new_maturity, new_interest, prev_plan, principal_left, early_payment_fine=0, bsmv=0.05, kkdf=0.15):
    '''
    Konut harici kredilerde erken kapama için gereken tutarı hesaplar. Bu kredilerde erken ödeme komisyonu 0 olarak belirlenmiştir. BSMV ve KKDF vergi oranları default gelir.
    Output: Eski kredide kalan anapara bakiyesi (yapılandırma tutarı)
            Eski kredide kalan toplam taksit tutarı
            Erken ödeme komisyonu (0)
            Yapılandırma kredisi için toplam ödenecek tutar
            Yapılandırma sayesinde edilen kar (ilk ödeme planına kıyasla)
    '''
    # prev_plan, principal_left = restructure_get_principal_left(prev_amount, prev_maturity, prev_interest)
    waiting_payment = prev_plan.monthly_pay[nof_installments_paid:].sum()
    # early_payment_fine = principal_left * early_payment_fine 
    
    new_plan = payment_plan_calculators(principal_left, new_maturity, new_interest, bsmv, kkdf)
    new_total_payment = new_plan.monthly_pay.sum()
    new_total_payment += principal_left*0.005 # fine + allocation fee
    profit = waiting_payment - new_total_payment
    return principal_left, waiting_payment, early_payment_fine, new_total_payment, profit, 0


def mortgage_restructure_calculation(prev_amount, prev_maturity, prev_interest, nof_installments_paid, new_maturity, new_interest, prev_plan, principal_left, early_payment_fine=None, bsmv=0, kkdf=0):
    '''
    Konut kredilerinde erken kapama için gereken tutarı hesaplar. Bu kredilerde erken ödeme komisyonu kalan takside baglı olarak degisir. BSMV ve KKDF vergi oranları 0 default gelir.
    Output: Eski kredide kalan anapara bakiyesi (yapılandırma tutarı)
            Eski kredide kalan toplam taksit tutarı
            Erken ödeme komisyonu (0)
            Yapılandırma kredisi için toplam ödenecek tutar
            Yapılandırma sayesinde edilen kar (ilk ödeme planına kıyasla)
    '''
    if early_payment_fine is not None:
        pass
    elif prev_maturity - nof_installments_paid <= 36:
        early_payment_fine = 0.01
    else:
        early_payment_fine = 0.02  

    # prev_plan, principal_left = restructure_get_principal_left(prev_amount, prev_maturity, prev_interest, kkdf=0, bsmv=0)
    waiting_payment = prev_plan.monthly_pay[nof_installments_paid:].sum()
    early_payment_fine = principal_left * early_payment_fine 
    
    new_plan = payment_plan_calculators(principal_left, new_maturity, new_interest, bsmv, kkdf)
    new_total_payment = new_plan.monthly_pay.sum()
    new_total_payment += early_payment_fine + principal_left*0.005
    profit = waiting_payment - new_total_payment
    
    return principal_left, waiting_payment, early_payment_fine, new_total_payment, profit, new_plan


def mortgage_ltv(ev_tutari):
    if ev_tutari <= 500000:
        pesinat = ev_tutari * 0.10
    else:
        pesinat = ev_tutari * 0.2
    maks_kredi_tutari = ev_tutari - pesinat
    return pesinat, maks_kredi_tutari


def tasit_ltv(tasit_tutari):
    maks_kredi_tutari = min(tasit_tutari, 120000)*0.7 + max(tasit_tutari-120000, 0)*0.5
    pesinat = tasit_tutari - maks_kredi_tutari
    return pesinat, maks_kredi_tutari


def gecikme_faizi(loan_amount, maturity, interest, nof_installments_paid, delinquent_day, bsmv, kkdf):
    '''
    Belli tutar, vade ve faizde ödeme planı hesaplar. BSMV ve KKDF vergilerini input olarak alır. Bu vergi oranları konut kredilerinde 0, diğer kredilerde 5% ve 15% olarak hesaplanır.
    Sonrasında ödenen taksit sayısı üzerinden kalan anapara ve gecikme gün üzerinden toplam gecikme faizi hesaplar. Gecikme faizine bir kez daha KKDF ve BSMV alınır (konut hariç)
    Output: gecikme faizi
            gecikme faizi üzerinden hesaplanan vergi (konutta 0)
            toplam gecikme faizi (konutta gecikme faizi ile eşit)
    '''
    del_interest = interest * 1.3
    plan = payment_plan_calculators(loan_amount, maturity, interest, bsmv, kkdf)
    remaining_plan = plan.iloc[nof_installments_paid:]
    del_plan = remaining_plan.iloc[:int(delinquent_day/30)+1]
    del_days = []
    for i in range(del_plan.shape[0]):
        del_days.append(delinquent_day - i*30)
    del_plan['del_day'] = del_days
    del_plan['del_interest'] = del_plan['principal_paid'] * del_interest / 30 * del_plan['del_day']
    del_interest = del_plan.del_interest.sum()
    del_interest_tax = del_interest * (bsmv + kkdf)
    total_del_interest = del_interest + del_interest_tax
    return del_interest, del_interest_tax, total_del_interest