from django.urls import path
from django.urls import include, re_path
from . import views
from .ajax_queries import ajax_sepet_refresh

urlpatterns = [
    path('sepet/', views.sepet_page_view, name='sepet_page_view'),
    path('sepet/ajax/sepet-refresh/', ajax_sepet_refresh, name='ajax_sepet_refresh'),  # <-- this one here
    # path('kredi-karsilastir/', views.loan_comparison_page, name='loan_comparison_page'),
]