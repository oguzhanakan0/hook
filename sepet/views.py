from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from scripts.py.payment_plan import payment_plan
import pandas as pd
import pprint
from loans.ajax_queries import get_liked_loans_from_session
from loans.views import show_subscription_notification
from home.cart_views import transfer_liked_loans_to_list
from home.views import calculate_loan_details
from scripts.py.loan_comparison import *
from userlogs.models import SubscriptionType
# from loans.views import loan_comparison_page


def sepet_page_view(request):
	print("== SEPT.VIEWS.sepet_page_view ==")



	if (request.method=='POST'):
		if 'compare-all' in request.POST:
			liked_loans = get_liked_loans_from_session(request, 'raw')
			liked_loans_list = transfer_liked_loans_to_list(liked_loans, sort=True)
			return render_loan_comparison_page(request, liked_loans_list)
		elif 'compare-selected' in request.POST:
			print("============== compare-selected =============")
			pprint.pprint(request.POST)
			liked_loans_list = transfer_liked_loans_to_list(request.POST)
			pprint.pprint(liked_loans_list)
			return render_loan_comparison_page(request, liked_loans_list)
	else:
		liked_loans = get_liked_loans_from_session(request, http_raw='raw')
		liked_loans_list = transfer_liked_loans_to_list(liked_loans)
		# liked_loans_list = [calculate_loan_details(e, e.principal.principal, e.tenure.tenure) for e in liked_loans_list]
		liked_loan_types = set([str(e.loan_type) for e in liked_loans_list])
		liked_loans_per_loan_type = {}
		for loan_type in liked_loan_types:
			liked_loans_per_loan_type[loan_type] = [e for e in liked_loans_list if str(e.loan_type)==loan_type]
		removed_loans = get_liked_loans_from_session(request, get='removed_loans', http_raw='raw')
		removed_loans_list = transfer_liked_loans_to_list(removed_loans)
		# removed_loans_list = [calculate_loan_details(e, e.principal.principal, e.tenure.tenure) for e in removed_loans_list]
		removed_loan_types = set([str(e.loan_type) for e in removed_loans_list])
		removed_loans_per_loan_type = {}
		for loan_type in removed_loan_types:
			removed_loans_per_loan_type[loan_type] = [e for e in removed_loans_list if str(e.loan_type)==loan_type]
			removed_loans_per_loan_type[loan_type] = removed_loans_per_loan_type[loan_type][:min(len(removed_loans_per_loan_type[loan_type]),5)]

		return render(request, 'sepet/sepet_page.html', {
			'liked_loans': liked_loans_per_loan_type,
			'removed_loans': removed_loans_per_loan_type,
			'no_liked_loans': True if len(liked_loans)==0 else False,
			'no_removed_loans': True if len(removed_loans)==0 else False,
			'show_subscription_notification':show_subscription_notification(request,subscription_type='saved-loans'),
			'subscription_type':'saved-loans'
		})

def render_loan_comparison_page(request, liked_loans_list):
	print("== LOAN.VIEWS.LOAN_COMPARISON_PAGE ==")
	if(len(liked_loans_list)<2):
		return HttpResponse('<script>alert("En az 2 adet kredi seçmeniz gerekmektedir.");window.location="/sepet";</script>')
	elif(len(liked_loans_list)==2):
		print('user has exactly 2 liked loans. redirecting to 2 loan comparison page..')
		l1 = liked_loans_list[0]
		l2 = liked_loans_list[1]
		loan_comparison_text, l1, l2 = compare_loans(l1,l2,l1_principal=l1.principal.principal,l1_tenure=l1.tenure.tenure,l2_principal=l2.principal.principal,l2_tenure=l2.tenure.tenure)
		return render(request, 'sepet/loan_comparison_page_twoloans.html', {
			'l1': l1,
			'l2': l2,
			'loan_comparison_text':loan_comparison_text
		})
	elif(len(liked_loans_list)<=20):
		print('user has more than 2 liked loans. redirecting to multiple loan comparison page..')
		# liked_loans_list = [calculate_loan_details(e, e.principal.principal, e.tenure.tenure) for e in liked_loans_list]
		return render(request, 'sepet/loan_comparison_page_multipleloans.html', {
			'loans': liked_loans_list,
			'enum': [str(e)+'. Kredi' for e in range(1,(len(liked_loans_list)+1))]
			# 'loan_comparison_text':loan_comparison_text
		})
	else:
		return HttpResponse('at most 20 loans should be liked')