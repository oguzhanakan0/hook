from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from scripts.py.payment_plan import payment_plan
import pandas as pd
import pprint
from loans.ajax_queries import get_liked_loans_from_session
from home.cart_views import transfer_liked_loans_to_list
from home.views import calculate_loan_details
import json

def ajax_sepet_refresh(request):
    print("== SEPET.VIEWS.ajax_sepet_refresh ==")
    # liked_loans = get_liked_loans_from_session(request, get='favorite_loans', http_raw='raw')
    liked_loans = json.loads(request.GET['favorite_loans'])
    liked_loans_list = transfer_liked_loans_to_list(liked_loans)
    liked_loans_list = [calculate_loan_details(e, e.principal.principal, e.tenure.tenure) for e in liked_loans_list]
    liked_loan_types = set([str(e.loan_type) for e in liked_loans_list])
    liked_loans_per_loan_type = {}
    for loan_type in liked_loan_types:
        liked_loans_per_loan_type[loan_type] = [e for e in liked_loans_list if str(e.loan_type)==loan_type]
    # the reason why I get removed loans from the session and NOT from the GET is
    # that the liked loans used in many pages so it gets to be transfered among requests
    # but removed loans are sepet specific.
    if request.GET.get('url') == '/sepet/':
        print('THIS BIT SHOULD NOT RUN IF YOU ARE NOT IN /SEPET/')
        removed_loans = get_liked_loans_from_session(request, get='removed_loans', http_raw='raw')
        removed_loans_list = transfer_liked_loans_to_list(removed_loans)
        removed_loans_list = [calculate_loan_details(e, e.principal.principal, e.tenure.tenure) for e in removed_loans_list]
        removed_loan_types = set([str(e.loan_type) for e in removed_loans_list])
        removed_loans_per_loan_type = {}
        print("=== removed_loans_per_loan_type ===")
        pprint.pprint(removed_loans_per_loan_type)
        for loan_type in removed_loan_types:
            removed_loans_per_loan_type[loan_type] = [e for e in removed_loans_list if str(e.loan_type)==loan_type]
            removed_loans_per_loan_type[loan_type] = removed_loans_per_loan_type[loan_type][:min(len(removed_loans_per_loan_type[loan_type]),5)]
        # print("Number of liked loans: "+str(len(liked_loans_list)))
        # print("Liked loans on ajax_sepet_refresh:")
        # pprint.pprint(liked_loans_list)
        # print("Number of removed loans: "+str(len(removed_loans_list)))
        # print("Removed loans on ajax_sepet_refresh:")
        # pprint.pprint(removed_loans_list)
        return render(request, 'sepet/layout.html', {
            'liked_loans': liked_loans_per_loan_type,
            'removed_loans': removed_loans_per_loan_type,
            'no_liked_loans': True if len(liked_loans)==0 else False,
            'no_removed_loans': True if len(removed_loans)==0 else False
        })
    else:
        print('THIS BIT SHOULD RUN IF YOU ARE NOT IN /SEPET/')
        return render(request, 'sepet/layout.html', {
            'liked_loans': liked_loans_per_loan_type,
            'no_liked_loans': True if len(liked_loans)==0 else False,
        })
